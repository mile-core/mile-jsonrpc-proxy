#include <iostream>
#include <condition_variable>

#include <boost/asio.hpp>
#include <boost/program_options.hpp>

#include <csa.h>

#include "server.h"

using namespace std;

static std::string opt_bind_addr = "0.0.0.0";
static unsigned int opt_bind_port = 4000;
static unsigned int opt_n_threads = 1;
static unsigned int opt_debug = false;

static std::string opt_node_addr = "149.28.162.70";
static unsigned int opt_node_port = 4010;
static bool opt_use_keepalive = true;
static bool opt_use_ssl = false;
static std::string opt_pem_file = "";

namespace po = boost::program_options;

bool isStart = false;
bool messageConnect = true;

std::condition_variable cv;

void network_callback(int code, const char *msg);

void on_auth(int code, const char *msg);

void shell_exit(int error_code);

void tcp_connect();

std::vector<std::string> gethostbyname(std::string hostname);

static bool parse_cmdline(int ac, char *av[]) {

    try {

        po::options_description desc("Allowed options");

        desc.add_options()
                ("help", "produce help message")

                ("debug", "print debug messages")

                ("no-keepalive", "do not asc keep-alive header")

                ("address,a", po::value<std::string>(&opt_bind_addr)->
                         default_value(opt_bind_addr),
                 "TCP bind address")

                ("pem-file,s", po::value<std::string>(&opt_pem_file)->
                         default_value(opt_pem_file),
                 "Open SSL pem file")

                ("port,p", po::value<unsigned int>(&opt_bind_port)->
                         default_value(opt_bind_port),
                 "TCP bind port")

                ("mile-node-address,m", po::value<std::string>(&opt_node_addr)->
                         default_value(opt_node_addr),
                 "MILE local node address")

                ("mile-node-port,n", po::value<unsigned int>(&opt_node_port)->
                         default_value(opt_node_port),
                 "MILE local node port")

            //("threads,t", po::value<unsigned int>(&opt_n_threads)->
            //         default_value(opt_n_threads),
            // "Thread pool size")

                ;

        po::variables_map vm;
        po::store(po::parse_command_line(ac, av, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            exit(0);
        }

        if (vm.count("debug")) {
            opt_debug = true;
        }

        if (vm.count("no-keepalive")) {
            opt_use_keepalive = false;
        }

        if (!opt_pem_file.empty()) {
            opt_use_ssl = true;
        }
    }

    catch (exception &e) {
        std::cerr << "error: " << e.what() << "\n";
        return false;
    }
    catch (...) {
        std::cerr << "Exception of unknown type!\n";
        return false;
    }

    return true;
}

int main(int argc, char *argv[]) {
    setlocale(LC_ALL, "");

    if (!parse_cmdline(argc, argv))
        return 1;



    // register network lib callback
    pcsa_on_connect(network_callback);
    pcsa_on_auth(on_auth);

    tcp_connect();

    try {
        // Initialise the server.
        std::size_t num_threads =
                boost::lexical_cast<std::size_t>(opt_n_threads);

        wallet_jsonrpc::jsonrpc::server s(opt_bind_addr, opt_bind_port, num_threads, opt_use_keepalive, opt_use_ssl, opt_pem_file);

        // Run the server until stopped.
        s.run();
    }
    catch (std::exception &e) {
        std::cerr << "exception: " << e.what() << "\n";
    }

    return 0;
}

void on_auth(int code, const char *msg) {

    if (!code) {
        printf("Logged in as user '%s'", "test");
        isStart = true;
        cv.notify_one();
        return;
    }

    printf("authentication error!\n");
    printf("([errcode = %d]: %s)\n", code, msg);

    shell_exit(1);
}

void tcp_connect() {

    isStart = false;

    try {
        std::vector<std::string> ips = gethostbyname(opt_node_addr);

        if (ips.size() == 0) {
            cout << "Address not found for: " << opt_bind_addr << endl;
            shell_exit(1);
            return;
        }


        std::string ip = ips[0];

        // connect to engine
        auto const rc = pcsa_connect(ip.c_str(),
                                     opt_node_port,
                                     "relay",
                                     "web");

        if (rc != PCSA_RES_OK) {
            if (PCSA_RES_TIMEOUT == rc) {
                printf("Timeout on connection!");
            } else if (PCSA_RES_RESOURCE_UNAVAILABLE == rc) {
                printf("Connection to server -- FAIL!");
            } else if (PCSA_RES_ACCESS_DENIED == rc) {
                printf("Access denied!");
            } else  //if (PCSA_RES_FAIL == rc)
            {
                printf("Internal error!");
            }

            // detection of error text and code
            {
                short int nErrCode = 0;
                char szErrText[120] = {0};

                nErrCode = pcsa_get_last_cs_result();
                pcsa_get_last_cs_error_text(szErrText, sizeof(szErrText));

                printf("%s", szErrText);
                printf("error-code (%d)", nErrCode);
                puts("");
            }

            shell_exit(1);
        }
    }
    catch (const std::exception &e) {
        cout << "Connect/Login error: " << e.what() << endl;
        shell_exit(1);
    }
    catch (...) {
        cout << "Connect/Login error: " << "<unknown exception>" << endl;
        shell_exit(1);
    }
}

void network_callback(int code, const char *msg) {

    printf("code = %i %s \n", code, msg);

    if (!code) {
        // connect
        messageConnect = true;
        printf("Server connected establish!");
        return;
    } else if (messageConnect) {
        printf("Server connected lost!");
        messageConnect = false;
    }

    tcp_connect();
}

void shell_exit(int error_code) {
    pcsa_destroy();
    exit(error_code);
}


std::vector<std::string> gethostbyname(std::string hostname) {

    std::vector<std::string> addresses;

    boost::asio::io_service io_service;

    boost::asio::ip::tcp::resolver resolver(io_service);
    boost::asio::ip::tcp::resolver::query query(hostname, "");

    boost::asio::ip::tcp::resolver::iterator destination = resolver.resolve(query);
    boost::asio::ip::tcp::resolver::iterator end;
    boost::asio::ip::tcp::endpoint endpoint;

    while (destination != end) {
        endpoint = *destination++;
        addresses.push_back(endpoint.address().to_string());
    }

    return addresses;

}
