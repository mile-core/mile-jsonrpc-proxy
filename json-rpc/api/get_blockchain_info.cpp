

#include "api.h"
#include "cache.h"
#include <csa_cxx.h>

namespace wallet_jsonrpc {

    namespace jsonrpc {


        pcsa_result get_blockchain_info_cxx(json_spirit::Object &query, json_spirit::Object &result) {

            static std::string cache_key("get_blockchain_info");

            pcsa_result handle_result = PCSA_RES_FAIL;

            if (result_cache::Instance().has(cache_key)){
                auto p = result_cache::Instance().get(cache_key);
                result.push_back(json_spirit::Pair("result", *p));
                return PCSA_RES_OK;
            }

            try {

                pcsa_blockchain_info info;

                if ((handle_result = get_blockchain_info(&info)) == PCSA_RES_OK) {

                    {
                        std::unique_lock<std::mutex> lk(chain_info::Instance().info_mutex);

                        if ((handle_result = chain_info::Instance().check()) != PCSA_RES_OK ){
                            return  handle_result;
                        }
                    }

                    if (result_cache::Instance().has(cache_key)){
                        auto p = result_cache::Instance().get(cache_key);
                        result.push_back(json_spirit::Pair("result", *p));
                        return PCSA_RES_OK;
                    }

                    return handle_result;
                }

                return handle_result;
            }
            catch (...) {
                return handle_result;
            }
        }
    }
}