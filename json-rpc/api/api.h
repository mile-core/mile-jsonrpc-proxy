#ifndef SHELL_API_H
#define SHELL_API_H

#include <mutex>
#include <unordered_map>
#include <json_spirit_value.h>
#include <csa_cxx.h>
#include <unordered_set>
#include "cache.h"

namespace wallet_jsonrpc {

    namespace jsonrpc {

        class result_cache {
        public:

            size_t set(const std::string &id,  json_spirit::Object *val, time_t expiration = 0,
                       const cache_writemode mode = FASTCACHE_WRITEMODE_WRITE_ALWAYS);

            shared_ptr<json_spirit::Object> get(const std::string &id);

            size_t del(const std::string &id);

            bool has(const std::string &id);

            static result_cache& Instance() {
                static result_cache myInstance;
                return myInstance;
            }

            result_cache(result_cache const&) = delete;             // Copy construct
            result_cache(result_cache&&) = delete;                  // Move construct
            result_cache& operator=(result_cache const&) = delete;  // Copy assign
            result_cache& operator=(result_cache &&) = delete;      // Move assign

        protected:
            result_cache() {
            }

            ~result_cache() {
            }

        private:
            wallet_jsonrpc::cache<std::string, json_spirit::Object> value;
        };


        class chain_info {

        public:

            pcsa_result asset_code_by(std::string &name, unsigned short *code);

            pcsa_result update(const pcsa_blockchain_info &info);
            pcsa_result check();

            static chain_info& Instance() {
                static chain_info myInstance;
                return myInstance;
            }

            chain_info(chain_info const&) = delete;             // Copy construct
            chain_info(chain_info&&) = delete;                  // Move construct
            chain_info& operator=(chain_info const&) = delete;  // Copy assign
            chain_info& operator=(chain_info &&) = delete;      // Move assign

            std::mutex info_mutex;

        protected:
            chain_info() {};
            ~chain_info() {}

        private:
            std::unordered_map<unsigned short, std::string> supported_assets_;
            std::unordered_set<std::string> supported_transactions_;
        };

        pcsa_result ping_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result get_wallet_keys_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result get_blockchain_info_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result create_wallet_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result get_wallet_state_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result get_transfer_assets_transaction_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result send_signed_transaction_cxx(json_spirit::Object &query, json_spirit::Object &result);

        pcsa_result get_wallet_transactions_cxx(json_spirit::Object &query, json_spirit::Object &result);

        }

}
#endif //SHELL_API_H
