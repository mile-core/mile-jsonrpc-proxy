

#include <json_spirit.h>
#include <csa_cxx.h>
#include <csa_cxx_container.h>
#include "api.h"
#include <boost/lexical_cast.hpp>

namespace wallet_jsonrpc {
    namespace jsonrpc {

        bool get_pcsa_keys(const std::string &type, const std::string &password,
                          const std::string &wallet, pcsa_keys_pair &keyPairs) {
            if (type == "local") {
                char *errorMessage = nullptr;
                if (get_local_wallet_keys(wallet.c_str(), password.c_str(), &keyPairs, &errorMessage)) {
                    if (errorMessage) {
                        free(errorMessage);
                    }
                    return false;
                }

            } else if (type == "node") {
                if (get_node_wallet_keys(wallet.c_str(), password.empty() ? nullptr : password.c_str(), &keyPairs)) {
                    return false;
                }
            } else {
                return false;
            }

            return true;
        }


        pcsa_result get_transfer_assets_transaction_cxx(json_spirit::Object &query, json_spirit::Object &result) {

            pcsa_result handle_result = PCSA_RES_FAIL;

            try {

                json_spirit::Value tmpval;

                tmpval = json_spirit::find_value(query, "params");

                unsigned short assets_code;

                if (tmpval.type() == json_spirit::obj_type) {

                    json_spirit::Object &obj = tmpval.get_obj();

                    std::string asset = json_spirit::find_value(obj, "asset").get_str();
                    std::string source_wallet = json_spirit::find_value(obj, "from").get_str();
                    std::string destination_wallet = json_spirit::find_value(obj, "to").get_str();
                    std::string amount = json_spirit::find_value(obj, "amount").get_str();
                    std::string private_key = json_spirit::find_value(obj, "private_key").get_str();

                    {
                        std::unique_lock<std::mutex> lk(chain_info::Instance().info_mutex);

                        if ((handle_result = chain_info::Instance().asset_code_by(asset, &assets_code)) !=
                            PCSA_RES_OK) {
                            return handle_result;
                        }
                    }

                    // pcsa_keys_pair keyPairs;
                    // memset(&keyPairs, 0, sizeof(pcsa_keys_pair));

                    //if ((handle_result = get_wallet_keys(source_wallet.c_str(), pass.empty() ? nullptr : pass.c_str(),
                    //                                     &keyPairs, error)) != PCSA_RES_OK) {
                    //    std::cout << "Can't get local wallet keys" << std::endl;
                    //    return handle_result;
                    //}

                    pcsa_keys_pair keyPairs;
                    get_pcsa_keys("node", private_key, source_wallet, keyPairs);

                    ///char *trx = 0;
                    ///TODO:

                    if (transfer_assets_transaction(&keyPairs, destination_wallet.c_str(), boost::lexical_cast<unsigned short>(asset), amount.c_str())) {///== PCSA_RES_OK) {

                        json_spirit::Object keys;

                        ///keys.push_back(json_spirit::Pair("transaction_data", trx));

                        result.push_back(json_spirit::Pair("result", keys));

                        //if (trx) {
                        //    free(trx);
                        //}

                        return handle_result;
                    }


                    //if (trx) {
                    //    free(trx);
                    //}
                }


                return handle_result;
            }
            catch (...) {
                return handle_result;
            }
        }
    }
}