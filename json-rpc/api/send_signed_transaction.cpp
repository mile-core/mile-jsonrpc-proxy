#include <json_spirit.h>
#include "api.h"
#include <csa_cxx.h>

namespace wallet_jsonrpc {
    namespace jsonrpc {
        pcsa_result send_signed_transaction_cxx(json_spirit::Object &query, json_spirit::Object &result) {

            static json_spirit::Value true_vale(true);

            pcsa_result handle_result = PCSA_RES_FAIL;

            json_spirit::Value tmpval = json_spirit::find_value(query, "params");

            try {

                if (tmpval.type() == json_spirit::obj_type) {
                    json_spirit::Object &obj = tmpval.get_obj();
                    std::string transaction_data = json_spirit::find_value(obj, "transaction_data").get_str();
                    auto *error = new pcsa_error;
                    if ((handle_result = transfer_assets_transaction_data(transaction_data.c_str(), error)) ==
                        PCSA_RES_OK) {
                        result.push_back(json_spirit::Pair("result", true_vale));
                        return handle_result;
                    }
                }

                return handle_result;
            }
            catch (...) {
                return handle_result;
            }
        }
    }
}
