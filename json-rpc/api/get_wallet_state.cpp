#include <json_spirit.h>
#include <csa_cxx.h>
#include "api.h"

namespace wallet_jsonrpc {
    namespace jsonrpc {
        pcsa_result get_wallet_state_cxx(json_spirit::Object &query, json_spirit::Object &result) {

            pcsa_result handle_result = PCSA_RES_FAIL;

            json_spirit::Value tmpval;
            tmpval = json_spirit::find_value(query, "params");

            try {
                if (tmpval.type() == json_spirit::obj_type) {

                    json_spirit::Object &obj = tmpval.get_obj();

                    json_spirit::Value pubVal = json_spirit::find_value(obj, "public_key");

                    pcsa_wallet_state state;

                    memset(&state, 0, sizeof(pcsa_wallet_state));

                    if (!pubVal.is_null()) {
                        if ((handle_result = get_wallet_state_by_key(pubVal.get_str().c_str(), &state)) != PCSA_RES_OK) {
                            return handle_result;
                        }
                    } else {
                        return handle_result;
                    }

                    json_spirit::Object keys;
                    json_spirit::Array balance;
                    for (int i = 0; i < state.assets_length; i++) {

                        json_spirit::Object keys;

                        keys.push_back(json_spirit::Pair("asset_code", state.assets[i].code));
                        keys.push_back(json_spirit::Pair("amount", state.assets[i].asset_name.name));

                        balance.push_back(keys);
                    }

                    keys.push_back(json_spirit::Pair("balance", balance));

                    keys.push_back(json_spirit::Pair("tags", state.tags));
                    keys.push_back(json_spirit::Pair("node_address", state.node_address));
                    keys.push_back(json_spirit::Pair("last_transaction_id", state.last_transaction_id));
                    keys.push_back(json_spirit::Pair("exist", state.exist));

                    result.push_back(json_spirit::Pair("result", keys));

                    free(state.assets);

                    return PCSA_RES_OK;

                }
                return handle_result;
            }
            catch (...) {
                return handle_result;
            }
        }
    }
}