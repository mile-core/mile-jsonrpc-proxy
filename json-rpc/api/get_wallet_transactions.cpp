#include <csa_cxx.h>
#include <json_spirit.h>
#include "api.h"

namespace wallet_jsonrpc {
    namespace jsonrpc {

        std::string parse_token(const std::string delimiter, std::string &description){

            std::string s(description);

            size_t pos = 0;
            std::string token;
            if ((pos = description.find(delimiter)) != std::string::npos) {
                token = s.substr(0, pos);
                s.erase(0, pos + delimiter.length());
            }
            description = s;

            return  token;
        }

        bool endsWith(const std::string& s, const std::string& suffix)
        {
            return s.size() >= suffix.size() &&
                   s.substr(s.size() - suffix.size()) == suffix;
        }

        std::vector<std::string> split(const std::string& s, const std::string& delimiter, const bool& removeEmptyEntries = true)
        {
            std::vector<std::string> tokens;

            for (size_t start = 0, end; start < s.length(); start = end + delimiter.length())
            {
                size_t position = s.find(delimiter, start);
                end = position != std::string::npos ? position : s.length();

                std::string token = s.substr(start, end - start);
                if (!removeEmptyEntries || !token.empty())
                {
                    tokens.push_back(token);
                }
            }

            if (!removeEmptyEntries &&
                (s.empty() || endsWith(s, delimiter)))
            {
                tokens.push_back("");
            }

            return tokens;
        }

        void parse_transaction_description(const std::string &_description, json_spirit::Object &result){

            std::string description(_description);

            std::string type = parse_token("(", description);
            std::string container = parse_token(")", description);

            std::vector<std::string> params = split(container, ",");

            std::string id = "";
            std::string from = "";
            std::string to = "";
            std::string amount = "";
            std::string assets = "";
            int j = 0;
            for (std::vector<std::string>::iterator it=params.begin(); it!=params.end(); it++, j++) {

                std::string param(*it);

                switch (j) {
                    case 0:{
                        id = parse_token("ID=", param);
                        id = param;
                        break;}
                    case 1:{
                        std::vector<std::string> fromto = split(param, "->");
                        if (fromto.size() == 2) {
                            from = fromto[0];
                            to = fromto[1];
                        }}
                        break;
                    case 2:{
                        std::vector<std::string> as = split(param, " ");
                        if (as.size() == 2) {
                            amount = as[0];
                            assets = as[1];
                        }}
                        break;
                }
            }

            result.push_back(json_spirit::Pair("type", type));
            result.push_back(json_spirit::Pair("id", id));
            result.push_back(json_spirit::Pair("from", from));
            result.push_back(json_spirit::Pair("to", to));
            result.push_back(json_spirit::Pair("assets", assets));
            result.push_back(json_spirit::Pair("amount", amount));
        }

        pcsa_result get_wallet_transactions_cxx(json_spirit::Object &query, json_spirit::Object &result) {

            pcsa_result handle_result = PCSA_RES_FAIL;

            json_spirit::Value tmpval;
            tmpval = json_spirit::find_value(query, "params");

            try {
                if (tmpval.type() == json_spirit::obj_type) {

                    json_spirit::Object &obj = tmpval.get_obj();

                    json_spirit::Value pubVal = json_spirit::find_value(obj, "public_key");
                    json_spirit::Value trxCount = json_spirit::find_value(obj, "count");

                    pcsa_wallet_state state;
                    pcsa_wallet_transaction *transactions;
                    unsigned int transactionsCount;

                    memset(&state, 0, sizeof(pcsa_wallet_state));

                    if (!pubVal.is_null()) {

                        if ((handle_result = get_wallet_transactions_by_key(pubVal.get_str().c_str(),
                                                                            trxCount.get_int(),
                                                                            &transactions,
                                                                            &transactionsCount)) != PCSA_RES_OK) {
                            return handle_result;
                        }
                    } else {
                        return handle_result;
                    }

                    json_spirit::Object keys;
                    json_spirit::Array trxArray;
                    for (int i = 0; i < transactionsCount; i++) {

                        json_spirit::Object tdesc;
                        parse_transaction_description(transactions[i].description, tdesc);

                        json_spirit::Object keys;

                        keys.push_back(json_spirit::Pair("description", tdesc));
                        keys.push_back(json_spirit::Pair("digest", transactions[i].digest));
                        keys.push_back(json_spirit::Pair("type", transactions[i].type));

                        trxArray.push_back(keys);
                    }

                    keys.push_back(json_spirit::Pair("transactions", trxArray));


                    result.push_back(json_spirit::Pair("result", keys));

                    free(transactions);

                    return PCSA_RES_OK;

                }
                return handle_result;
            }
            catch (...) {
                return handle_result;
            }
        }
    }
}