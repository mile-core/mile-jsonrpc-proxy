#include <json_spirit.h>
#include "api.h"
#include <csa_cxx.h>

namespace wallet_jsonrpc {
    namespace jsonrpc {
        pcsa_result create_wallet_cxx(json_spirit::Object &query, json_spirit::Object &result) {

            pcsa_result handle_result = PCSA_RES_FAIL;

            json_spirit::Value tmpval;
            tmpval = json_spirit::find_value(query, "params");

            try {
                if (tmpval.type() == json_spirit::obj_type) {

                    json_spirit::Object &obj = tmpval.get_obj();
                    std::string name = json_spirit::find_value(obj, "wallet_name").get_str();
                    std::string pass = json_spirit::find_value(obj, "password").get_str();

                    pcsa_keys_pair keysPair;
                    memset(&keysPair, 0, sizeof(pcsa_keys_pair));

                    if ((handle_result = create_node_wallet(name.c_str(), pass.empty() ? nullptr : pass.c_str(), &keysPair)) == PCSA_RES_OK) {
                        json_spirit::Object keys;

                        keys.push_back(json_spirit::Pair("public_key", keysPair.public_key));
                        keys.push_back(json_spirit::Pair("private_key", keysPair.private_key));

                        result.push_back(json_spirit::Pair("result", keys));

                        return handle_result;
                    }

                }
                return handle_result;
            }
            catch (...) {
                return handle_result;
            }
        }
    }
}