#include "api.h"
#include <csa_cxx.h>


namespace wallet_jsonrpc {
    namespace jsonrpc {

        size_t result_cache::set(const std::string &id, json_spirit::Object *val, time_t expiration ,
                                 const cache_writemode mode) {
            auto p = shared_ptr<json_spirit::Object>(val);
            return this->value.set(id, p, expiration, mode);
        }

        shared_ptr<json_spirit::Object> result_cache::get(const std::string &id) {
            return this->value.get(id);
        }

        size_t result_cache::del(const std::string &id) {
            return this->value.del(id);
        }

        bool result_cache::has(const std::string &id) {
            return this->value.has(id);
        }

        pcsa_result chain_info::update(const pcsa_blockchain_info &info) {

            static std::string cache_key("get_blockchain_info");

            json_spirit::Object keys;

            keys.push_back(json_spirit::Pair("project", info.project_name));
            keys.push_back(json_spirit::Pair("version", info.version));

            json_spirit::Array transactions;
            if (info.supported_transactions != nullptr) {
                for (int i = 0; i < info.transactions_count; ++i) {
                    supported_transactions_.insert(info.supported_transactions[i].name);
                    transactions.push_back(info.supported_transactions[i].name);
                }
            }

            keys.push_back(json_spirit::Pair("supported_transactions", transactions));

            json_spirit::Array assets;
            if (info.supported_assets != nullptr) {
                for (int i = 0; i < info.assets_count; ++i) {
                    std::istringstream str(info.supported_assets[i].asset_name.name);
                    std::istream_iterator<std::string> it(str);
                    if (it == std::istream_iterator<std::string>()) {
                        std::string text("Get blockchain empty asset.");
                        //if (error) {
                        //    strncpy(error->text, text.c_str(), text.length() - 1);
                        //}
                        return PCSA_RES_FAIL;
                    }
                    supported_assets_.insert({info.supported_assets[i].code, *it});

                    json_spirit::Object keys;

                    keys.push_back(json_spirit::Pair("name", info.supported_assets[i].asset_name.name));
                    keys.push_back(json_spirit::Pair("code", info.supported_assets[i].code));

                    assets.push_back(keys);
                }
            }

            keys.push_back(json_spirit::Pair("supported_assets", assets));

            result_cache::Instance().set(cache_key, new json_spirit::Object(keys));

            return PCSA_RES_OK;
        }

        pcsa_result chain_info::check() {

            pcsa_result handle_result = PCSA_RES_FAIL;

            if (chain_info::Instance().supported_assets_.empty()) {

                pcsa_blockchain_info info;

                if ((handle_result = get_blockchain_info(&info)) != PCSA_RES_OK) {
                    return handle_result;
                }

                if ((handle_result = chain_info::Instance().update(info)) != PCSA_RES_OK) {
                    free(info.supported_assets);
                    free(info.supported_transactions);
                    return handle_result;
                }

                free(info.supported_assets);
                free(info.supported_transactions);
            }

            if (chain_info::Instance().supported_assets_.empty()) {
                return handle_result;
            }
            else {
                return PCSA_RES_OK;
            }
        }

        pcsa_result chain_info::asset_code_by(std::string &name, unsigned short *code){

            pcsa_result handle_result = PCSA_RES_FAIL;

            if ((handle_result = chain_info::Instance().check()) != PCSA_RES_OK){
                return handle_result;
            }

            auto it = std::find_if(chain_info::Instance().supported_assets_.begin(),
                                   chain_info::Instance().supported_assets_.end(),
                                   [&name](const decltype(chain_info::Instance().supported_assets_)::value_type &value) {
                                       return value.second == name;
                                   });

            if (it == chain_info::Instance().supported_assets_.end()) {
                return handle_result;
            }

            *code = it->first;

            return PCSA_RES_OK;
        }
    }
}
