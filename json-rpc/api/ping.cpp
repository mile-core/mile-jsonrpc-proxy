#include "api.h"
#include <csa_cxx.h>

namespace wallet_jsonrpc {
    namespace jsonrpc {
        pcsa_result ping_cxx(json_spirit::Object &query, json_spirit::Object &result) {
            json_spirit::Value tmpval(true);

            pcsa_result handle_result = PCSA_RES_FAIL;

            pcsa_blockchain_info info;
            if ((handle_result = get_blockchain_info(&info)) == PCSA_RES_OK) {
                result.push_back(json_spirit::Pair("result", tmpval));
                return handle_result;
            }

            return handle_result;
        }
    }
}
