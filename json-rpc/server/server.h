#ifndef SHELL_JSONRPCSERVER_H
#define SHELL_JSONRPCSERVER_H

#include <string>
#include <vector>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include "connection.h"
#include "request_handler.h"

namespace wallet_jsonrpc {
    namespace jsonrpc {

        class server: private boost::noncopyable {
        public:
            /// Construct the server to listen on the specified TCP address and port, and
            /// serve up files from the given directory.
            explicit server(const std::string& address,
                            unsigned int port,
                            std::size_t thread_pool_size,
                            bool use_keepalive,
                            bool use_ssl = false,
                            const std::string &pemfile = "");

            /// Run the server's io_service loop.
            void run();

        private:
            /// Initiate an asynchronous accept operation.
            void start_accept();

            /// Handle completion of an asynchronous accept operation.
            void handle_accept(const boost::system::error_code& e);

            /// Handle a request to stop the server.
            void handle_stop();

            /// The number of threads that will call io_service::run().
            std::size_t thread_pool_size_;

            /// The io_service used to perform asynchronous operations.
            boost::asio::io_service io_service_;

            /// The signal_set is used to register for process termination notifications.
            boost::asio::signal_set signals_;

            /// Acceptor used to listen for incoming connections.
            boost::asio::ip::tcp::acceptor acceptor_;

            /// SSL context
            boost::asio::ssl::context context_;

            /// The next connection to be accepted.
            connection_ptr new_connection_;
            ssl_conn_ptr new_ssl_conn_;

            /// The handler for all incoming requests.
            request_handler request_handler_;

            bool  use_keepalive_;
            bool  use_ssl_;
            std::string opt_ssl_pemfile_;
        };
    }
}


#endif //SHELL_JSONRPCSERVER_H
