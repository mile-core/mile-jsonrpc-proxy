//
// Created by denis svinarchuk on 02.06.2018.
//

#ifndef SHELL_REQUEST_HEADER_H
#define SHELL_REQUEST_HEADER_H


#include <string>
#include <boost/noncopyable.hpp>

namespace wallet_jsonrpc {
    namespace jsonrpc {

        struct reply;
        struct request;

        /// The common handler for all incoming requests.
        class request_handler
                : private boost::noncopyable
        {
        public:
            /// Construct with a directory containing files to be served.
            explicit request_handler();

            /// Handle a request and produce a reply.
            void handle_request(const request& req, reply& rep, bool keepalive);

        private:

            /// Perform URL-decoding on a string. Returns false if the encoding was
            /// invalid.
            static bool url_decode(const std::string& in, std::string& out);
            void fill_cors(reply& rep);
        };

    }
}

#endif //SHELL_REQUEST_HEADER_H
