//
// Created by denis svinarchuk on 02.06.2018.
//

#ifndef SHELL_MIME_TYPES_H
#define SHELL_MIME_TYPES_H


#include <string>

namespace wallet_jsonrpc {
    namespace jsonrpc {

        namespace mime_types {

/// Convert a file extension into a MIME type.
            std::string extension_to_type(const std::string& extension);

        } // namespace mime_types
    }
}


#endif //SHELL_MIME_TYPES_H
