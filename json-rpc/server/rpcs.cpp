//
// Created by denis svinarchuk on 02.06.2018.
//

#include "rpcs.h"
#include "../api/api.h"

#define ARRAYLEN(array)     (sizeof(array)/sizeof((array)[0]))

namespace wallet_jsonrpc {
    namespace jsonrpc {

        static const struct handler rpc_handlers[] =
                {
                        {"ping",                ping_cxx},
                        {"create-wallet",       create_wallet_cxx},
                        {"get-wallet-keys",     get_wallet_keys_cxx},
                        {"get-wallet-state",    get_wallet_state_cxx},
                        {"get-blockchain-info", get_blockchain_info_cxx},
                        {"get-transfer-assets-transaction",     get_transfer_assets_transaction_cxx},
                        {"send-signed-transaction",     send_signed_transaction_cxx},
                        {"get-wallet-transactions", get_wallet_transactions_cxx}
                };

        void exec(json_spirit::Object &query, json_spirit::Object &result) {
            json_spirit::Value tmpval;

            tmpval = json_spirit::find_value(query, "version");

            result.push_back(json_spirit::Pair("jsonrpc", "2.0"));

            if (tmpval.type() != json_spirit::null_type)
                result.push_back(json_spirit::Pair("version", tmpval));
            else
                result.push_back(json_spirit::Pair("version", "1.0"));    // fb to 1.0

            tmpval = json_spirit::find_value(query, "id");

            result.push_back(json_spirit::Pair("id", tmpval));    // may be null

            tmpval = json_spirit::find_value(query, "method");


            if (tmpval.type() != json_spirit::str_type) {
                result.push_back(json_spirit::Pair("error", "invalid method"));
                return;
            }

            unsigned int i = 0;

            for (; i < ARRAYLEN(rpc_handlers); i++) {
                if (tmpval.get_str() == rpc_handlers[i].method) {

                    pcsa_result handle_result = rpc_handlers[i].actor(query, result);

                    if (handle_result == PCSA_RES_OK) return;

                    json_spirit::Object keys;

                    keys.push_back(json_spirit::Pair("message",""));
                    keys.push_back(json_spirit::Pair("code", (int) (handle_result)));

                    result.push_back(json_spirit::Pair("error", keys));

                    return;
                }
            }

            json_spirit::Object keys;

            keys.push_back(json_spirit::Pair("message", "unknown RPC method"));
            keys.push_back(json_spirit::Pair("code", -1));

            result.push_back(json_spirit::Pair("error", keys));
        }

    }    // namespace jsonrpc
}

