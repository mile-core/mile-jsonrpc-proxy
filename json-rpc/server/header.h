//
// Created by denis svinarchuk on 01.06.2018.
//

#ifndef SHELL_HEADER_H
#define SHELL_HEADER_H

#include <string>

namespace wallet_jsonrpc {
    namespace jsonrpc {

        struct header {
            std::string name;
            std::string value;

            header() {}

            header(std::string name_, std::string value_) {
                name = name_;
                value = value_;
            }
        };
    }
}

#endif //SHELL_HEADER_H
