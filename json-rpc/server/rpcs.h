//
// Created by denis svinarchuk on 02.06.2018.
//

#ifndef SHELL_RPCS_H
#define SHELL_RPCS_H

#include <unordered_set>
#include <unordered_map>

#include "connection.h"

#include "json_spirit_reader.h"
#include "json_spirit_writer.h"
#include "json_spirit_utils.h"
#include "json_spirit_value.h"

#include "csa_cxx.h"
#include "csa_type.h"

#define PACKAGE_NAME "mile-jsonrpcd"
#define PACKAGE_VERSION "0.3.1"

namespace wallet_jsonrpc {
    namespace jsonrpc {

        struct handler {
            std::string method;

            pcsa_result (*actor)(json_spirit::Object &query, json_spirit::Object &result);
        };

        extern void exec(json_spirit::Object &query, json_spirit::Object &result);

    }    // namespace jsonrpc
}


#endif //SHELL_RPCS_H
