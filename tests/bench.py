#

import requests
import json
import time
import threading

count = 1000

def main():
    url = "http://localhost:4000/v1/api"

    headers = {'content-type': 'application/json'}

    # Example echo method
    payload = {"method": "get-blockchain-info", "params":[], "id": 1, "jsonrpc": "2.0", "version": "1.0"}
    data = json.dumps(payload)

    def run_request():
        response = requests.post(url, data=data, headers=headers).json()
        assert (not response.get("error"))

    threads = []

    for i in range(0, count):
        t = threading.Thread(target=run_request, args=())
        threads.append(t)

    t0 = time.time()

    for t in threads:
        t.start()
        t.join()

    t1 = time.time()

    print("Total rate: %s rps" % (count/(t1-t0)))

if __name__ == "__main__":
    main()
