#!/usr/bin/env bash

curl  -H "Content-Type: application/json"  -d '{"method":"get-wallet-keys","params":{"wallet_name": "test-denn", "password": "1234567890"}, "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"get-blockchain-info","params":[], "id": 1, "jsonrpc": "2.0", "version": "1.0"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"ping","params":[], "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"create-wallet","params":{"wallet_name": "test-denn5", "password": "1234567890"}, "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api

#curl  -H "Content-Type: application/json"  -d '{"method":"transfer-assets","params":{"asset": "XDR", "amount": "0.001", "source_wallet_name": "test-denn", "destination_wallet": "FRtDNXjsvUGfumq1Ed3BuAs8sXaR4yskL2expT9D5g3nLH84a", "password": "1234567890"}, "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api
#
#curl  -H "Content-Type: application/json"  -d '{"method":"transfer-assets","params":{"asset": "XDR", "amount": "0.001", "from": "zVG4iPaggWUUaDEkyEyFBv8dNYSaFMm2C7WS8nSMKWLsSh9x", "to": "FRtDNXjsvUGfumq1Ed3BuAs8sXaR4yskL2expT9D5g3nLH84a", "private_key": "SBFejyKP4Yjivkyen3AGPesxovJbgfFB5xDv9qWoxHqsBXxVgWEDybUz3H3RaTxCoED4pTHVrgSD5Ej41AQTULx8xdviN"}, "id": 1, "jsonrpc": "2.0", "version": "1"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"get-wallet-state","params":{"public_key": "FRtDNXjsvUGfumq1Ed3BuAs8sXaR4yskL2expT9D5g3nLH84a"}, "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"get-transfer-assets-transaction","params":{"asset": "XDR", "amount": "0.001", "from": "zVG4iPaggWUUaDEkyEyFBv8dNYSaFMm2C7WS8nSMKWLsSh9x", "to": "FRtDNXjsvUGfumq1Ed3BuAs8sXaR4yskL2expT9D5g3nLH84a", "private_key": "SBFejyKP4Yjivkyen3AGPesxovJbgfFB5xDv9qWoxHqsBXxVgWEDybUz3H3RaTxCoED4pTHVrgSD5Ej41AQTULx8xdviN"}, "id": 1, "jsonrpc": "2.0", "version": "1"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"send-signed-transaction","params":{"transaction_data": "1c000000000000000100024023ef83b11f80eab20cae7ecbb971142a326005a8bd2671abd9ef12bdd30120c34a06fed40b56644fbf2f3844f7d717941faf15fc8e1b10b98453e328ffd9efa7c64b378941000000000000000000018524403eb2ec74b1812fbf653731776e5c0064c4488385024a53c8b9879a549fd3f59412766f8fb501909faba215afa2a1859642c012a46c547f63a36bfd4e00"}, "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api

curl  -H "Content-Type: application/json"  -d '{"method":"send-signed-transaction","params":{"transaction_data": "21000000000000000100024023ef83b11f80eab20cae7ecbb971142a326005a8bd2671abd9ef12bdd30120c34a06fed40b56644fbf2f3844f7d717941faf15fc8e1b10b98453e328ffd9efa7c64b3789410000000000000000000109ce1bee1340c534d30039a5b658d039f543039f2fa329857c5fae2c75e5713f1ba5f3a71e5e634244f749c95a20ed08ba165da60a5bdab550603618fa46150"}, "id": 1, "jsonrpc": "2.0", "version": "10"}' http://localhost:4000/v1/api