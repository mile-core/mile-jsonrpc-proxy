# About
Network lib and api for blockchain.

## How to build

To build project use cmake in `build` directory. cmake supported next arguments:

cmake argument | Description   | Default
-------------- | ------------- | --------
`LIGHT_BUILD`       |Build light api. Without network functional. | 0
`ASIO_STANDALONE`   |`LIGHT_BUILD` must be disable. Enable asio without dependency |0
`WITH_CONTAINER`    |Build with containers and openssl | 1
`DEBUG_BUILD`       |Debug build | 0
`SANITIZER`         |Enable sanitizer only in debug mode |0

