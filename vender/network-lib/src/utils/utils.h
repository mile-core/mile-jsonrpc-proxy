#pragma once
#include <string>

#define CHECK_CONDITION(condition, errorMessage, error)\
                if (condition)\
                {\
                    LogAndSafeError(errorMessage, error);\
                    return PCSA_RES_FAIL;\
                }

const std::string StringFormat(const char* format, ...);
void LogAndSafeError(char** errorMessage, const char* msg);