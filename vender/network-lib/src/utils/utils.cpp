#include <stdarg.h>
#include <cstring>

#include "utils.h"
#include "csa_pdbg.h"

const std::string StringFormat(const char* format, ...)
{
    char buffer[1024] = {};
    va_list ap = {};

    va_start(ap, format);
    vsnprintf(buffer, sizeof(buffer), format, ap);
    va_end(ap);

    return std::string(buffer);
}

void LogAndSafeError(char** errorMessage, const char* msg)
{
    PDBG_ERR(msg);
    *errorMessage = nullptr;
    size_t size = strlen(msg);
    if (!size)
    {
        return;
    }

    *errorMessage = (char *) malloc(size + 1);
    if (*errorMessage == nullptr)
    {
        return;
    }

    strncpy(*errorMessage, msg, size);
    (*errorMessage)[size] = '\0';
}