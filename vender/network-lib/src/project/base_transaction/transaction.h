#ifndef _BASE_TRANSACTION_H_
#define _BASE_TRANSACTION_H_

#include <utility>
#include <vector>

#include "crypto_types.h"
#include "serializer.h"
#include "deserializer.h"
#include "crypto.h"

class Transaction
{
    uint64_t         m_id;
    Digest           m_digest;        // transaction signature
    Signature        m_signature;        // transaction signature
    // additional variables
    bool             m_signed;           // transaction is signed
protected:
    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const = 0;
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const = 0;
    virtual void SerializeBody(BSerializer& serializer) const { }
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription) { return true; }
    //
    Digest CalculateDigest() const;
    void Close();
public:
    Transaction(const uint64_t& id);
    virtual ~Transaction() {};

    virtual unsigned short Code() const = 0;
    virtual string Name() const = 0;
    // get transaction size
    virtual unsigned short Size() const;

    void Serialize(BSerializer& serializer) const;
    bool Deserialize(BDeserializer& deserializer, string& errorDescription);
    void Description(std::vector<std::pair<std::string, std::string>>& description) const;

    string ToHexString() const;
    bool SetHexString(const string& s, string& errorDescription);
    void Sign(const Signer& signer);
};

#endif