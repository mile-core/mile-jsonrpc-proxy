#include "connection.h"
#include "connection_msg.h"
#include "csa_impl.h"
#include "csa_type.h"
#include "csa_cxx.h"
#include "crypto.h"
#include "utils.h"
#include "csa_pdbg.h"
#include "csa_cxx_light.h"

using namespace Engine;

static pcsa_result transaction_prepair(const char* walletPublicKey, 
                                       uint64_t& id)
{
    PublicKey publicKey;
    std::string errorDescription;
    if (!publicKey.SetBase58CheckString(walletPublicKey, errorDescription))
    {
        SetErrorText(StringFormat("Public key has invalide base58 encode: '%s'", errorDescription.c_str()));
        return PCSA_RES_FAIL;
    }

    if (get_connection()->check_transaction_cache(publicKey, id))
    {
        return PCSA_RES_OK;
    }

    pcsa_wallet_state state;
    int result = get_wallet_state_by_key(walletPublicKey, &state);
    if (state.assets != nullptr)
    {
        free(state.assets);
    }

    if (result != PCSA_RES_OK)
    {
        SetErrorText("Can't get last transaction id");
        return PCSA_RES_FAIL;
    }

    if (!StringToUInt64(state.last_transaction_id, id, false))
    {
        SetErrorText(StringFormat("Can't get last transaction id from string: %s", state.last_transaction_id));
        return PCSA_RES_FAIL;   
    }

    get_connection()->set_transaction_cache(publicKey, id + 1);

    return PCSA_RES_OK;
}

pcsa_result  register_node_transaction(const pcsa_keys_pair* keyPair, 
                                       const char* nodeAddress)
{
    try
    {
        if (keyPair == nullptr)
        {
            SetErrorText("Key pairs must me specified");
            return PCSA_RES_FAIL;
        }

        uint64_t id = 0;
        if (transaction_prepair(keyPair->public_key, id))
        {
            return PCSA_RES_FAIL;
        }

        char* transaction = nullptr;
        char* errorMessage = nullptr;

        if (create_transaction_register_node(keyPair, nodeAddress, id, &transaction, &errorMessage))
        {
            if (errorMessage != nullptr)
            {
                SetErrorText(errorMessage);
                free(errorMessage);
            }
            return PCSA_RES_FAIL;
        }

        if (transaction == nullptr)
        {
            SetErrorText("Internal error: transaction not created");
            return PCSA_RES_FAIL;
        }

        std::string trans(transaction);
        free(transaction);

        XMsgOut mo; XMsgIn mi;

        mo.set_op("add-transaction")
          .add("transaction_name", "RegisterNodeTransaction")
          .add("transaction_data", trans);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  unregister_node_transaction(const pcsa_keys_pair* keyPair)
{
    try
    {
        if (keyPair == nullptr)
        {
            SetErrorText("Key pairs must me specified");
            return PCSA_RES_FAIL;
        }

        uint64_t id = 0;
        if (transaction_prepair(keyPair->public_key, id))
        {
            return PCSA_RES_FAIL;
        }

        char* transaction = nullptr;
        char* errorMessage = nullptr;

        if (create_transaction_unregister_node(keyPair, id, &transaction, &errorMessage))
        {
            if (errorMessage != nullptr)
            {
                SetErrorText(errorMessage);
                free(errorMessage);
            }
            return PCSA_RES_FAIL;
        }

        if (transaction == nullptr)
        {
            SetErrorText("Internal error: transaction not created");
            return PCSA_RES_FAIL;
        }

        std::string trans(transaction);
        free(transaction);

        XMsgOut mo; XMsgIn mi;
        mo.set_op("add-transaction")
          .add("transaction_name", "UnregisterNodeTransaction")
          .add("transaction_data", trans);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  transfer_assets_transaction(const pcsa_keys_pair* srcKeyPair, 
                                         const char* dstWalletPublicKey,
                                         unsigned short assets,
                                         const char* amount)
{
    try
    {
        if (dstWalletPublicKey == nullptr)
        {
            SetErrorText("Destination public key must be specified");
            return PCSA_RES_FAIL;
        }

        if (srcKeyPair == nullptr)
        {
            SetErrorText("Key pairs must me specified");
            return PCSA_RES_FAIL;
        }        

        uint64_t id = 0;
        if (transaction_prepair(srcKeyPair->public_key, id))
        {
            return PCSA_RES_FAIL;
        }
        char* transaction = nullptr;
        char* errorMessage = nullptr;

        if (create_transaction_transfer_assets(srcKeyPair, dstWalletPublicKey, id, 
                                               assets, amount, &transaction, &errorMessage))
        {
            if (errorMessage != nullptr)
            {
                SetErrorText(errorMessage);
                free(errorMessage);
            }
            return PCSA_RES_FAIL;
        }
        if (transaction == nullptr)
        {
            SetErrorText("Internal error: transaction not created");
            return PCSA_RES_FAIL;
        }
        std::string trans(transaction);
        free(transaction);

        XMsgOut mo; XMsgIn mi;
        mo.set_op("add-transaction")
          .add("transaction_name", "TransferAssetsTransaction")
          .add("transaction_data", trans);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  user_data_transaction(const pcsa_keys_pair* keyPair,
                                   const char* userData,
                                   unsigned int length)
{
    try
    {
        if (keyPair == nullptr)
        {
            SetErrorText("Key pairs must me specified");
            return PCSA_RES_FAIL;
        }  

        uint64_t id = 0;
        if (transaction_prepair(keyPair->public_key, id))
        {
            return PCSA_RES_FAIL;
        }

        char* transaction = nullptr;
        char* errorMessage = nullptr;

        if (create_transaction_user_data(keyPair, id, userData, &transaction, &errorMessage))
        {
            if (errorMessage != nullptr)
            {
                SetErrorText(errorMessage);
                free(errorMessage);
            }
            return PCSA_RES_FAIL;
        }

        if (transaction == nullptr)
        {
            SetErrorText("Internal error: transaction not created");
            return PCSA_RES_FAIL;
        }

        std::string trans(transaction);
        free(transaction);

        XMsgOut mo; XMsgIn mi;
        mo.set_op("add-transaction")
          .add("transaction_name", "UserDataTransaction")
          .add("transaction_data", trans);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result register_system_wallet_transaction(const pcsa_keys_pair* srcKeyPair, 
                                               const char* dstWalletPublicKey,
                                               unsigned char walletTag)
{
    try
    {
        if (dstWalletPublicKey == nullptr)
        {
            SetErrorText("Destination public key must be specified");
            return PCSA_RES_FAIL;
        }

        if (srcKeyPair == nullptr)
        {
            SetErrorText("Key pairs must me specified");
            return PCSA_RES_FAIL;
        }        

        uint64_t id = 0;
        if (transaction_prepair(srcKeyPair->public_key, id))
        {
            return PCSA_RES_FAIL;
        }
        char* transaction = nullptr;
        char* errorMessage = nullptr;

        if (create_transaction_register_system_wallet(srcKeyPair, dstWalletPublicKey, id, 
                                                      walletTag, &transaction, &errorMessage))
        {
            if (errorMessage != nullptr)
            {
                SetErrorText(errorMessage);
                free(errorMessage);
            }
            return PCSA_RES_FAIL;
        }

        if (transaction == nullptr)
        {
            SetErrorText("Internal error: transaction not created");
            return PCSA_RES_FAIL;
        }

        std::string trans(transaction);
        free(transaction);

        XMsgOut mo; XMsgIn mi;
        mo.set_op("add-transaction")
          .add("transaction_name", "RegisterSystemWalletTransaction")
          .add("transaction_data", trans);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  unregister_system_wallet_transaction(const pcsa_keys_pair* srcKeyPair, 
                                         const char* dstWalletPublicKey,
                                         unsigned char walletTag)
{
    try
    {
        if (dstWalletPublicKey == nullptr)
        {
            SetErrorText("Destination public key must be specified");
            return PCSA_RES_FAIL;
        }

        if (srcKeyPair == nullptr)
        {
            SetErrorText("Key pairs must me specified");
            return PCSA_RES_FAIL;
        }        

        uint64_t id = 0;
        if (transaction_prepair(srcKeyPair->public_key, id))
        {
            return PCSA_RES_FAIL;
        }
        char* transaction = nullptr;
        char* errorMessage = nullptr;

        if (create_transaction_unregister_system_wallet(srcKeyPair, dstWalletPublicKey, id, 
                                                        walletTag, &transaction, &errorMessage))
        {
            if (errorMessage != nullptr)
            {
                SetErrorText(errorMessage);
                free(errorMessage);
            }
            return PCSA_RES_FAIL;
        }

        if (transaction == nullptr)
        {
            SetErrorText("Internal error: transaction not created");
            return PCSA_RES_FAIL;
        }

        std::string trans(transaction);
        free(transaction);

        XMsgOut mo; XMsgIn mi;
        mo.set_op("add-transaction")
          .add("transaction_name", "UnregisterSystemWalletTransaction")
          .add("transaction_data", trans);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}