#include "project/base_transaction/transaction.h"

Transaction::Transaction(const uint64_t& id)
    : m_id(id)
    , m_signed(false)
{
    m_digest.Clear();
    m_signature.Clear();
}

unsigned short Transaction::Size() const
{
    // 8 bytes for transaction ID, signed flag and possible signature size or digest size for unsigned transactions e.g. in genesis block
    return sizeof(uint64_t) + sizeof(unsigned char) + static_cast<unsigned short>(m_signed ? m_signature.Size() : m_digest.Size());
}

void Transaction::Serialize(BSerializer& serializer) const
{
    serializer << m_id;
    SerializeBody(serializer);
    // serialize signed flag because transactions inside genesis block can be unsigned 
    serializer << m_signed;
    if (m_signed)
    {
        serializer << m_signature;
    }
    else
    {
        serializer << m_digest;
    }
}

bool Transaction::Deserialize(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_id;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize transaction ID: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    if (!DeserializeBody(deserializer, errorDescription))
    {
        return false;
    }

    deserializer >> m_signed;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize transaction signed flag: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    if (m_signed)
    {
        deserializer >> m_signature;

        if (deserializer.ErrorState())
        {
            errorDescription = StringFormat("couldn't deserialize transaction signature: %s", deserializer.ErrorDescription().c_str());
            return false;
        }

        // digest calculation is not needed for signed transactions because digest should be recalculated each time during transaction checking
        m_digest = CalculateDigest();
    }
    else // not signed transaction
    {
        deserializer >> m_digest;

        if (deserializer.ErrorState())
        {
            errorDescription = StringFormat("couldn't deserialize transaction digest: %s", deserializer.ErrorDescription().c_str());
            return false;
        }

        Digest calculatedDigest = CalculateDigest();

        if (m_digest != calculatedDigest)
        {
            errorDescription = StringFormat("bad deserialized transaction digest: '%s' <> calculated digest '%s'", m_digest.ToBase58CheckString().c_str(), calculatedDigest.ToBase58CheckString().c_str());
            return false;
        }
    }

    return true;
}

string Transaction::ToHexString() const
{
    BSerializer serializer; // write mode and little endian format
    // serialize transaction data
    Serialize(serializer);
    // encode transaction data
    return serializer.toHex();
}

bool Transaction::SetHexString(const string& s, string& errorDescription)
{
    vector<unsigned char> data;

    if (!HexToBin(s, data, errorDescription))
    {
        errorDescription = StringFormat("couldn't deserialize '%s' from hex string: %s", Name().c_str(), errorDescription.c_str());
        return false;
    }

    BDeserializer deserializer(data); // offset=0 and little endian format

    if (!Deserialize(deserializer, errorDescription))
    {
        errorDescription = StringFormat("couldn't deserialize '%s' from hex string: %s", Name().c_str(), errorDescription.c_str());
        return false;
    }

    return true;
}

void Transaction::Description(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("id", UInt64ToDecString(m_id)));
    description.push_back(std::make_pair("name", Name()));
    description.push_back(std::make_pair("code", std::to_string(Code())));
    MakeDescription(description);
    description.push_back(std::make_pair("signed", (m_signed ? "true" : "false")));
    if (m_signed)
    {
        description.push_back(std::make_pair("signature", m_signature.ToHexString()));
    } else
    {
        description.push_back(std::make_pair("digest", m_digest.ToHexString()));
    }
}

Digest Transaction::CalculateDigest() const
{
    Digest digest;
    DigestCalculator digestCalculator;

    digestCalculator.Initialize();
    digestCalculator.Update(m_id); // process transaction ID
    //
    CalculateBodyDigest(digestCalculator);
    //
    digestCalculator.Finalize(digest);

    return digest;
}

void Transaction::Close()
{
    m_digest = CalculateDigest();
    m_signed = false;
}

void Transaction::Sign(const Signer& signer)
{
    Close();

    // sign transaction digest
    signer.SignDigest(m_digest, m_signature);

    m_signed = true;
}