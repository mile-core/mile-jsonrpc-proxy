#include <string>
#include <cstring>

#include "crypto_types.h"
#include "project/base_transaction/blockchain_transaction.h"
#include "crypto.h"
#include "csa_cxx_light.h"
#include "csa_type_light.h"
#include "utils.h"

static pcsa_result light_transaction_prepair(const pcsa_keys_pair* keyPair,
                                           PublicKey* publicKey,
                                           PrivateKey* privateKey,
                                           char** errorMessage)
{

    std::string error;
    if (!publicKey->SetBase58CheckString(keyPair->public_key, error))
    {
        LogAndSafeError(errorMessage, StringFormat("Public key has invalide base58 encode: '%s'", error.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    if (!privateKey->SetBase58CheckString(keyPair->private_key, error))
    {
        LogAndSafeError(errorMessage, StringFormat("Private key has invalide base58 encode: '%s'", error.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result create_transaction_register_node(const pcsa_keys_pair* keyPair, 
                                             const char* nodeAddress,
                                             uint64_t id,
                                             char** transaction,
                                             char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (nodeAddress == nullptr)
    {
        LogAndSafeError(errorMessage, "Node address is not define");
        return PCSA_RES_FAIL;
    } 

    size_t nodeAddressLength = strlen(nodeAddress);
    if (nodeAddressLength > 64 || nodeAddressLength == 0)
    {
        LogAndSafeError(errorMessage, StringFormat("Node address length must be in [1..64] but length is '%zu'", nodeAddressLength).c_str());
        return PCSA_RES_FAIL;
    }

    PublicKey publicKey;
    PrivateKey privateKey;
    if (light_transaction_prepair(keyPair, &publicKey, &privateKey, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    RegisterNodeTransaction nodeTransaction(id + 1, publicKey, nodeAddress);
    Signer signer(privateKey, publicKey);
    nodeTransaction.Sign(signer);

    std::string trans{nodeTransaction.ToHexString()};

    *transaction = (char *) malloc(trans.length() + 1);
    CHECK_CONDITION(*transaction == nullptr, errorMessage, "Allocation failed");
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result create_transaction_unregister_node(const pcsa_keys_pair* keyPair,
                                               uint64_t id,
                                               char** transaction,
                                               char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    PublicKey publicKey;
    PrivateKey privateKey;
    if (light_transaction_prepair(keyPair, &publicKey, &privateKey, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    UnregisterNodeTransaction unNodeTransaction(id + 1, publicKey);
    Signer signer(privateKey, publicKey);
    unNodeTransaction.Sign(signer);

    std::string trans{unNodeTransaction.ToHexString()};

    *transaction = (char *) malloc(trans.length() + 1);
    CHECK_CONDITION(*transaction == nullptr, errorMessage, "Allocation failed");
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result create_transaction_transfer_assets(const pcsa_keys_pair* srcKeyPair,
                                               const char* dstWalletPublicKey,
                                               uint64_t id,
                                               unsigned short assets,
                                               const char* amount,
                                               char** transaction,
                                               char** errorMessage)
{

    if (srcKeyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (dstWalletPublicKey == nullptr)
    {
        LogAndSafeError(errorMessage, "Destination wallet private key is not define");
        return PCSA_RES_FAIL;  
    }

    if (amount == nullptr)
    {
        LogAndSafeError(errorMessage, "Amount is not define");
        return PCSA_RES_FAIL;  
    }


    PublicKey srcPublicKey;
    PrivateKey srcPrivateKey;
    if (light_transaction_prepair(srcKeyPair, &srcPublicKey, &srcPrivateKey, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    PublicKey dstPublicKey;
    std::string errorDescription;
    if (!dstPublicKey.SetBase58CheckString(dstWalletPublicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Public key has invalide base58 encode: '%s'", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    blockchain_amount_t amountType;
    if (!StringToBlockchainAmount(amount, amountType))
    {
        LogAndSafeError(errorMessage, StringFormat("Could't parse amount string %s", amount).c_str());
        return PCSA_RES_FAIL;
    }

    TransferAssetsTransaction transferAssets(id + 1, assets, srcPublicKey, dstPublicKey, amountType);
    Signer signer(srcPrivateKey, srcPublicKey);
    transferAssets.Sign(signer);

    std::string trans{transferAssets.ToHexString()};

    *transaction = (char *) malloc(trans.length() + 1);
    CHECK_CONDITION(*transaction == nullptr, errorMessage, "Allocation failed");
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result  create_transaction_user_data(const pcsa_keys_pair* keyPair,
                                          uint64_t id,
                                          const char* dataString,
                                          char** transaction,
                                          char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (dataString == nullptr || *dataString == '\0')
    {
        LogAndSafeError(errorMessage, "User data is not define");
        return PCSA_RES_FAIL;  
    }

    std::vector<unsigned char> data;

    while (*dataString)
    {
        data.push_back((unsigned char) *dataString++);
    }
    
    PublicKey publicKey;
    PrivateKey privateKey;

    if (light_transaction_prepair(keyPair, &publicKey, &privateKey, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    UserDataTransaction dataTransaction(id + 1, publicKey, data);
    Signer signer(privateKey, publicKey);
    dataTransaction.Sign(signer);

    std::string trans{dataTransaction.ToHexString()};

    *transaction = (char *) malloc(trans.length() + 1);
    CHECK_CONDITION(*transaction == nullptr, errorMessage, "Allocation failed");
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result create_transaction_register_system_wallet(const pcsa_keys_pair* srcKeyPair,
                                                       const char* dstWalletPublicKey,
                                                       uint64_t id,
                                                       unsigned char walletTag,
                                                       char** transaction,
                                                       char** errorMessage)
{

    if (srcKeyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (dstWalletPublicKey == nullptr)
    {
        LogAndSafeError(errorMessage, "Destination wallet private key is not define");
        return PCSA_RES_FAIL;  
    }

    PublicKey srcPublicKey;
    PrivateKey srcPrivateKey;
    if (light_transaction_prepair(srcKeyPair, &srcPublicKey, &srcPrivateKey, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    PublicKey dstPublicKey;
    std::string errorDescription;
    if (!dstPublicKey.SetBase58CheckString(dstWalletPublicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Public key has invalide base58 encode: '%s'", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    RegisterSystemWalletTransaction registerSystemWallet(id + 1, srcPublicKey, dstPublicKey, walletTag);
    Signer signer(srcPrivateKey, srcPublicKey);
    registerSystemWallet.Sign(signer);

    std::string trans{registerSystemWallet.ToHexString()};

    *transaction = (char *) malloc(trans.length() + 1);
    CHECK_CONDITION(*transaction == nullptr, errorMessage, "Allocation failed");
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}

pcsa_result create_transaction_unregister_system_wallet(const pcsa_keys_pair* srcKeyPair,
                                                       const char* dstWalletPublicKey,
                                                       uint64_t id,
                                                       unsigned char walletTag,
                                                       char** transaction,
                                                       char** errorMessage)
{

    if (srcKeyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Key pairs is not define");
        return PCSA_RES_FAIL;
    } 

    if (dstWalletPublicKey == nullptr)
    {
        LogAndSafeError(errorMessage, "Destination wallet private key is not define");
        return PCSA_RES_FAIL;  
    }

    PublicKey srcPublicKey;
    PrivateKey srcPrivateKey;
    if (light_transaction_prepair(srcKeyPair, &srcPublicKey, &srcPrivateKey, errorMessage))
    {
        return PCSA_RES_FAIL;
    }

    PublicKey dstPublicKey;
    std::string errorDescription;
    if (!dstPublicKey.SetBase58CheckString(dstWalletPublicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Public key has invalide base58 encode: '%s'", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    UnregisterSystemWalletTransaction unregisterSystemWallet(id + 1, srcPublicKey, dstPublicKey, walletTag);
    Signer signer(srcPrivateKey, srcPublicKey);
    unregisterSystemWallet.Sign(signer);

    std::string trans{unregisterSystemWallet.ToHexString()};

    *transaction = (char *) malloc(trans.length() + 1);
    CHECK_CONDITION(*transaction == nullptr, errorMessage, "Allocation failed");
    strncpy(*transaction, trans.c_str(), trans.length());
    (*transaction)[trans.length()] = '\0';

    return PCSA_RES_OK;
}