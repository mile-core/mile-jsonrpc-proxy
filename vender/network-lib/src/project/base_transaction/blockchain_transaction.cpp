#include "project/base_transaction/blockchain_transaction.h"

// RegisterNodeTransaction
RegisterNodeTransaction::RegisterNodeTransaction()
    : Transaction(0) // not initialized?
{
}

RegisterNodeTransaction::RegisterNodeTransaction(const uint64_t& id, const PublicKey& nodeWalletPublicKey, const string& nodeAddress)
    : Transaction(id)
    , m_nodeWalletPublicKey(nodeWalletPublicKey)
    , m_nodeAddress(nodeAddress)
{
    if (m_nodeAddress.size() > cMaxDomainNameLength)
    {
        m_nodeAddress.resize(cMaxDomainNameLength);
    }
}

void RegisterNodeTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_nodeWalletPublicKey);
    digestCalculator.Update(m_nodeAddress, cMaxDomainNameLength); // cut domain string if needed
}

void RegisterNodeTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_nodeWalletPublicKey << setw(64) << setfill('\0') << m_nodeAddress.c_str();
}

bool RegisterNodeTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_nodeWalletPublicKey;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize node wallet public key: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    Array<cMaxDomainNameLength> nodeAddressData;

    // deserialize array with known fixed size
    deserializer >> nodeAddressData;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize node address: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    // cut possible zeros at the end of vector
    // note: node address data size should be non-zero
    for (size_t i = 0; i < nodeAddressData.Size() && nodeAddressData.Data[i]; ++i)
    {
        m_nodeAddress.push_back(nodeAddressData.Data[i]);    
    }
 
    return true;
}

unsigned short RegisterNodeTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_nodeWalletPublicKey.Size() + cMaxDomainNameLength);
}

unsigned short RegisterNodeTransaction::Code() const
{
    return eRegisterNode;
}

void RegisterNodeTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("nodeWalletPublicKey", m_nodeWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("nodeAddress", m_nodeAddress));
}

string RegisterNodeTransaction::Name() const
{
    return "RegisterNodeTransaction";
}

// UnregisterNodeTransaction

UnregisterNodeTransaction::UnregisterNodeTransaction()
    : Transaction(0) // not initialized?
{
}

UnregisterNodeTransaction::UnregisterNodeTransaction(const uint64_t& id, const PublicKey& nodeWalletPublicKey)
    : Transaction(id)
    , m_nodeWalletPublicKey(nodeWalletPublicKey)
{
}

void UnregisterNodeTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_nodeWalletPublicKey);
}

void UnregisterNodeTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_nodeWalletPublicKey;
}

bool UnregisterNodeTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_nodeWalletPublicKey;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize node wallet public key: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    return true;
}

unsigned short UnregisterNodeTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_nodeWalletPublicKey.Size());
}

unsigned short UnregisterNodeTransaction::Code() const
{
    return eUnregisterNode;
}

void UnregisterNodeTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("nodeWalletPublicKey", m_nodeWalletPublicKey.ToHexString()));
}

string UnregisterNodeTransaction::Name() const
{
    return "UnregisterNodeTransaction";
}

// RegisterSystemWalletTransaction
RegisterSystemWalletTransaction::RegisterSystemWalletTransaction()
    : Transaction(0) // not initialized?
    , m_systemWalletTag(0)
{
    m_supervisorWalletPublicKey.Clear();
    m_systemWalletPublicKey.Clear();
}

RegisterSystemWalletTransaction::RegisterSystemWalletTransaction(const uint64_t& id, const PublicKey& supervisorWalletPublicKey, const PublicKey& systemWalletPublicKey, unsigned char systemWalletTag)
    : Transaction(id)
    , m_supervisorWalletPublicKey(supervisorWalletPublicKey)
    , m_systemWalletPublicKey(systemWalletPublicKey)
    , m_systemWalletTag(systemWalletTag)
{
}

void RegisterSystemWalletTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_supervisorWalletPublicKey);
    digestCalculator.Update(m_systemWalletPublicKey);
    digestCalculator.Update(m_systemWalletTag);
}

void RegisterSystemWalletTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_supervisorWalletPublicKey << m_systemWalletPublicKey << m_systemWalletTag;
}

bool RegisterSystemWalletTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_supervisorWalletPublicKey >> m_systemWalletPublicKey >> m_systemWalletTag;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize supervisor wallet public key, system wallet public key or system wallet tag: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    return true;
}

void RegisterSystemWalletTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("supervisorWalletPublicKey", m_supervisorWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("systemWalletPublicKey", m_systemWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("systemWalletTag", std::to_string(m_systemWalletTag)));
}

unsigned short RegisterSystemWalletTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_supervisorWalletPublicKey.Size() + m_systemWalletPublicKey.Size() + sizeof(unsigned char));
}

unsigned short RegisterSystemWalletTransaction::Code() const
{
    return eRegisterSystemWallet;
}

string RegisterSystemWalletTransaction::Name() const
{
    return "RegisterSystemWalletTransaction";
}

// UnregisterSystemWalletTransaction
UnregisterSystemWalletTransaction::UnregisterSystemWalletTransaction()
    : Transaction(0) // not initialized?
    , m_systemWalletTag(0)
{
    m_supervisorWalletPublicKey.Clear();
    m_systemWalletPublicKey.Clear();
}

UnregisterSystemWalletTransaction::UnregisterSystemWalletTransaction(const uint64_t& id, const PublicKey& supervisorWalletPublicKey, const PublicKey& systemWalletPublicKey, unsigned char systemWalletTag)
    : Transaction(id)
    , m_supervisorWalletPublicKey(supervisorWalletPublicKey)
    , m_systemWalletPublicKey(systemWalletPublicKey)
    , m_systemWalletTag(systemWalletTag)
{
}

void UnregisterSystemWalletTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_supervisorWalletPublicKey);
    digestCalculator.Update(m_systemWalletPublicKey);
    digestCalculator.Update(m_systemWalletTag);
}

void UnregisterSystemWalletTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_supervisorWalletPublicKey << m_systemWalletPublicKey << m_systemWalletTag;
}

bool UnregisterSystemWalletTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_supervisorWalletPublicKey >> m_systemWalletPublicKey >> m_systemWalletTag;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize supervisor wallet public key, system wallet public key or system wallet tag: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    return true;
}

void UnregisterSystemWalletTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("supervisorWalletPublicKey", m_supervisorWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("systemWalletPublicKey", m_systemWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("systemWalletTag", std::to_string(m_systemWalletTag)));
}

unsigned short UnregisterSystemWalletTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_supervisorWalletPublicKey.Size() + m_systemWalletPublicKey.Size() + sizeof(unsigned char));
}

unsigned short UnregisterSystemWalletTransaction::Code() const
{
    return eRegisterSystemWallet;
}

string UnregisterSystemWalletTransaction::Name() const
{
    return "UnregisterSystemWalletTransaction";
}


//

UserDataTransaction::UserDataTransaction()
    : Transaction(0) // not initialized?
{
    m_userWalletPublicKey.Clear();
    m_userData.clear();
}

UserDataTransaction::UserDataTransaction(const uint64_t& id, const PublicKey& userWalletPublicKey, const vector<unsigned char>& userData)
    : Transaction(id)
    , m_userWalletPublicKey(userWalletPublicKey)
    , m_userData(userData)
{
}

void UserDataTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_userData);
}

void UserDataTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_userWalletPublicKey << m_userData;
}

bool UserDataTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_userWalletPublicKey >> m_userData;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize user data transaction: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    return true;
}

void UserDataTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("userWalletPublicKey", m_userWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("userData", std::string(m_userData.begin(), m_userData.end())));
}

unsigned short UserDataTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_userWalletPublicKey.Size() + 4 + m_userData.size()); // user data size + user data
}

unsigned short UserDataTransaction::Code() const
{
    return eUserData;
}

string UserDataTransaction::Name() const
{
    return "UserDataTransaction";
}

// TransferAssetsTransaction
TransferAssetsTransaction::TransferAssetsTransaction()
    : Transaction(0) // not initialized?
    , m_assetCode(0)
    , m_amount(0)
{
}

TransferAssetsTransaction::TransferAssetsTransaction(const uint64_t& id, unsigned short assetCode, const PublicKey& sourceWalletPublicKey, const PublicKey& destinationWalletPublicKey, const blockchain_amount_t& amount)
    : Transaction(id)
    , m_assetCode(assetCode)
    , m_sourceWalletPublicKey(sourceWalletPublicKey)
    , m_destinationWalletPublicKey(destinationWalletPublicKey)
    , m_amount(amount)
{
}

void TransferAssetsTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    // add tokes/coins serialization
    digestCalculator.Update(m_assetCode);
    digestCalculator.Update(m_sourceWalletPublicKey);
    digestCalculator.Update(m_destinationWalletPublicKey);
    // serialize blockchain amount
    blockchain_amount_data_t amountData;
    SerializeBlockchainAmount(m_amount, amountData);
    // process blockchain amount to update digest
    digestCalculator.Update(amountData);
}

void TransferAssetsTransaction::SerializeBody(BSerializer& serializer) const
{
    blockchain_amount_data_t amountData;
    SerializeBlockchainAmount(m_amount, amountData);
    serializer << m_assetCode << m_sourceWalletPublicKey << m_destinationWalletPublicKey << amountData;
}

bool TransferAssetsTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_assetCode >> m_sourceWalletPublicKey >> m_destinationWalletPublicKey;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize asset code, source or destionation wallet public key: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    blockchain_amount_data_t amountData;
    deserializer >> amountData;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize blockchain amount data: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    DeserializeBlockchainAmount(amountData, m_amount);

    return true;
}

void TransferAssetsTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("assetCode", std::to_string(m_assetCode)));
    description.push_back(std::make_pair("sourceWalletPublicKey", m_sourceWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("destinationWalletPublicKey", m_destinationWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("amount", BlockchainAmountToString(m_amount)));
}

unsigned short TransferAssetsTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + sizeof(unsigned short) + m_sourceWalletPublicKey.Size() + m_destinationWalletPublicKey.Size() + eBlockchainAmountDataSize);
}

unsigned short TransferAssetsTransaction::Code() const
{
    return eTransferAssets;
}

string TransferAssetsTransaction::Name() const
{
    return "TransferAssetsTransaction";
}


DistributionFeeTransaction::DistributionFeeTransaction()
    : Transaction(0) // not initialized?
{
}

DistributionFeeTransaction::DistributionFeeTransaction(const uint64_t& id, const PublicKey& masterNodeWalletPublicKey,
                                                       const blockchain_amount_t& amountOfTokens, const blockchain_amount_t& amountOfCoins)
    : Transaction(id)
    , m_masterNodeWalletPublicKey(masterNodeWalletPublicKey)
    , m_amountOfTokens(amountOfTokens)
    , m_amountOfCoins(amountOfCoins)
{
}

void DistributionFeeTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_masterNodeWalletPublicKey);

    // serialize blockchain amount
    blockchain_amount_data_t amountData;
    SerializeBlockchainAmount(m_amountOfTokens, amountData);
    // process blockchain amount to update digest
    digestCalculator.Update(amountData);
    //
    SerializeBlockchainAmount(m_amountOfCoins, amountData);
    // process blockchain amount to update digest
    digestCalculator.Update(amountData);
}

void DistributionFeeTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_masterNodeWalletPublicKey;

    blockchain_amount_data_t amountData;
    //
    SerializeBlockchainAmount(m_amountOfTokens, amountData);
    serializer << amountData;
    //
    SerializeBlockchainAmount(m_amountOfCoins, amountData);
    serializer << amountData;
}

bool DistributionFeeTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_masterNodeWalletPublicKey;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize master node wallet public key: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    blockchain_amount_data_t amountData;

    deserializer >> amountData;
    //
    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize amount of tokens: %s", deserializer.ErrorDescription().c_str());
        return false;
    }
    DeserializeBlockchainAmount(amountData, m_amountOfTokens);

    deserializer >> amountData;
    //
    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize amount of coins: %s", deserializer.ErrorDescription().c_str());
        return false;
    }
    DeserializeBlockchainAmount(amountData, m_amountOfCoins);

    return true;
}

unsigned short DistributionFeeTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_masterNodeWalletPublicKey.Size() + eBlockchainAmountDataSize + eBlockchainAmountDataSize);
}

unsigned short DistributionFeeTransaction::Code() const
{
    return eDistributionFee;
}

string DistributionFeeTransaction::Name() const
{
    return "DistributionFeeTransaction";
}

void DistributionFeeTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("masterNodeWalletPublicKey", m_masterNodeWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("amountOfTokens", BlockchainAmountToString(m_amountOfTokens)));
    description.push_back(std::make_pair("amountOfCoins", BlockchainAmountToString(m_amountOfCoins)));
}

PublishCurrencyRateTransaction::PublishCurrencyRateTransaction()
    : Transaction(0) // not initialized?
    , m_currencyRate(0)
{
    m_ownerWalletPublicKey.Clear(); // not registered by default
}

PublishCurrencyRateTransaction::PublishCurrencyRateTransaction(const uint64_t& id, const PublicKey& ownerWalletPublicKey, const blockchain_amount_t& currencyRate)
    : Transaction(id)
    , m_ownerWalletPublicKey(ownerWalletPublicKey)
    , m_currencyRate(currencyRate)
{
}

void PublishCurrencyRateTransaction::CalculateBodyDigest(DigestCalculator& digestCalculator) const
{
    digestCalculator.Update(m_ownerWalletPublicKey);
    // serialize currency rate
    blockchain_amount_data_t amountData;
    SerializeBlockchainAmount(m_currencyRate, amountData);
    // process blockchain amount to update digest
    digestCalculator.Update(amountData);
}

void PublishCurrencyRateTransaction::SerializeBody(BSerializer& serializer) const
{
    serializer << m_ownerWalletPublicKey;

    blockchain_amount_data_t amountData;
    //
    SerializeBlockchainAmount(m_currencyRate, amountData);
    serializer << amountData;
}

bool PublishCurrencyRateTransaction::DeserializeBody(BDeserializer& deserializer, string& errorDescription)
{
    errorDescription = "";

    deserializer >> m_ownerWalletPublicKey;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize owner wallet public key: %s", deserializer.ErrorDescription().c_str());
        return false;
    }

    blockchain_amount_data_t amountData;

    deserializer >> amountData;
    //
    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize currency rate: %s", deserializer.ErrorDescription().c_str());
        return false;
    }
    DeserializeBlockchainAmount(amountData, m_currencyRate);

    return true;
}

unsigned short PublishCurrencyRateTransaction::Size() const
{
    return static_cast<unsigned short>(Transaction::Size() + m_ownerWalletPublicKey.Size() + eBlockchainAmountDataSize);
}

unsigned short PublishCurrencyRateTransaction::Code() const
{
    return ePublishCurrencyRate;
}

string PublishCurrencyRateTransaction::Name() const
{
    return "PublishCurrencyRateTransaction";
}

void PublishCurrencyRateTransaction::MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const
{
    description.push_back(std::make_pair("ownerWalletPublicKey", m_ownerWalletPublicKey.ToHexString()));
    description.push_back(std::make_pair("currencyRate", BlockchainAmountToString(m_currencyRate)));
}

std::unique_ptr<Transaction> deserializeTransaction(BDeserializer& deserializer, string& errorDescription)
{
    unsigned short code = 0;
    deserializer >> code;

    if (deserializer.ErrorState())
    {
        errorDescription = StringFormat("Couldn't deserialize transaction code: %s", deserializer.ErrorDescription().c_str());
        return {};
    }

    std::unique_ptr<Transaction> transaction;
    switch(code)
    {
        case eRegisterNode:
            transaction.reset(new RegisterNodeTransaction());
            break;
        case eUnregisterNode:
            transaction.reset(new UnregisterNodeTransaction());
            break;
        case eRegisterSystemWallet:
            transaction.reset(new RegisterSystemWalletTransaction());
            break;
        case eUnregisterSystemWallet:
            transaction.reset(new UnregisterSystemWalletTransaction());
            break;
        case eUserData:
            transaction.reset(new UserDataTransaction());
            break;
        case eTransferAssets:
            transaction.reset(new TransferAssetsTransaction());
            break;
        case eDistributionFee:
            transaction.reset(new DistributionFeeTransaction());
            break;
        case ePublishCurrencyRate:
            transaction.reset(new PublishCurrencyRateTransaction());
            break;
        default:
            errorDescription = StringFormat("Not supported transaction with code: '%d'", code);
            return {};
    }

    if (!transaction->Deserialize(deserializer, errorDescription))
    {
        return {};
    }

    return transaction;
}