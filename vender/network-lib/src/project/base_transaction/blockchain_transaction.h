#ifndef _BLOCKCHAIN_TRANSACTION_H_
#define _BLOCKCHAIN_TRANSACTION_H_

#include <memory>

#include "project/base_transaction/transaction.h"

static const size_t cMaxDomainNameLength = 64;

enum EBaseTransactionType
{
    eRegisterNode             = 1,
    eUnregisterNode           = 2,
    eRegisterSystemWallet     = 3,
    eUnregisterSystemWallet   = 4,
    eUserData                 = 5,
    eTransferAssets           = 10,
    eDistributionFee          = 11,
    ePublishCurrencyRate      = 12,

    //
    eLastBaseTransactionIndex
};

class RegisterNodeTransaction : public Transaction
{
    PublicKey m_nodeWalletPublicKey;
    string    m_nodeAddress;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    RegisterNodeTransaction();
    RegisterNodeTransaction(const uint64_t& id, const PublicKey& nodeWalletPublicKey, const string& nodeAddress);
    virtual ~RegisterNodeTransaction() {};
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
};

class UnregisterNodeTransaction : public Transaction
{
    PublicKey m_nodeWalletPublicKey;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    UnregisterNodeTransaction();
    UnregisterNodeTransaction(const uint64_t& id, const PublicKey& nodeWalletPublicKey);
    virtual ~UnregisterNodeTransaction() {};
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
};

class RegisterSystemWalletTransaction : public Transaction
{
    PublicKey     m_supervisorWalletPublicKey;
    PublicKey     m_systemWalletPublicKey;
    unsigned char m_systemWalletTag;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    RegisterSystemWalletTransaction();
    RegisterSystemWalletTransaction(const uint64_t& id, const PublicKey& supervisorWalletPublicKey, const PublicKey& systemWalletPublicKey, unsigned char systemWalletTag);
    virtual ~RegisterSystemWalletTransaction() {};
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
};

class UnregisterSystemWalletTransaction : public Transaction
{
    PublicKey     m_supervisorWalletPublicKey;
    PublicKey     m_systemWalletPublicKey;
    unsigned char m_systemWalletTag; // system wallet tag to clear

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    UnregisterSystemWalletTransaction();
    UnregisterSystemWalletTransaction(const uint64_t& id, const PublicKey& supervisorWalletPublicKey, const PublicKey& systemWalletPublicKey, unsigned char systemWalletTag);
    virtual ~UnregisterSystemWalletTransaction() {};
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
};

class UserDataTransaction : public Transaction
{
    PublicKey             m_userWalletPublicKey;
    vector<unsigned char> m_userData;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    UserDataTransaction();
    UserDataTransaction(const uint64_t& id, const PublicKey& userWalletPublicKey, const vector<unsigned char>& userData);
    virtual ~UserDataTransaction() {};
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
};

class TransferAssetsTransaction : public Transaction
{
    unsigned short      m_assetCode;
    PublicKey           m_sourceWalletPublicKey;
    PublicKey           m_destinationWalletPublicKey;
    blockchain_amount_t m_amount;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    TransferAssetsTransaction();
    TransferAssetsTransaction(const uint64_t& id, unsigned short assetCode, const PublicKey& sourceWalletPublicKey, const PublicKey& destinationWalletPublicKey, const blockchain_amount_t& amount);
    virtual ~TransferAssetsTransaction() {};
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
};

// distribution fee: tokens, coins
class DistributionFeeTransaction : public Transaction
{
    PublicKey           m_masterNodeWalletPublicKey; // fee tranferred to master node wallet
    // note: use asset code and amount of fee with different distribution transactions at the end of block?
    blockchain_amount_t m_amountOfTokens;
    blockchain_amount_t m_amountOfCoins;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    DistributionFeeTransaction();
    DistributionFeeTransaction(const uint64_t& id, const PublicKey& masterNodeWalletPublicKey,
                               const blockchain_amount_t& amountOfTokens, const blockchain_amount_t& amountOfCoins);
    virtual ~DistributionFeeTransaction() {};

    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
};

class PublishCurrencyRateTransaction : public Transaction
{
    PublicKey           m_ownerWalletPublicKey;
    blockchain_amount_t m_currencyRate;

    virtual void CalculateBodyDigest(DigestCalculator& digestCalculator) const;
    virtual void SerializeBody(BSerializer& serializer) const;
    virtual bool DeserializeBody(BDeserializer& deserializer, string& errorDescription);
public:
    PublishCurrencyRateTransaction();
    PublishCurrencyRateTransaction(const uint64_t& id, const PublicKey& ownerWalletPublicKey, const blockchain_amount_t& currencyRate);
    virtual ~PublishCurrencyRateTransaction() {};

    virtual unsigned short Size() const;
    virtual unsigned short Code() const;
    virtual string Name() const;
    virtual void MakeDescription(std::vector<std::pair<std::string, std::string>>& description) const;
};

std::unique_ptr<Transaction> deserializeTransaction(BDeserializer& deserializer, string& errorDescription);

#endif