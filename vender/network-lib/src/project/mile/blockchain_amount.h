#ifndef BLOCKCHAIN_AMOUNT_H
#define BLOCKCHAIN_AMOUNT_H

#include <string>
#include <vector>
#include "crypto_types.h"

#ifdef USE_R128_FIXEDPOINT
    #include "r128.h"

    typedef R128 blockchain_amount_t;

    enum EBlockchainAmountData
    {
        eBlockchainAmountDataSize = 128/8 // 16 bytes
    };

#elif defined(USE_DEC_FIXEDPOINT)
    #include "decimal_fixed_point.h"

    // 10 decimal digits before comma
    // 18 decimal digits after comma
    typedef DecFixedPoint<12,6> blockchain_amount_t;

    enum EBlockchainAmountData
    {
        eBlockchainAmountDataSize = 10 // 15 bytes (1 byte sign + (10+18)/2 digits) 
    };

#else // use boost multiprecision
    #include <boost/multiprecision/cpp_bin_float.hpp>

    using namespace boost::multiprecision;

    // 14 bytes = mantissa 12 bytes + exponent 2 bytes
    // template <unsigned Digits, digit_base_type DigitBase = digit_base_10, class Allocator = void, class Exponent = int, Exponent MinExponent = 0, Exponent MaxExponent = 0>
    typedef number<backends::cpp_bin_float<96, backends::digit_base_2, void, boost::int16_t, -126, 127>, et_off> blockchain_amount_t;

    enum EBlockchainAmountData
    {
        eBlockchainAmountDataSize = 14 // 14 bytes
    };

    // fixed point type 128.128 bits
    // class FixedPoint? based on boost?

#endif

using std::string;
using std::vector;

// used for serialization/deserialization
// note: Array<N> should be used for blockchain amount data
typedef Array<eBlockchainAmountDataSize> blockchain_amount_data_t;

string BlockchainAmountToString(const blockchain_amount_t& amount);

bool StringToBlockchainAmount(const string& blockchainAmountString, blockchain_amount_t& amount);

void SerializeBlockchainAmount(const blockchain_amount_t& amount, blockchain_amount_data_t& data);

void DeserializeBlockchainAmount(const blockchain_amount_data_t& data, blockchain_amount_t& amount);

string BlockchainAmountToHexString(const blockchain_amount_t& amount);

#endif
