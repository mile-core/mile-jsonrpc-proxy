#include <iostream>
#include <random>
#include <functional>
#include "crypto_types.h"

int main()
{
    std::string number = "55082883721167889035508288372116788903";
    uint256_t ui;
    if (!StringToUInt256(number, ui, false))
    {
        std::cout << "StringToUInt256: " << number << " fail" << std::endl;
        return 1;
    }

    std::string checkNumber = UInt256ToDecString(ui);

    if (checkNumber != number)
    {
        std::cout << "Fail check: " << number << "<>" << checkNumber << std::endl;
        return 2;
    }

    std::default_random_engine generator;
    std::uniform_int_distribution<uint64_t> distribution(std::numeric_limits<uint64_t>::min(),
                                                         std::numeric_limits<uint64_t>::max() - 1);
    auto dice = std::bind(distribution, generator);

    for (int i = 0; i < 1000; ++i)
    {
        std::stringstream s;
        s << dice();
        std::string randomNumber;
        s >> randomNumber;
        uint256_t check;
        if (!StringToUInt256(randomNumber, check, false))
        {
            std::cout << "StringToUInt256: " << randomNumber << " fail" << std::endl;
            return 1;
        }

        std::string checkNumberRandom = UInt256ToDecString(check);

        if (checkNumberRandom != randomNumber)
        {
            std::cout << "Fail check: " << randomNumber << "<>" << checkNumberRandom << std::endl;
            return 2;
        }

    }

    return 0;
}