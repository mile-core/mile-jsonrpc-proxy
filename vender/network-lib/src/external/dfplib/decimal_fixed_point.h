#ifndef DECIMAL_FIXED_POINT_H
#define DECIMAL_FIXED_POINT_H

#include <vector>
#include <array>
#include <string>
#include <sstream>
#include <algorithm>

using std::vector;
using std::array;
using std::string;

// decimal fixed point number
// N.M - N decimal digits before comma and M decimal digits after comma
// serialized size is equal to ceil((N+M)/2)+1 (pack sign too)
template <int N, int M>
class DecFixedPoint
{
    bool                  m_sign; // 1 byte for negation sign '-'
    vector<unsigned char> m_data; // N+M bytes
    // array size for deserialization
    enum EArraySize
    {
        // for sign and for digits
        eArraySize = (N + M)/2 + (((N + M) & 1) != 0 ? 1 : 0) + 1
    };
    constexpr size_t Size() const { return (N + M)/2 + (((N + M) & 1) != 0 ? 1 : 0) + 1; }
    //
    void AddAbsValue(const DecFixedPoint& other)
    {
        unsigned char carry = 0;
        //
        for (size_t i = 0; i < (M+N); i++)
        {
            unsigned char sum = m_data[i] + other.m_data[i] + carry;
            m_data[i] = sum % 10;
            carry = sum / 10;
        }
        // check carry?
        if (carry > 0)
        {
            // overflow error

        }
    }
    bool SubtractAbsValue(const DecFixedPoint& other)
    {
        unsigned char carry = 0;
        //
        if (Compare(other) >= 0) // >= other
        {
            // loop via all digits
            for (size_t i = 0; i < (M+N); i++)
            {
                // m_data = m_data - other.m_data
                if ((other.m_data[i] + carry) > m_data[i])
                {
                    m_data[i] += 10 - (other.m_data[i] + carry);
                    carry = 1;
                }
                else // >= other
                {
                    m_data[i] -= other.m_data[i] + carry;
                    carry = 0;
                }
            }
            return false; // order is not changed so it means sign is not changed
        }
        // < other
        // change sign
        m_sign = !m_sign;
        // loop via all digits
        for (size_t i = 0; i < (M+N); i++)
        {
            // m_data = other.m_data - m_data
            if ((m_data[i] + carry) > other.m_data[i])
            {
                m_data[i] = 10 + other.m_data[i] - (m_data[i] + carry);
                carry = 1;
            }
            else // >= other
            {
                m_data[i] = other.m_data[i] - (m_data[i] + carry);
                carry = 0;
            }
        }
        return true; // order is changed so it means sign is changed
    }
public:
    // --- constructors ---
    DecFixedPoint()
        : m_sign(false)  // '+' by default
        , m_data(N+M, 0) // N+M bytes
    {
    }
    DecFixedPoint(const char* value)
        : m_sign(false)  // '+' by default
        , m_data(N+M, 0) // N+M bytes
    {
        if (!value)
        {
            return;
        }
        // ignore possible errors
        Set(string(value));
    }
    DecFixedPoint(int value)
        : m_sign(value < 0)
        , m_data(N+M, 0)
    {
        if (value < 0)
        {
            value = -value;
        }
        // process positive integer
        for (int i = M; value > 0 && i < (N+M); i++)
        {
             m_data[i] = static_cast<unsigned char>(value % 10);
             value = value / 10;
        }
    }
    DecFixedPoint(int64_t value)
        : m_sign(value < 0)
        , m_data(N+M, 0)
    {
        if (value < 0)
        {
            value = -value;
        }
        // process positive integer
        for (int i = M; value > 0 && i < (N+M); i++)
        {
             m_data[i] = static_cast<unsigned char>(value % 10);
             value = value / 10;
        }
    }
    DecFixedPoint(float value)
        : m_sign(value < 0.0f)
        , m_data(N+M, 0)
    {
   return;
        // process float number
        float f = value;
        // process positive integer
        for (int i = M; f > 0.0f && i < (N+M); i++)
        {
             m_data[i] = static_cast<unsigned char>(static_cast<unsigned int>(f) % 10);
             f /= 10.0f;
        }
        // process fractional part
        f = value - static_cast<unsigned int>(f);
        //

    }
    DecFixedPoint(const DecFixedPoint& other)
        : m_sign(other.m_sign)
        , m_data(other.m_data)
    {
    }
    // --- destructor ---
    ~DecFixedPoint()
    {
    }
    //
    bool IsZero() const
    {
        return !std::any_of(m_data.begin(), m_data.end(), [](unsigned char v) { return v > 0; });
    }
    //
    void Clear()
    {
        m_sign = false;
        std::fill(m_data.begin(), m_data.end(), 0);
    }
    void Negate()
    {
        if (IsZero())
        {
            return; // set sign to false?
        }
        m_sign = !m_sign;
    }
    //
    bool Set(const string& s)
    {
        // boost::trim?
        if (s.empty())
        {
            return false; // empty string
        }

        bool beforeComma = true, digitFound = false;
        int indexAfterComma = M-1;
        vector<unsigned char> m_dataBeforeComma;
        m_dataBeforeComma.reserve(N+1);
        //
        auto CopyDigitsBeforeComma = [this, &m_dataBeforeComma]()
        {
            if (m_dataBeforeComma.empty())
            {
                return;
            }
            // copy data before comma
            int sourceIndex = m_dataBeforeComma.size() - 1;
            int destinationIndex = M; // index of least significant decimal digit
            // 
            for (int i = 0; i < m_dataBeforeComma.size(); i++, sourceIndex--, destinationIndex++)
            {
               m_data[destinationIndex] = m_dataBeforeComma[sourceIndex];
            }
        };
        //
        for (size_t i = 0; i < s.size(); i++)
        {
            if (s[i] == '+' || s[i] == '-')
            {
                if (i != 0)
                {
                    return false;
                }
                m_sign = s[i] == '-';
            }
            else if (s[i] >= '0' && s[i] <= '9')
            {
                unsigned char d = s[i] - '0';
                if (beforeComma)
                {
                    if (d == 0 && m_dataBeforeComma.empty())
                    {
                        // skip adding leading zeros?
                        digitFound = true;
                        continue;
                    }
                    if (m_dataBeforeComma.size() >= N)
                    {
                        return false;
                    }
                    m_dataBeforeComma.push_back(d);
                }
                else // after comma
                {
                    if (indexAfterComma < 0)
                    {
                        return false;
                    }
                    m_data[indexAfterComma--] = d;
                }
                digitFound = true;
            }
            else if (s[i] == '.')
            {
                if (!beforeComma) // error - second comma
                {
                    return false;
                }
                // check error if comma is a first symbol inside string e.g. '.12345' or '-.12345' or '+.12345'?
                if (!digitFound)
                {
                    return false;
                }
                // copy data before comma
                CopyDigitsBeforeComma();
                //
                beforeComma = false;
            }
            else // error
            {
                return false;
            }
        }
        // if comma not found
        if (beforeComma)
        {
            // copy data before comma
            CopyDigitsBeforeComma();
        }

        return true;
    }
    //
    DecFixedPoint& operator= (const DecFixedPoint& other)
    {
        m_sign = other.m_sign;
        m_data = other.m_data;
        return *this;
    }
    DecFixedPoint& operator= (int value)
    {
        // clear digits and save sign
        m_sign = value >= 0;
        std::fill(m_data.begin(), m_data.end(), 0);
        //
        if (value >= 0)
        {
            value = -value;
        }
        // process positive integer
        for (int i = M; value > 0 && i < (N+M-1); i++)
        {
             m_data[i] = static_cast<unsigned char>(value % 10);
             value = value / 10;
        }
    }
    // bool operator()! const
    bool operator()() const
    {
        // at least one digit is not zero
        return std::any_of(m_data.begin(), m_data.end(), [](unsigned char v) { return v > 0; });
    }
    // compare operator
    // returns:
    // +1 - if this > other
    //  0 - if this == other
    // -1 - if this < other
    int Compare(const DecFixedPoint& other) const
    {
        for (int i = N+M-1; i >= 0; i--)
        {
            if (m_data[i] > other.m_data[i]) return 1;
            if (m_data[i] < other.m_data[i]) return -1;
        }
        return 0;
    }
    // negation operator
    DecFixedPoint operator- () const
    {
        DecFixedPoint<N, M> negatedNumber(*this);
        negatedNumber.Negate();
        return negatedNumber;
    }
    //
    DecFixedPoint& operator+= (const DecFixedPoint& other)
    {
        if (other.m_data.size() != (N+M)) // error
        {
            return *this; // ignore bad number other
        }
        // if signs are equal
        if (m_sign == other.m_sign) // +n1 + +n2 = n1 + n2 or -n1 + -n2 = -(n1 + n2)
        {
            // do not change sign in this case
            AddAbsValue(other);
        }
        else if (!m_sign && other.m_sign) // +n1 + -n2 = n1 - n2
        {
            m_sign = SubtractAbsValue(other);
        }
        else if (m_sign && !other.m_sign) // -n1 + +n2 = n2 - n1
        {
            DecFixedPoint<N,M> tmp(other);
            m_sign = tmp.SubtractAbsValue(*this);
            m_data = tmp.m_data;
        }
        return *this;
    }
    DecFixedPoint& operator-= (const DecFixedPoint& other)
    {
        if (other.m_data.size() != (N+M)) // error
        {
            return *this; // ignore bad number other
        }
        // if signs are not equal
        // +n1 - -n2 = n1 + n2 or -n1 - +n2 = -(n1 + n2)
        if (m_sign != other.m_sign)
        {
            // do not change sign in this case
            AddAbsValue(other);
        }
        else if (!m_sign && !other.m_sign) // +n1 - +n2 = n1 - n2
        {
            m_sign = SubtractAbsValue(other);
        }
        else if (m_sign && other.m_sign) // -n1 - -n2 = n2 - n1
        {
            DecFixedPoint<N,M> tmp(other);
            m_sign = tmp.SubtractAbsValue(*this);
            m_data = tmp.m_data;
        }
        return *this;
    }
    DecFixedPoint& operator*= (const DecFixedPoint& other)
    {
        m_sign = m_sign != other.m_sign;

        // find number of digits

        // allocate space for result
        vector<unsigned char> product((N+M) * 2, 0);
        // loop via all digits
        for (int i = 0; i < (N+M); i++)
        {
            unsigned char carry = 0;
            // loop via all digits in other number
            for (int j = 0; j < (N+M); j++)
            {
                product[i + j] += carry + m_data[j] * other.m_data[i];
                carry = product[i + j] / 10;
                product[i + j] %= 10;
            }
            product[i + (N+M)] += carry; // last digit comes from final carry
        }
        // process fixed point and round result if needed
        for (int i = 0; i < (N+M); i++)
        {
            m_data[i] = product[i + M];
        }

        return *this;
    }
    DecFixedPoint& operator/= (const DecFixedPoint& other)
    {
        if (other.IsZero())
        {
            return *this; // error - exception?
        }

        // if signs are not equal
        // + / - = - or - / + = -
        m_sign = m_sign != other.m_sign;



        return *this;
    }
    // mul 10^n
    DecFixedPoint& operator<< (unsigned char n)
    {
        if (n >= (N+M-1))
        { 
            return *this; // error - exception?
        }
        // check shift < (N+M-1 - degree)
        //
        for (int i = N+M-1; i >= n; i--)
        {
            m_data[i] = m_data[i - n];
        }
        for (int i = 0; i < n; i++)
        {
            m_data[i] = 0;
        }
        return *this;
    }
    // div 10^n
    DecFixedPoint& operator>> (unsigned char n)
    {
        if (n >= (N+M-1))
        { 
            return *this; // error - exception?
        }
        for (int i = 0; i < (N+M-n); i++)
        {
            m_data[i] = m_data[i + n];
        }
        for (int i = N+M-n; i < (N+M); i++)
        {
            m_data[i] = 0;
        }
        return *this;
    }
    // add precision?
    string ToString() const
    {
        std::stringstream ss;
        if (m_sign)
        {
            ss << '-';
        }
        bool firstDigitFound = false;
        //
        for (int i = N+M-1; i >= M; i--)
        {
            if (!firstDigitFound && m_data[i] == 0)
            {
                continue; // skip leading zeros
            }
            ss << static_cast<char>('0' + m_data[i]);
            firstDigitFound = true;
        }
        if (!firstDigitFound)
        {
            // add zero
            ss << '0';
        }
        // check if part after comma have non-zero digits
        int lastNonZeroDigitIndex = 0;
        //
        while (lastNonZeroDigitIndex < M)
        {
            if (m_data[lastNonZeroDigitIndex] != 0)
            {
                break;
            }
            lastNonZeroDigitIndex++;
        }
        // non-zero not found inside fractional part of number
        if (lastNonZeroDigitIndex == M)
        {
            return ss.str();
        }
        ss << '.';
        //
        for (int i = M-1; i >= lastNonZeroDigitIndex; i--)
        {
            ss << static_cast<char>('0' + m_data[i]);
        }
        return ss.str();
    }
    // for debug
    string ToRawString() const
    {
        std::stringstream ss;
        if (m_sign)
        {
            ss << '-';
        }
        for (int i = N+M-1; i >= 0; i--)
        {
            ss << static_cast<char>('0' + m_data[i]);
        }
        return ss.str();
    }
    // serialize
    // note: pack to BCD
    void Serialize(array<unsigned char, eArraySize>& data) const
    {
        // serialize sign
        data[0] = m_sign ? '\001' : '\000';
        // serialize digits
        for (size_t i = 0; i < (N+M); i += 2)
        {
            // convert to BCD representation
            data[(i/2) + 1] = m_data[i] | (m_data[i+1] << 4);
        }
        if ((N+M) & 1)
        {
            // serialize last digit (most significant digit)
            data[eArraySize-1] = m_data[m_data.size() - 1];
        }
    }
    // deserialize
    // note: unpack from BCD
    bool Deserialize(const array<unsigned char, eArraySize>& data)
    {
        // deserialize sign
        m_sign = data[0] > 0;
        // deserialize digits
        for (size_t i = 0; i < (N+M)/2; i++)
        {
            unsigned char d1 = data[i+1] & 15;
            unsigned char d2 = (data[i+1] >> 4) & 15;
            // check
            if (d1 > 9 || d2 > 9)
            {
                return false;
            }
            m_data[i*2] = d1;
            m_data[i*2+1] = d2;
        }
        if ((N+M) & 1)
        {
            // deserialize last digit
            unsigned char d = data[eArraySize] & 15;
            // check
            if (d > 9)
            {
                return false;
            }
            m_data[N+M-1] = d;
        }
        return true;
    }
};

template <int N, int M>
bool operator== (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    return dfp1.Compare(dfp2) == 0;
}

template <int N, int M>
bool operator!= (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    return !operator==<N,M>(dfp1, dfp2);
}

template <int N, int M>
bool operator> (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    return dfp1.Compare(dfp2) > 0;
}

template <int N, int M>
bool operator< (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    return dfp1.Compare(dfp2) < 0;
}

template <int N, int M>
bool operator>= (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    return dfp1.Compare(dfp2) >= 0;
}

template <int N, int M>
bool operator<= (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    return dfp1.Compare(dfp2) <= 0;
}

template <int N, int M>
DecFixedPoint<N,M> operator+ (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    DecFixedPoint<N,M> tmp(dfp1);
    tmp += dfp2;
    return tmp;
}

template <int N, int M>
DecFixedPoint<N,M> operator- (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    DecFixedPoint<N,M> tmp(dfp1);
    tmp -= dfp2;
    return tmp;
}

template <int N, int M>
DecFixedPoint<N,M> operator* (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    DecFixedPoint<N,M> tmp(dfp1);
    tmp *= dfp2;
    return tmp;
}

template <int N, int M>
DecFixedPoint<N,M> operator/ (const DecFixedPoint<N,M>& dfp1, const DecFixedPoint<N,M>& dfp2)
{
    DecFixedPoint<N,M> tmp(dfp1);
    tmp /= dfp2;
    return tmp;
}

#endif
