				Key container 
		Key container, key container protection 

Keys are stored in encrypted form. Criticality of key protection is due to the presence of a large balance on the wallet. 
To prevent the hacking of a purse, a number of measures have been taken . 
1. The private key is stored in an encrypted form. The encryption algorithm is very stable. 
OpenSSL library with open source is used. 
2. The key container is re-encrypted each time after the key is read. Thus, after each reading of the key, 
a container with new contents is obtained. This mechanism greatly complicates the analysis of the crypto container . 
3. The program code for converting a password into a key contains an algorithmic nonlinear time delay in the interval 
from half a second to one and a half seconds. The delay does not give an attack on the cryptocontainer using the 
mechanism for selecting passwords. 
4. The key is hidden in an array of data of the order of 100 kb, having the same statistical properties as the key. 
To allocate an encrypted key from an array of data is a difficult task. When encrypted, the spreading algorithm of the 
encrypted data in the statistical garbage changes based on the encryption password and salt. 
5. The private key does not exist in memory in the clear. After transferring the use of the private key, the memory is 
cleared. 
6. The private key is stored in the memory under the mask. The mask is removed immediately before performing the data 
signing operation by the user. After using the private key of the memory area where these operations were performed, 
they are overwritten by a random sequence. 
7. The container is resistant to total search due to time delays inserted in the algorithm. 
8. The cryptocontainer is fault tolerant. Each record of the encrypted container is checked by decryption. In the event
 of a failure, it is re-encrypted and re-recorded. 

	Key container, key container implementation 
The cryptocontainer is implemented as a class. The class contains the following methods: 

saveKey receives the name of a crypto container, a link to a vector with a public key, a reference to a vector with
 a private key, a link to a string with a password, an optional link to the crypto container reading function,
 an optional link to the crypto container record function, optionally a link to user data, to the output string 
with errors, returns a Boolean value of the success of the function. The function receives a private key on the input
 and encrypts it with the received password, then the function based on the password and salt creates an algorithm for 
spreading the encrypted key in the cryptocontainer. Then a cryptocontainer is generated, which is filled with random 
garbage. In this random garbage, the encrypted private key is inserted using the previously obtained algorithm.
 At the beginning of the crypto container, a sign is set that it is a crypto container. Also, it stores the public key.
 The public key, unlike the private key, is not stored in an encrypted form. All data is systematically smeared over the 
crypto container array, depending on the password and salt entered. Then, afterwards, the cryptographic container is 
recorded using the optional function of the write function, or by using the default function that writes to the file. 
Immediately after recording, the crypto container is read using the optional read transfer function, or by using 
the default function. From the record in the file, the salt value is calculated, the encrypted data is taken out of the 
container array based on the generated algorithm for smearing the encrypted data in the crypto container data array,
 then data is extracted from the data of the crypto container data array. The result is an encrypted private key. 
An encrypted private key with the password transmitted at the beginning of the function is sent to decrypt. 
If the decryption was successful, then the function returns True. If the decoding fails, the function returns an error 
that the private key was not saved. 

getKey receives incoming parameters - on the input the name of the crypto container, receives the password, optionally
 receives the crypto container reading function, optionally receives the crypto container record function, optionally 
receives a pointer to the user data. Outgoing parameters - returns the vector of the public key, the vector of 
the private key, the string with errors. The function returns a Boolean success value for the private key. The function 
reads the crypto container using the read function, optionally passed users to the crypto container, or by using 
the function of reading from the file by default. After that, the function parses the contents of the crypto container, 
for this it uses the transmitted password and the saved salt to generate an algorithm for extracting data from 
the container file. After that, the encrypted private key is taken out of the container array. The encrypted private
 key is sent for decryption. After that, the decrypted private key is placed in the transmitted link to the private key
 by the user. And after that, with the transmitted password, the crypto container is encrypted with the function 
described earlier. After the new encryption of the crypto container, the crypto container is read out and decrypted. 
If the decryption occurs with the returned value "falce", then an error is returned. Otherwise, the function returns "true". 
If the decryption has already occurred, the new cryptographic container is again saved with the same filename, 
then a separate thread starts that counts the time interval in the form of 2 microseconds, and then wipes the user's
 memory area where the decrypted private key was transmitted. 

saveMaskedKey function receives the cryptocontainer file name, a public key vector reference, a private key vector 
reference, a mask vector reference, a password vector reference, optionally receives a link to the cryptocontainer 
function in the function, optionally a reference to the cryptocontainer function from the function , optionally receives 
a pointer to user data . The function outputs the mask, the masked key, the error string with the output parameter of 
the key and returns the Boolean value of the success of the function termination. The function receives the private key 
in a masked form and then transfers it to the encryption. After encryption, the generation is based on the transmitted
 password and the algorithm salt for smearing the encrypted key in the crypto container. After this, a crypto container 
is randomly generated, and the data obtained after encryption of the private key is placed on it based on the algorithm 
obtained earlier. This data is written to the crypto container, and then the crypto container is read by the function
 passed by the user earlier, or by the default reading from the file. Then, the cryptographic container is decrypted 
and the encrypted private key is extracted from the received algorithm. The private key is decrypted and it is checked 
whether this decrypted private key matches the key that was transmitted earlier. In case of a match, the function 
returns "true". If the key does not match, it returns "false" and the error string that the private key has not been saved. 

getMaskedKey function receives a link to the crypto container name, a link to the password string, optionally receives
 a link to the crypto container reading function, optionally receives a link to the crypto container entry function, 
optionally receives a pointer to the user data. Returns a vector with a public key, returns a vector with a private key,
 returns a vector with a mask, returns a string with errors. With the help of the received password and salt, the algorithm 
for extracting the encrypted key container file based on the password, previously stored salt, is generated.
 After the algorithm is generated and the cryptocontainer is read out using the function passed by the user, or by using 
the default function of reading from the file. After that, the encrypted private key is taken from the crypto container.
 The encrypted private key is sent for decryption, after which a mask is applied to it. The mask and the masked key
 are passed to the user, and the cryptographic container itself is encrypted again using the previously transmitted password,
 and if the encryption has failed unsuccessfully, it is re-encrypted again. After the separate thread is started, 
in order to remove the key that was deciphered after 2 microseconds, and which in the mosquito form is peredoan to the user. 

getPublicKey function receives a string with the name of the cryptocontainer , optionally receives a pointer to the crypto
 container reading function, optionally receives a pointer to the crypto container entry function, optionally receives
 a pointer to the user data. Returns the vector with the public key. The function receives a crypto container using
 the received name. Reads the public key from it, which is stored in the clear. Places the public key in the link of 
the user passed to the function. Informs that the public key cryptocontainer has been read. 

setNewPassword functions sets a new password for the cryptocontainer.
