#include "src/crypto_encrypt.h"

int main(int argc, char* argv[])
{
    if (argc != 1)
    {
    	std::cout << "no arguments expected" << std::endl;
        return 1;
    }
    int i,j;
    string errorDescription;
    string plaintext = "4FVQJ2dtu084fiiqoZhfq8cmWHJZXdVm6siffHb8jC2Xd2erHTYDUdmJUcvjnyQQ";
    string opentext  = "1JtuTfi58H8dV6ifbjCX2e3TDm9c4jyq";
    string masktext =  "2342J2dtuT24fiiqoZhfq8c3WHJ3XdVm64iff3bfjC2Xd23rHTYD5dmJU6vjn7F9";
    string password = "Dead beef dfgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdsfgdsfgdfsgdf123fsgdfsg";
    string password2 = "Dead beef dfgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdfsgdsfgdsfgdfsgdf123";
    string outputFileName = "cryptocontainerbare.key";
    string outputFileName2 = "cryptocontainermask.key";
    vector<unsigned char> privatekey(plaintext.begin(),plaintext.end());
    vector<unsigned char> publickey(opentext.begin(),opentext.end());
    vector<unsigned char> mask(masktext.begin(),masktext.end());
    cout << "The  private key to encrypt" << std::endl;
    cout << plaintext << std::endl;
    cout << "The  public key to save" << std::endl;
    cout << opentext << std::endl;
    ContainerCipher cryptocontainer;
    for (j=0; j < 10; j++)
    {
    if(!cryptocontainer.saveKey(/*/outputFileName/*/to_string(j)/**/,publickey,privatekey,password,errorDescription))
    {
    	cout << errorDescription << std::endl;;
    }
    vector<unsigned char> newKey;
    vector<unsigned char> newPublicKey;
    //string pwd = "";
    if(!cryptocontainer.getKey(to_string(j),newPublicKey,newKey,/*NULL/*pwd/*/password2,errorDescription))
    {
    	cout << errorDescription << std::endl;;
    }
    string newplaintext(newKey.begin(),newKey.end());
	std::cout << "The  private key decrypted from the cryptocontainer via bare key interface with wrong password" << std::endl;
	cout << newplaintext << std::endl;
    string newopentext(newPublicKey.begin(),newPublicKey.end());
	std::cout << "The  public key gotten from the cryptocontainer via bare key interface with wrong password" << std::endl;
	cout << newopentext << std::endl;
    newKey.clear();
    newPublicKey.clear();
    if(!cryptocontainer.getKey(to_string(j),newPublicKey,newKey,/*NULL/*pwd/*/password,errorDescription))
    {
    	cout << errorDescription << std::endl;;
    }
    string newplaintext2(newKey.begin(),newKey.end());
	std::cout << "The  private key decrypted from the cryptocontainer via bare key interface with right password" << std::endl;
	cout << newplaintext2 << std::endl;
    string newopentext2(newPublicKey.begin(),newPublicKey.end());
	std::cout << "The  public key gotten from the cryptocontainer via bare key interface with right password" << std::endl;
	cout << newopentext2 << std::endl;
    vector<unsigned char> key;
    key.reserve(privatekey.size());
    key = privatekey;
    for (i = 0; i < privatekey.size(); i++)
    {
        key[i] = privatekey[i]^mask[i];
    }
    if(!cryptocontainer.saveMaskedKey(outputFileName2,publickey,key,mask,password,errorDescription))
    {
     	cout << errorDescription << std::endl;
     }
    vector<unsigned char> newKey2;
    vector<unsigned char> newPublicKey2;
    if(!cryptocontainer.getMaskedKey(outputFileName2,newPublicKey2,newKey2,mask,/*NULL/*pwd/*/password2,errorDescription))
	{
	    cout << errorDescription << std::endl;;
	}
    vector<unsigned char> key2;
    key2.reserve(newKey2.size());
    key2 = newKey2;
    for (i = 0; i < newKey2.size(); i++)
    {
        key2[i] = newKey2[i]^mask[i];
    }
    string newplaintext3(key2.begin(),key2.end());
	std::cout << std::endl << "The  private key decrypted from the cryptocontainer via masked key interface with wrong password" << std::endl;
	cout << newplaintext3 << std::endl;
    string newopentext3(newPublicKey2.begin(),newPublicKey2.end());
	std::cout << std::endl << "The  public key gotten from the cryptocontainer via masked key interface with wrong password" << std::endl;
	cout << newopentext3 << std::endl;
    newKey2.clear();
    newPublicKey2.clear();
    if(!cryptocontainer.getMaskedKey(outputFileName2,newPublicKey2,newKey2,mask,/*NULL/*pwd/*/password,errorDescription))
	{
	    cout << errorDescription << std::endl;;
	}
     vector<unsigned char> key3;
     key3.reserve(newKey2.size());
     key3 = newKey2;
     for (i = 0; i < newKey2.size(); i++)
     {
         key3[i] = newKey2[i]^mask[i];
     }
     string newplaintext4(key3.begin(),key3.end());
 	std::cout << std::endl << "The  private key decrypted from the cryptocontainer via masked key interface with right password" << std::endl;
 	cout << newplaintext4 << std::endl;
     string newopentext4(newPublicKey2.begin(),newPublicKey2.end());
 	std::cout << std::endl << "The  public key gotten from the cryptocontainer via masked key interface with right password" << std::endl;
 	cout << newopentext4 << std::endl;
    }
     std::cout << std::endl << "The test encryption is done" << std::endl;
    return 0;
}
