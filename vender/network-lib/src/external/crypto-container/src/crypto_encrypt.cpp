#include "crypto_encrypt.h"

static const unsigned int KEY_SIZE = 32;
static const unsigned int BLOCK_SIZE = 16;
static const unsigned int PRIVATE_KEY_SIZE = 64;
static const unsigned int PUBLIC_KEY_SIZE = 32;
static const unsigned int TRASH_SIZE = 0x10000;
static const string sign = "DeadBeef";

vector<unsigned char> fileRead(const string& fileName, string& errorDescription, void *user_data)
{
    vector <unsigned char> vec;
    ifstream iFile(fileName.c_str(), std::ios::binary);
    if (!iFile)
    {
        errorDescription = "Cannot read file '"+fileName+"'";
        return vec;
    }
    if (iFile.is_open()) {
        iFile.seekg(0, std::ios::end);
        std::size_t pos = static_cast<std::size_t>(iFile.tellg());
        iFile.seekg(0);
        vec.resize(pos);
        if (!vec.empty()) {
            iFile.read(reinterpret_cast<char*>(&vec.front()), vec.size());
        }
    }
    return vec;
}

bool fileWrite(const string& fileName,vector<unsigned char>& data, string& errorDescription, void *user_data)
{
    ofstream ofs(fileName.c_str(), std::ios::binary);
    if (!ofs)
    {
    	errorDescription = "Cannot write file '"+fileName+"'";
        return false;
    }
    if (ofs.is_open()) {
        ofs.write(reinterpret_cast<const char*>(data.data()), data.size());
    }
    ofs.flush();
    ofs.close();
    return true;
}

template<typename T> void vDump(const string& fileName, unsigned int ln, const string& prefix, const T& d)
{
    std::cout << fileName << ":" << ln << ": " << prefix << "\t" << d << std::endl;
}

template<> void vDump<string>(const string& fileName, unsigned int ln, const string& prefix, const string& d)
{
    std::cout << fileName << ":" << ln << ": " << prefix << "\t" << left << setw(64) << d << " (" << d.size() << ")" << std::endl;
}

template<typename T> void tDump(const string& fileName, unsigned int ln, const string& prefix, const T& d)
{
    std::cout << fileName << ":" << ln << ": " << prefix << "\t";
    for(unsigned int i=0;i<sizeof(T);++i)
    {
        std::cout << setw(2) << setfill('0') << hex << right << (unsigned int)(d[i]) << dec << setfill(' ');
    }
    std::cout << " (" << sizeof(T) << ")" << std::endl;
}

void bDump(const string& fileName, unsigned int ln, const string& prefix, unsigned char* a, unsigned int len)
{
    std::cout << fileName << ":" << ln << ": " << prefix;
    for(unsigned int i=0;i<len;++i)
    {
        if ((i%16)==0)
        {
            if (i)
            {
                std::cout << std::endl;
                std::cout << "\t\t\t";
            }
            else
            {
                std::cout << "\t\t";
            }
        }
        else
            if (i)
            {
        	    std::cout << ", ";
            }
        std::cout << setw(2) << hex << right << (unsigned int)(a[i]) << dec;
    }
    std::cout << " (" << len << ")" << std::endl;
}


ContainerCipher::ContainerCipher(): m_cipher("aes-256-cbc"),
m_digest("sha1"),
m_count(1),
m_embeded(true),
m_debug(false)
{
}

ContainerCipher::ContainerCipher(const string& cipher, const string& digest, unsigned int count, bool embeded): m_cipher(cipher),
m_digest(digest),
m_count(count),
m_embeded(embeded),
m_debug(false)
{
}

ContainerCipher::~ContainerCipher()
{
}

bool ContainerCipher::saveKey(const string& fileName,vector<unsigned char>& publicKey, vector<unsigned char>& privateKey, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void*),bool (*w)(const string&,vector<unsigned char>&, string&, void*), void *user_data)
{
    DEBUG_FCT("saveKey");
    DEBUG_BDUMP(&privateKey.front(),privateKey.size());
    if(privateKey.size() != PRIVATE_KEY_SIZE)
    {
    	errorDescription = "Wrong private key size";
    	return false;
    }
    string salt;
    vector<unsigned char> trash(TRASH_SIZE);
    if(!RAND_bytes(&trash.front(),trash.size()))
    {
    	errorDescription = "RAND_bytes fault";
    	return false;
    }
    string skey(privateKey.begin(), privateKey.end());
    string ciphertext = encrypt(skey, errorDescription,password,salt);
    if(ciphertext.size()==0)
    {
    	return false;
    }
    vector<unsigned char> pass(password.begin(),password.end());
    unsigned int size = 0;
    unsigned int i;
    unsigned int delta = 0;
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)m_salt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> cipher(ciphertext.begin(),ciphertext.end());
    DEBUG_BDUMP(&cipher.front(),cipher.size());
    vector<unsigned char> vsign(sign.begin(),sign.end());
    DEBUG_BDUMP((unsigned char*)ciphertext.c_str(),ciphertext.size());
    for (i = 0; i < vsign.size(); i++)
    {
        trash[i] = vsign[i];
    }
    for (i = vsign.size(); i < vsign.size()+PUBLIC_KEY_SIZE; i++)
    {
        trash[i] = publicKey[i - vsign.size()];
    }
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        trash[i] = m_salt[i - vsign.size() -PUBLIC_KEY_SIZE];
    }
    for (i = size +vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i < size +vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt)+ cipher.size(); i++)
    {
    	delta = delta + trash[i - size -vsign.size()-PUBLIC_KEY_SIZE-sizeof(m_salt)];
        trash[((0xf&size) + 2) * i + delta] = cipher[i - size -vsign.size()-PUBLIC_KEY_SIZE-sizeof(m_salt)];
    }
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    if(!w(fileName,trash, errorDescription, user_data))
    {
    	return false;
    }
    trash.clear();
    trash = r(fileName, errorDescription, user_data);
    if(trash.size()==0)
    {
    	return false;
    }
    size = 0;
    vector<unsigned char> oldSalt;
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        oldSalt.push_back(trash[i]);
    }
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)oldSalt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> newcipher;
    delta = 0;
    for (i = 0; i < PRIVATE_KEY_SIZE*2 + 1; i++)
    {
    	 delta = delta + trash[i];
         newcipher.push_back(trash[((0xf&size) + 2)*(i + size+vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt))+ delta]);
    }
    string newciphertext(newcipher.begin(),newcipher.end());
    DEBUG_BDUMP(&newcipher.front(),newcipher.size());
    DEBUG_BDUMP((unsigned char*)newciphertext.c_str(),newciphertext.size());
    string plaintext = decrypt(newciphertext, errorDescription,password,salt);
    if (plaintext.size()==0)
    {
    	return false;
    }
    vector<unsigned char> dkey(plaintext.begin(),plaintext.end());
    if((privateKey) != dkey)
    {
    	errorDescription = "Cryptocontainer error! private key is not saved!";
    	return false;
    //	while (!saveKey(fileName,publicKey, privateKey, password, errorDescription,r,w,user_data));
    }
    return true;
}

void keyRemove(vector<unsigned char>& privateKey)
{
	std::this_thread::sleep_for(std::chrono::microseconds(200));
    privateKey.clear();
}


bool ContainerCipher::getKey(const string& fileName,vector<unsigned char>& publicKey, vector<unsigned char>& privateKey, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void*),bool (*w)(const string&,vector<unsigned char>&, string&, void*), void *user_data)
{
    DEBUG_FCT("getKey");
    unsigned int delta = 0;
    string salt;
    vector <unsigned char> trash = r(fileName, errorDescription, user_data);
    if(trash.size()==0)
    {
    	return false;
    }
    vector<unsigned char> vsign(sign.begin(),sign.end());
    vector<unsigned char> newsign;
    unsigned int i;
    unsigned int size = 0;
    for (i = 0; i < vsign.size(); i++)
    {
        newsign.push_back(trash[i]);
    }
    if (newsign != vsign)
    {
    	errorDescription = "The file is not the cryptocomtainer";
    	return false;
    }
    (publicKey).clear();
    for (i = vsign.size(); i < vsign.size()+PUBLIC_KEY_SIZE; i++)
    {
        publicKey.push_back(trash[i]);
    }
    vector<unsigned char> pass((password).begin(),(password).end());
    vector<unsigned char> oldSalt;
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        oldSalt.push_back(trash[i]);
    }
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)oldSalt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> cipher;
    for (i = 0; i < PRIVATE_KEY_SIZE*2 + 1; i++)
    {
        delta = delta + trash[i];
        cipher.push_back(trash[((0xf&size) + 2)*(i + size+vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt))+ delta]);
    }
    string ciphertext(cipher.begin(),cipher.end());
    DEBUG_BDUMP(&cipher.front(),cipher.size());
    DEBUG_BDUMP((unsigned char*)ciphertext.c_str(),ciphertext.size());
    string plaintext = decrypt(ciphertext, errorDescription,(password),salt);
    if(plaintext.size()==0)
    {
    	errorDescription = "wrong password";
    	return false;
    }
    vector<unsigned char> dkey(plaintext.begin(),plaintext.end());
    (privateKey) = dkey;
    DEBUG_BDUMP(&(privateKey).front(),(privateKey).size());
    while (!saveKey(fileName,publicKey, privateKey, password, errorDescription,r,w,user_data));
    std::thread  keyDelay (keyRemove,std::ref(privateKey));  // spawn new thread that calls keyRemove
    keyDelay.detach();
    return true;
}

bool ContainerCipher::setNewPassword(const string& fileName, const string& password, const string& newPassword, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void*),bool (*w)(const string&,vector<unsigned char>&, string&, void*), void *user_data)
{
    DEBUG_FCT("setNewPassword");
    unsigned int delta = 0;
    string salt;
    vector <unsigned char> trash = r(fileName, errorDescription, user_data);
    if(trash.size()==0)
    {
    	return false;
    }
    vector<unsigned char> vsign(sign.begin(),sign.end());
    vector<unsigned char> newsign;
    vector<unsigned char> publicKey;
    vector<unsigned char> privateKey;
    unsigned int i;
    unsigned int size = 0;
    for (i = 0; i < vsign.size(); i++)
    {
        newsign.push_back(trash[i]);
    }
    if (newsign != vsign)
    {
    	errorDescription = "The file is not the cryptocomtainer";
    	return false;
    }
    (publicKey).clear();
    for (i = vsign.size(); i < vsign.size()+PUBLIC_KEY_SIZE; i++)
    {
        publicKey.push_back(trash[i]);
    }
    vector<unsigned char> pass(password.begin(),password.end());
    vector<unsigned char> oldSalt;
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        oldSalt.push_back(trash[i]);
    }
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)oldSalt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> cipher;
    for (i = 0; i < PRIVATE_KEY_SIZE*2 + 1; i++)
    {
        delta = delta + trash[i];
        cipher.push_back(trash[((0xf&size) + 2)*(i + size+vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt))+ delta]);
    }
    string ciphertext(cipher.begin(),cipher.end());
    DEBUG_BDUMP(&cipher.front(),cipher.size());
    DEBUG_BDUMP((unsigned char*)ciphertext.c_str(),ciphertext.size());
    string plaintext = decrypt(ciphertext, errorDescription,password,salt);
    if(plaintext.size()==0)
    {
    	errorDescription = "wrong password";
    	return false;
    }
    vector<unsigned char> dkey(plaintext.begin(),plaintext.end());
    (privateKey) = dkey;
    DEBUG_BDUMP(&(privateKey).front(),(privateKey).size());
    while (!saveKey(fileName,publicKey, privateKey, newPassword, errorDescription,r,w,user_data));
    std::thread  keyDelay (keyRemove,std::ref(privateKey));  // spawn new thread that calls keyRemove
    keyDelay.detach();
    return true;
}

bool ContainerCipher::saveMaskedKey(const string& fileName,vector<unsigned char>& publicKey, vector<unsigned char>& maskedPrivateKey, vector<unsigned char>& mask, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void*),bool (*w)(const string&,vector<unsigned char>&, string&, void*), void *user_data)
{
    DEBUG_FCT("saveMaskedKey");
    DEBUG_BDUMP(&(maskedPrivateKey).front(),maskedPrivateKey.size());
    if(maskedPrivateKey.size() != PRIVATE_KEY_SIZE)
    {
    	errorDescription = "Wrong masked private key size";
    	return false;
    }
    string salt;
    vector<unsigned char> trash(TRASH_SIZE);
    if(!RAND_bytes(&trash.front(),trash.size()))
    {
    	errorDescription = "RAND_bytes fault";
    	return false;
    }
    unsigned int delta = 0;
    vector<unsigned char> key;
    key.reserve(maskedPrivateKey.size());
    key = maskedPrivateKey;
    unsigned int i;
    for (i = 0; i < maskedPrivateKey.size(); i++)
    {
        key[i] = maskedPrivateKey[i]^(mask)[i];
    }
    DEBUG_BDUMP(&key.front(),key.size());
    string skey(key.begin(), key.end());
    string ciphertext = encrypt(skey, errorDescription,password,salt);
    if(ciphertext.size()==0)
    {
    	return false;
    }
    vector<unsigned char> pass(password.begin(),password.end());
    unsigned int size = 0;
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)m_salt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> cipher(ciphertext.begin(),ciphertext.end());
    DEBUG_BDUMP(&cipher.front(),cipher.size());
    vector<unsigned char> vsign(sign.begin(),sign.end());
    DEBUG_BDUMP((unsigned char*)ciphertext.c_str(),ciphertext.size());
    for (i = 0; i < vsign.size(); i++)
    {
        trash[i] = vsign[i];
    }
    for (i = vsign.size(); i < vsign.size()+PUBLIC_KEY_SIZE; i++)
    {
        trash[i] = (publicKey)[i - vsign.size()];
    }
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        trash[i] = m_salt[i - vsign.size() -PUBLIC_KEY_SIZE];
    }
    for (i = size +vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i < size +vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt)+ cipher.size(); i++)
    {
    	delta =delta + trash[i - size -vsign.size()-PUBLIC_KEY_SIZE-sizeof(m_salt)];
        trash[((0xf&size) + 2) * i + delta] = cipher[i - size -vsign.size()-PUBLIC_KEY_SIZE-sizeof(m_salt)];
    }
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    if(!w(fileName,trash, errorDescription, user_data))
    {
    	return false;
    }
    trash.clear();
    trash = r(fileName, errorDescription, user_data);
    if(trash.size()==0)
    {
    	return false;
    }
    size = 0;
    vector<unsigned char> oldSalt;
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        oldSalt.push_back(trash[i]);
    }
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)oldSalt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> newcipher;
    delta = 0;
    for (i = 0; i < PRIVATE_KEY_SIZE*2 + 1; i++)
    {
    	delta = delta + trash[i];
         newcipher.push_back(trash[((0xf&size) + 2)*(i + size+vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt)) + delta]);
    }
    string newciphertext(newcipher.begin(),newcipher.end());
    DEBUG_BDUMP(&newcipher.front(),newcipher.size());
    DEBUG_BDUMP((unsigned char*)newciphertext.c_str(),newciphertext.size());
    string plaintext = decrypt(newciphertext, errorDescription,password,salt);
    if(plaintext.size()==0)
    {
    	return false;
    }
    vector<unsigned char> dkey(plaintext.begin(),plaintext.end());
    if(key != dkey)
    {
    	errorDescription = "Cryptocontainer error! private key is not saved!";
    	return false;
    //    while(!saveMaskedKey(fileName,publicKey,maskedPrivateKey,mask,password,errorDescription,r,w,user_data));
    }
    return true;
}

bool ContainerCipher::getMaskedKey(const string& fileName,vector<unsigned char>& publicKey, vector<unsigned char>& maskedPrivateKey, vector<unsigned char>& mask, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void*),bool (*w)(const string&,vector<unsigned char>&, string&, void*), void *user_data)
{
    DEBUG_FCT("getMaskedKey");
    string salt;
    vector <unsigned char> trash = r(fileName, errorDescription, user_data);
    if(trash.size()==0)
    {
    	return false;
    }
    vector<unsigned char> vsign(sign.begin(),sign.end());
    vector<unsigned char> newsign;
    unsigned int i;
    unsigned int size = 0;
    unsigned int delta = 0;
    for (i = 0; i < vsign.size(); i++)
    {
        newsign.push_back(trash[i]);
    }
    if (newsign != vsign)
    {
    	errorDescription = "The file is not the cryptocomtainer";
    	return false;
    }
    (publicKey).clear();
    for (i = vsign.size(); i < vsign.size()+PUBLIC_KEY_SIZE; i++)
    {
        (publicKey).push_back(trash[i]);
    }
    vector<unsigned char> pass((password).begin(),(password).end());
    vector<unsigned char> oldSalt;
    for (i = vsign.size()+PUBLIC_KEY_SIZE; i < vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt); i++)
    {
        oldSalt.push_back(trash[i]);
    }
    for (i = 0; i < pass.size(); i++)
    {
        size = size + (unsigned int)pass[i];
    }
    for (i = 0; i < sizeof(m_salt); i++)
    {
        size = size + (unsigned int)oldSalt[i];
    }
    size = size&0x3ff;
    DEBUG_BDUMP(&trash.front(),trash.size()/32);
    vector<unsigned char> cipher;
    for (i = 0; i < PRIVATE_KEY_SIZE*2 + 1; i++)
    {
        delta = delta + trash[i];
        cipher.push_back(trash[((0xf&size) + 2)*(i + size+vsign.size()+PUBLIC_KEY_SIZE+sizeof(m_salt))+ delta]);
    }
    string ciphertext(cipher.begin(),cipher.end());
    string plaintext = decrypt(ciphertext, errorDescription,(password),salt);
    if(plaintext.size()==0)
    {
    	errorDescription = "wrong password";
    	return false;
    }
    vector<unsigned char> key(plaintext.begin(),plaintext.end());
    DEBUG_BDUMP(&key.front(),key.size());
    vector<unsigned char> dKey;
    dKey.reserve(key.size());
    dKey = key;
    for (i = 0; i < key.size(); i++)
    {
        dKey[i] =key[i]^(mask)[i];
    }
    DEBUG_BDUMP(&dKey.front(),dKey.size());
    (maskedPrivateKey) = dKey;
    DEBUG_BDUMP(&(maskedPrivateKey).front(),(maskedPrivateKey).size());
    while(!saveMaskedKey(fileName,publicKey,maskedPrivateKey,mask,password,errorDescription,r,w,user_data));
    std::thread  keyDelay (keyRemove,std::ref(maskedPrivateKey));  // spawn new thread that calls keyRemove
    keyDelay.detach();
    return true;
}

bool ContainerCipher::getPublicKey(const string& fileName, vector<unsigned char>& publicKey, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *),bool (*w)(const string&,vector<unsigned char>&, string&, void *), void *user_data)
{
    DEBUG_FCT("getPublicKey");

    vector <unsigned char> trash = r(fileName, errorDescription, user_data);
    if(trash.size()==0)
    {
    	return false;
    }
    vector<unsigned char> vsign(sign.begin(),sign.end());
    vector<unsigned char> newsign;
    unsigned int i;

    for (i = 0; i < vsign.size(); i++)
    {
        newsign.push_back(trash[i]);
    }
    if (newsign != vsign)
    {
    	errorDescription = "The file is not the cryptocomtainer";
    	return false;
    }
    publicKey.clear();
    for (i = vsign.size(); i < vsign.size()+PUBLIC_KEY_SIZE; i++)
    {
        publicKey.push_back(trash[i]);
    }
 	return true;
}
string ContainerCipher::encrypt(const string& plaintext, string& errorDescription, const string& password, const string& salt)
{
    DEBUG_FCT("encrypt");
    setSalt(salt);
    string empty;
    if(!init(password, errorDescription))
    {
        return empty;
    }
    kvT  x = encodeCipher(plaintext, errorDescription);
    unsigned char* ct    = x.first;
    unsigned int   ctlen = x.second;
    DEBUG_BDUMP(ct,ctlen);
    string ret = encodeBase64(ct,ctlen, errorDescription);
    delete [] ct;
    DEBUG_MDUMP(ret);
    return ret;
}

string ContainerCipher::decrypt(const string& mimetext, string& errorDescription, const string& password, const string& salt)
{
    DEBUG_FCT("decrypt");
    kvT  x     = decodeBase64(mimetext, errorDescription);
    unsigned char* ct    = x.first;
    unsigned char* ctbeg = ct;
    unsigned int   ctlen = x.second;
    DEBUG_BDUMP(ct,ctlen);
    string error;
    if (strncmp((const char*)ct,SALTED,8) == 0)
    {
        memcpy(m_salt,&ct[8],8);
        ct += 16;
        ctlen -= 16;
    }
    else
    {
        setSalt(salt);
    }
    if(!init(password, errorDescription))
    {
        return error;
    };
    string ret = decodeCipher(ct,ctlen, errorDescription);
    delete [] ctbeg;
    DEBUG_MDUMP(ret);
    return ret;
}

string ContainerCipher::encodeBase64(unsigned char* ciphertext, unsigned int ciphertext_len, string& errorDescription) const
{
    DEBUG_FCT("encodeBase64");
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bm  = BIO_new(BIO_s_mem());
    b64 = BIO_push(b64,bm);
    if (BIO_write(b64,ciphertext,ciphertext_len)<2)
    {
    	errorDescription = "BIO_write() failed";
    	return errorDescription;
    }
    if (BIO_flush(b64)<1)
    {
        errorDescription = "BIO_flush() failed";
        return errorDescription;
    }
    BUF_MEM *bptr=0;
    BIO_get_mem_ptr(b64,&bptr);
    unsigned int len=bptr->length;
    char* mimetext = new char[len+1];
    memcpy(mimetext, bptr->data, bptr->length-1);
    mimetext[bptr->length-1]=0;
    BIO_free_all(b64);
    string ret = mimetext;
    delete [] mimetext;
    return ret;
}

kvT ContainerCipher::decodeBase64(const string& mimetext, string& errorDescription) const
{
    DEBUG_FCT("decodeBase64");
    kvT x;
    int SZ=mimetext.size();
    x.first = new unsigned char[SZ];
    char* tmpbuf = new char[SZ+1];
    strcpy(tmpbuf,mimetext.c_str());
    BIO* b64 = BIO_new(BIO_f_base64());
    if (SZ <= 64)
    {
        BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    }
    BIO* bm  = BIO_new_mem_buf(tmpbuf,mimetext.size());
    bm = BIO_push(b64,bm);
    x.second = BIO_read(bm,x.first,SZ);
    BIO_free_all(bm);
    delete [] tmpbuf;
    return x;
}

kvT ContainerCipher::encodeCipher(const string& plaintext, string& errorDescription) const
{
    DEBUG_FCT("encodeCipher");
    unsigned int SZ=plaintext.size()+AES_BLOCK_SIZE+20;
    unsigned char* ciphertext = new unsigned char[SZ];
    std::memset(ciphertext,0,SZ);
    unsigned char* pbeg = ciphertext;
    unsigned int off = 0;
    if (m_embeded)
    {
        memcpy(&ciphertext[0],SALTED,8);
        memcpy(&ciphertext[8],m_salt,8);
        off = 16;
        ciphertext += off;
    }
    int ciphertext_len=0;
    int ciphertext_padlen=0;
    EVP_CIPHER_CTX ctx;
    EVP_CIPHER_CTX_init(&ctx);
    if (!EVP_EncryptInit_ex(&ctx, EVP_aes_256_cbc(), NULL, m_key, m_iv))
    {
        errorDescription = "EVP_EncryptInit_ex failed";
        return kvT(0,0);
    }
    EVP_CIPHER_CTX_set_key_length(&ctx, EVP_MAX_KEY_LENGTH);
    unsigned char* p    = (unsigned char*)plaintext.c_str();
    unsigned int   plen = plaintext.size();
    if (!EVP_EncryptUpdate(&ctx,ciphertext,&ciphertext_len,p,plen))
    {
        errorDescription = "EVP_EncryptUpdate failed";
        return kvT(0,0);
    }
    unsigned char* pbuf = ciphertext+ciphertext_len;
    if (!EVP_EncryptFinal_ex(&ctx,pbuf,&ciphertext_padlen))
    {
        errorDescription = "EVP_EncryptFinal_ex failed";
        return kvT(0,0);
    }
    ciphertext_len += ciphertext_padlen + off;
    EVP_CIPHER_CTX_cleanup(&ctx);
    return kvT(pbeg,ciphertext_len);
}

string ContainerCipher::decodeCipher(unsigned char* ciphertext, unsigned int ciphertext_len, string& errorDescription) const
{
    DEBUG_FCT("decodeCipher");
    unsigned int SZ = ciphertext_len+20;
    unsigned char* plaintext = new unsigned char[SZ];
    std::memset(plaintext,0,SZ);
    int plaintext_len = 0;
    EVP_CIPHER_CTX ctx;
    EVP_CIPHER_CTX_init(&ctx);
    string error;
    if (!EVP_DecryptInit_ex(&ctx, EVP_aes_256_cbc(), NULL, m_key, m_iv))
    {
        errorDescription = "EVP_DecryptInit_ex failed";
        return error;
    }
    EVP_CIPHER_CTX_set_key_length(&ctx, EVP_MAX_KEY_LENGTH);
    if (!EVP_DecryptUpdate(&ctx,plaintext,&plaintext_len,ciphertext,ciphertext_len))
    {
        errorDescription = "EVP_DecryptUpdate failed";
        return error;
    }
    int plaintext_padlen=0;
    if (!EVP_DecryptFinal_ex(&ctx,plaintext+plaintext_len,&plaintext_padlen))
    {
        errorDescription = "EVP_DecryptFinal_ex failed";
        return error;
    }
    plaintext_len += plaintext_padlen;
    plaintext[plaintext_len] = 0;
    string ret((char*)plaintext,PRIVATE_KEY_SIZE);
    delete [] plaintext;
    EVP_CIPHER_CTX_cleanup(&ctx);
    return ret;
}

void ContainerCipher::setSalt(const string& salt)
{
    DEBUG_FCT("setSalt");
    if (salt.length() != 8)
    {
    	RAND_bytes(m_salt, 8);
        DEBUG_BDUMP(&m_salt[0],sizeof(m_salt));
    }
    else
    {
        memcpy(m_salt,salt.c_str(),8);
    }
    return;
}

bool ContainerCipher::init(const string& password, string& errorDescription)
{
    DEBUG_FCT("init");
    m_password = password;
    std::memset(m_key,0,sizeof(m_key));
    std::memset(m_iv,0,sizeof(m_iv));
    OpenSSL_add_all_algorithms();
    const EVP_CIPHER* cipher = EVP_get_cipherbyname(m_cipher.c_str());
    const EVP_MD*     digest = EVP_get_digestbyname(m_digest.c_str());
    if (!cipher)
    {
    	errorDescription = "init(): cipher does not exist "+m_cipher;
        return false;
    }
    if (!digest)
    {
    	errorDescription = "init(): digest does not exist "+m_digest;
        return false;
    }
//    unsigned char cdelay;
//    RAND_bytes(&cdelay, 1);
//    unsigned idelay = ((0x32 + (unsigned int)cdelay)&0x7F)* 10;
//    sleep_for(milliseconds(idelay));
    int ks = EVP_BytesToKey(cipher, digest, m_salt, (unsigned char*)m_password.c_str(), m_password.length(), m_count, m_key, m_iv);
    if (ks!=32)
    {
    	errorDescription = "EVP_BytesToKey error";
        return false;
    }
    DEBUG_DUMP(m_password);
    DEBUG_DUMP(m_cipher);
    DEBUG_DUMP(m_digest);
    DEBUG_TDUMP(m_salt);
    DEBUG_TDUMP(m_key);
    DEBUG_TDUMP(m_iv);
    DEBUG_DUMP(m_count);
    return true;
}



