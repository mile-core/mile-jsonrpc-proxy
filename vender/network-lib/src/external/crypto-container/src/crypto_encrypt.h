// Encryption and Decryption based on the OpenSSL library
#ifndef CRYPTO_ENCRYPT_H
#define CRYPTO_ENCRYPT_H
#ifndef _GLIBCXX_USE_NANOSLEEP
#define _GLIBCXX_USE_NANOSLEEP
#endif
#include <fstream>
#include <iostream>
#include <string>
#include <memory>
#include <iomanip>
#include <limits>
#include <stdexcept>
#include <array>
#include <cstdint>
#include <sstream>
#include <list>
#include <cstring>
#include <vector>
#include <set>
#include <iomanip>
#include <istream>
#include <chrono>
#include <cstdlib>
#include <utility>
#include <cstdlib>
#include <algorithm>
#include <functional>
#include <thread>
#include <openssl/aes.h>
#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/conf.h>
#include <openssl/evp.h>
#include <openssl/err.h>
#include <openssl/rsa.h>
#include <openssl/engine.h>

using namespace std;
using std::string;
using std::vector;
using std::set;
using std::ostream;
using namespace std::this_thread;
using namespace std::chrono;



using EVP_CIPHER_CTX_free_ptr = std::unique_ptr<EVP_CIPHER_CTX, decltype(&::EVP_CIPHER_CTX_free)>;

typedef unsigned char aesKeyT[32];
typedef unsigned char aesIvT[32];
typedef unsigned char aesSaltT[8];
typedef std::pair<unsigned char*,unsigned int> kvT;

#define DEBUG_PRE        __FILE__ << ":" << __LINE__ << ": "
#define DEBUG_FCT(fct)   if(m_debug) std::cout << DEBUG_PRE << "FCT " << fct << std::endl
#define DEBUG_TDUMP(v)   if(m_debug) tDump(__FILE__,__LINE__,#v,v)
#define DEBUG_DUMP(v)    if(m_debug) vDump(__FILE__,__LINE__,#v,v)
#define DEBUG_DUMPR(k,v) if(m_debug) vDump(__FILE__,__LINE__,k,v)
#define DEBUG_BDUMP(a,x) if(m_debug) bDump(__FILE__,__LINE__,#a,a,x)
#define DEBUG_MDUMP(a)   if(m_debug) bDump(__FILE__,__LINE__,#a,(unsigned char*)a.c_str(),a.size())
#define DEBUG_MADEIT     std::cout << DEBUG_PRE << "MADE IT" << std::endl
#define DUMP(v)          vDump(__FILE__,__LINE__,#v,v)
#define SALTED           "Salted__"

vector<unsigned char> fileRead(const string& fileName, string& errorDescription, void *user_data);
bool fileWrite(const string& fileName,vector<unsigned char>& data, string& errorDescription, void *user_data);

class ContainerCipher
{
public:
    ContainerCipher();
    ContainerCipher(const string& cipher, const string& digest, unsigned int count, bool embeded=true);
    ~ContainerCipher();
    bool saveKey(const string& fileName, vector<unsigned char>& publicKey, vector<unsigned char>& privateKey, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *)=fileRead,bool (*w)(const string&,vector<unsigned char>&, string&, void *)=fileWrite, void *user_data=((void*)0));
    bool getKey(const string& fileName, vector<unsigned char>& publicKey, vector<unsigned char>& privateKey, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *)=fileRead,bool (*w)(const string&,vector<unsigned char>&, string&, void *)=fileWrite, void *user_data = ((void*)0));
    bool setNewPassword(const string& fileName, const string& password,  const string& newPassword, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *)=fileRead,bool (*w)(const string&,vector<unsigned char>&, string&, void *)=fileWrite, void *user_data = ((void*)0));
    bool saveMaskedKey(const string& fileName, vector<unsigned char>& publicKey, vector<unsigned char>& maskedPrivateKey, vector<unsigned char>& mask, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *)=fileRead,bool (*w)(const string&,vector<unsigned char>&, string&, void *)=fileWrite, void *user_data = ((void*)0));
    bool getMaskedKey(const string& fileName, vector<unsigned char>& publicKey, vector<unsigned char>& maskedPrivateKey, vector<unsigned char>& mask, const string& password, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *)=fileRead,bool (*w)(const string&,vector<unsigned char>&, string&, void *)=fileWrite, void *user_data = ((void*)0));
    bool getPublicKey(const string& fileName, vector<unsigned char>& publicKey, string& errorDescription,vector<unsigned char> (*r)(const string&, string&, void *)=fileRead,bool (*w)(const string&,vector<unsigned char>&, string&, void *)=fileWrite, void *user_data = ((void*)0));
private:
    string encrypt(const string& plaintext, string& errorDescription, const string& password="", const string& salt="");
    string decrypt(const string& ciphertext, string& errorDescription, const string& password="", const string& salt="");
    string encodeBase64(unsigned char* ciphertext, unsigned int ciphertext_len, string& errorDescription) const;
    kvT encodeCipher(const string& plaintext, string& errorDescription) const;
    kvT decodeBase64(const string& mimetext, string& errorDescription) const;
    string decodeCipher(unsigned char* ciphertext, unsigned int ciphertext_len, string& errorDescription) const;
    void debug(string& errorDescription, bool debugEnable=true)
    {
        m_debug=debugEnable;
    }
    bool debug(string *pErrorDescription) const
    {
        return m_debug;
    }
    void setSalt(const string& salt);
    bool init(const string& password, string& errorDescription);
    string m_password;
    string m_cipher;
    string m_digest;
    aesSaltT  m_salt;
    aesKeyT   m_key;
    aesIvT    m_iv;
    unsigned int        m_count;
    bool        m_embeded;
    bool        m_debug;
};

#endif
