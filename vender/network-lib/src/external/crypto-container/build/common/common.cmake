# common headers and sources for Crypto Container framework
cmake_minimum_required(VERSION 2.8)
set(MAJOR_VERSION 0)
set(MINOR_VERSION 16)

message(STATUS "Version: ${MAJOR_VERSION}.${MINOR_VERSION}")

# get git revision
execute_process(COMMAND git rev-parse HEAD OUTPUT_VARIABLE GIT_REVISION OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "Git revision: " ${GIT_REVISION})
add_definitions(-DGIT_REVISION="${GIT_REVISION}")

# get git last commit date
execute_process(COMMAND git log -1 --format=%cd HEAD OUTPUT_VARIABLE GIT_DATE OUTPUT_STRIP_TRAILING_WHITESPACE)
message(STATUS "Git last commit date: " ${GIT_DATE})
add_definitions(-DGIT_DATE="${GIT_DATE}")

# get current date string in format YYYY.MM.DD
if (${CMAKE_MAJOR_VERSION} GREATER 2) # CMake v.3.0 at least
    # possible format options: "%Y-%m-%d %H:%M"
    string(TIMESTAMP CURRENT_DATE "%Y-%m-%d")
else () # CMake e.g. v.2.8
    if (WIN32)   # Windows (note: date format will be dependent on the country/locale in this case)
        execute_process(COMMAND cmd /C date /T OUTPUT_VARIABLE CURRENT_DATE OUTPUT_STRIP_TRAILING_WHITESPACE)
    else (WIN32) # Linux
        execute_process(COMMAND date "+%Y-%m-%d" OUTPUT_VARIABLE CURRENT_DATE OUTPUT_STRIP_TRAILING_WHITESPACE)
    endif ()
    # string(REPLACE "\n" "" CURRENT_DATE ${CURRENT_DATE})
endif ()

message(STATUS "Build date: " ${CURRENT_DATE})
add_definitions(-DBUILD_DATE="${CURRENT_DATE}")

if (CMAKE_SIZEOF_VOID_P EQUAL 8)
    set(BUILD_BITS 64) # 64 bits
elseif (CMAKE_SIZEOF_VOID_P EQUAL 4)
    set(BUILD_BITS 32) # 32 bits
endif()
add_definitions(-DBUILD_BITS=${BUILD_BITS})

if (DEFINED RELEASE_BUILD AND "${RELEASE_BUILD}" STREQUAL "1")
    message(STATUS "Release ${BUILD_BITS}-bits build is used")
    set(CMAKE_BUILD_TYPE Release)
    # note: already defined
    # add_definitions(-DRELEASE_BUILD)
else ()
    message(STATUS "Debug ${BUILD_BITS}-bits build is used (use -DRELEASE_BUILD=1 in cmake command line for release build)")
    set(CMAKE_BUILD_TYPE Debug)
    add_definitions(-DDEBUG_BUILD)
endif ()

# common include directories

include_directories (
    ../../
    ../../src
)

file (GLOB COMMON_HEADERS
    ../../src/*.h
)

file (GLOB COMMON_SOURCES
    ../../src/*.cpp
)

set(BUILD_SHARED_LIBS OFF)

find_package(Threads)
find_package(OpenSSL)

message(STATUS "Compiler: ${CMAKE_CXX_COMPILER_ID}")
