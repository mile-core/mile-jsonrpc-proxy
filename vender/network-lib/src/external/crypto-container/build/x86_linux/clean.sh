cd ../../external/leveldb
make clean
cd ../../build/x86_linux
rm -r CMakeFiles
rm Makefile
rm cmake_install.cmake
rm CMakeCache.txt
rm *.log
rm -Rf db_tests
rm *.a