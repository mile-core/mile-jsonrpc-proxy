rmdir /S /Q CMakeFiles 
rmdir /S /Q cryptocontainer_test
del Makefile
del cmake_install.cmake
del CMakeCache.txt
del *.log
@rem CL specific
del *.pdb
del *.res
del *.ilk
del *.lib
del CTestTestfile.cmake
