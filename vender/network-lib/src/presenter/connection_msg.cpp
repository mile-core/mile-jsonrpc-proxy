/* 
 * connection_msg.cpp
*/
#include <string.h>
#include <time.h>

#include <exception>
#include <iostream>

#include <boost/foreach.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/version.hpp>

#include "connection_msg.h"
#include "csa_pdbg.h"

using namespace boost::property_tree;
using namespace std;

namespace Engine
{

Params::Params()
{
}


Params::Params(ptree const &pt) : pt_{pt}
{
}


Params const & Params::operator=(Params const &other)
{
    if (&other != this)
    {
        // yes, copy
        this->pt_ = other.pt_;
    }

    return *this;
}

string Params::param(string const &param) const noexcept(false)
{
    if(!param.size())
    {
        return pt_.data();
    }

    return pt_.get(param, "");
}


int Params::param_int(string const &param_name) const
{
    int result = strtol(param(param_name).c_str(), nullptr, 0);
    return result;
}


unsigned Params::param_uint(string const &param_name) const
{
    unsigned result = (unsigned)strtol(param(param_name).c_str(), nullptr, 0);
    return result;
}

Params Params::get_child(string const &param_name) const
{
    boost::optional<const ptree&> optional = pt_.get_child_optional(param_name);
    return Params(!optional ? ptree() : optional.get());
}

size_t Params::size() const
{
    return pt_.size();
}

ptree::iterator Params::begin()
{
    return pt_.begin();
}

ptree::iterator Params::end()
{
    return pt_.end();
}

XMsgIn::XMsgIn() : result_(0)
{
}

XMsgIn::~XMsgIn()
{
}

const XMsgIn& XMsgIn::init_event(const ptree& tree)  
{
    boost::optional<const ptree&> optional = tree.get_child_optional("event");
    pt_ = optional ? optional.get() : tree;
    id_ = pt_.get("id", "");
    return *this;
}

void XMsgIn::init(const string& s)
{
    const char* szDefStr = "<none>";
    std::stringstream ss{s};
    ptree pt;
    read_json(ss, pt);

    pt_.clear();

    result_ = pt.get("result", 1);  // the default value is FAIL

    if (result_ != 0)
    {
        text_ = pt.get("text", szDefStr);   // optional
        PDBG("result_=%d text_=%s", result_, text_.c_str());
        return;     // error occurred
    }

    boost::optional<ptree&> optional = pt.get_child_optional("params");
    if (optional)
    {
        pt_ = optional.get();
        text_ = pt_.get("text", szDefStr);             
    }
}

const std::string& XMsgIn::id() const
{
    return id_;
}


void XMsgIn::clear()
{
    result_ = 0;
    text_.clear();
    pt_.clear();
}

//--------------------------------------------------------------------------------------------------

XMsgOut::XMsgOut()
{
}


XMsgOut::~XMsgOut()
{
}


XMsgOut& XMsgOut::set_op(const string& op)
{
    pt_.put("id", op);
    return *this;
}


XMsgOut& XMsgOut::add(const string& param_key, const string& param_value, bool params)
{
    pt_.add((params ? "params." : "") + param_key, param_value);
    return *this;
}

XMsgOut& XMsgOut::add_child(const std::string& param_key, const XMsgOut& param_value, bool params)
{
    pt_.add_child((params ? "params." : "") + param_key, param_value.pt_);
    return *this;
}

string XMsgOut::asString() const
{
    std::stringstream ss;
    write_json(ss, pt_);
    return ss.str();
}

    XMsgOut &XMsgOut::push_back(const XMsgOut &param_value) {
        pt_.push_back(std::make_pair("",param_value.pt_));
        return *this;
    }

}
