#pragma once

#include <string>
#include <unordered_map>
#include <boost/property_tree/ptree.hpp>

using boost::property_tree::ptree;

namespace Engine
{

class XMsgOut
{
public:
    XMsgOut();
    ~XMsgOut();

    XMsgOut& set_op(const std::string& op);

    XMsgOut& add(const std::string& param_key, const std::string& param_value, bool params = true);
    XMsgOut& add_child(const std::string& param_key, const XMsgOut& param_value, bool params = true);
    XMsgOut& push_back(const XMsgOut& param_value);


    std::string asString() const;

    using string_pair = std::pair<std::string, std::string>;

private:
    ptree pt_;
};

class Params
{
public:
    Params();
    Params(ptree const &pt);
    Params const & operator=(Params const &other);

    // basic param value getter (as std::string)
    // empty param name for case when pt_ is empty (stores just the value)
    std::string param(std::string const &param = std::string{}) const;
    // type-specific getters - wrappers of param()
    int param_int(std::string const &param = std::string{}) const;
    unsigned param_uint(std::string const &param = std::string{}) const;

    Params get_child(std::string const &param) const;

    size_t size() const;

    ptree::iterator begin();
    ptree::iterator end();

private:
    ptree pt_;
};

class XMsgIn
{
public:

    XMsgIn();
    ~XMsgIn();

    /*
    * s - xml
    */
    const XMsgIn& init_event(const ptree& tree);
    void init(const std::string& s);
    void clear();

    const std::string& id() const;

    int result() const { return result_; }
    std::string const & text() const { return text_; }

    Params params() const { return Params{pt_}; }

private:
    ptree pt_;  // "params" subtree of input xml
    int result_;
    std::string text_;
    std::string id_;
};

} // namespace Engine