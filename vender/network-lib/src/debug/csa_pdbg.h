/*
 * csa_pdbg.h
 */

#ifndef CSA_PDBG_H_
#define CSA_PDBG_H_

#ifdef DEBUG_BUILD
#include "crypto_types.h"
#include "csa_type.h"
// general form
#define PDBG_(group, fmt, ...) pcsa_pdbg(group, __FILE__, __LINE__, fmt, ##__VA_ARGS__)
// for aux debug messages
#define PDBG(fmt, ...) pcsa_pdbg("debug", __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define PDBG_T(fmt, ...) pcsa_pdbg("test", __FILE__, __LINE__, fmt, ##__VA_ARGS__)
// for error debug messages
#define PDBG_ERR(fmt, ...) pcsa_pdbg("error", __FILE__, __LINE__, fmt, ##__VA_ARGS__)
#define PDBG_CACHE_LOCAL_KEY(walletName, publicKey, privateKey) pcsa_cache_key_pair(std::string("l:") + walletName, publicKey, privateKey)
#define PDBG_CACHE_NODE_KEY(walletName, publicKey, privateKey) pcsa_cache_key_pair(std::string("n:") + walletName, publicKey, privateKey)
#define PDBG_GET_CACHE_NODE_KEY(walletName, pair) do {\
                                                  if (pcsa_get_cache_key_pair(std::string("n:") + walletName, pair)) return PCSA_RES_OK;}\
                                                  while(0)
#define PDBG_GET_CACHE_LOCAL_KEY(walletName, pair) do {\
                                                   if (pcsa_get_cache_key_pair(std::string("l:") + walletName, pair)) return PCSA_RES_OK;}\
                                                   while(0)
#define PDBG_DEL_CACHE_LOCAL_KEY(walletName) pcsa_del_cache_key_pair(std::string("l:") + walletName)
#define PDBG_DEL_CACHE_NODE_KEY(walletName)  pcsa_del_cache_key_pair(std::string("n:") + walletName)
void pcsa_cache_key_pair(const std::string& name, const PublicKey& publicKey, const PrivateKey& privateKey);
bool pcsa_get_cache_key_pair(const std::string& name, pcsa_keys_pair* pair);
void pcsa_del_cache_key_pair(const std::string& name);
void pcsa_pdbg(char const *group, char const *file, int line, char const *fmt, ...);
#else	// DEBUG_BUILD
#define PDBG_(group, fmt, ...)
#define PDBG(fmt, ...)
#define PDBG_T(fmt, ...)
#define PDBG_ERR(fmt, ...)
#define PDBG_CACHE_LOCAL_KEY(walletName, publicKey, privateKey)
#define PDBG_CACHE_NODE_KEY(walletName, publicKey, privateKey)
#define PDBG_GET_CACHE_NODE_KEY(walletName, pair)
#define PDBG_GET_CACHE_LOCAL_KEY(walletName, pair)
#define PDBG_DEL_CACHE_LOCAL_KEY(walletName)
#define PDBG_DEL_CACHE_NODE_KEY(walletName)
#endif	// DEBUG_BUILD

#endif /* CSA_PDBG_H_ */