#include <stdarg.h>

#include <unordered_set>
#include <unordered_map>
#include <string>
#include <string.h>

#include "csa_pdbg.h"
#include "csa.h"

#ifdef ANDROID
#include <android/log.h>
#endif

static std::unordered_set<std::string> group_set;
static std::unordered_map<std::string, std::pair<PublicKey, PrivateKey>> cache_keys;

void pcsa_pdbg(char const *group, char const *file, int line, char const *fmt, ...)
{
    if (!fmt)
    {
        return;
    }
    
    if (group_set.find(std::string{group}) == group_set.end() &&
        group_set.find(std::string{"all"}) == group_set.end() &&
        std::string{group} != "test")
    {
        return;
    }

    char buffer[1024] = "";
    snprintf(buffer, sizeof(buffer), "[%s] %s:%d: ", group, file, line);
    size_t length = (size_t)strlen(buffer);
    char* p = buffer + length;

    va_list ap;

    va_start(ap, fmt);
    vsnprintf(p, sizeof(buffer) - length, fmt, ap);
    va_end(ap);

#ifdef ANDROID
    __android_log_print(ANDROID_LOG_VERBOSE, "JNI", "%s", buffer);
#else
    printf("%s\n", buffer);
#endif
}

void pcsa_pdbg_set_groups(char const *groups)
{
    char const *p, *p_end;
    p = groups;
    do
    {
        p_end = strchr(p, ',');
        if(p_end)
        {
            group_set.insert(std::string{p, std::string::size_type(p_end - p)});
            p = p_end + 1;
        }
        else
        {
            group_set.insert(std::string{p});
        }
    }
    while(p_end);
}

void pcsa_cache_key_pair(const std::string& walletName, const PublicKey& publicKey, const PrivateKey& privateKey)
{
    auto it = cache_keys.find(walletName);
    if (it == cache_keys.end())
    {
        cache_keys[walletName] = std::make_pair(publicKey, privateKey);
        return;
    }

    auto& value = it->second;
    value.first = publicKey;
    value.second = privateKey;
}

bool pcsa_get_cache_key_pair(const std::string& walletName, pcsa_keys_pair* pair)
{
    auto it = cache_keys.find(walletName);
    if (it == cache_keys.end())
    {
        return false;
    }

    const auto& value = it->second;
    strncpy(pair->public_key, value.first.ToBase58CheckString().c_str(), sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(pair->private_key, value.second.ToBase58CheckString().c_str(), sizeof(pcsa_keys_pair::private_key) - 1);
    return true;
}

void pcsa_del_cache_key_pair(const std::string& name)
{
    cache_keys.erase(name);
}