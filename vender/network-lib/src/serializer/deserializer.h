#ifndef _BINARY_ESERIALIZER_H
#define _BINARY_ESERIALIZER_H

#include <cstdint>
#include <vector>
#include <string>
#include "crypto_types.h"

using std::vector;
using std::string;

class BDeserializer
{
    const vector<unsigned char>& m_data;
    size_t                       m_offset;          // offset inside data
    size_t                       m_currentPosition; // current position inside data
    bool                         m_littleEndian;
    //
    bool                         m_errorState;
    string                       m_errorDescription;
    //
    bool CheckError(size_t sizeToRead);
public:
    // 
    BDeserializer(const vector<unsigned char>& data, size_t offset = 0, bool littleEndian = true);
    //
    size_t Position() const { return m_currentPosition; }
    bool ErrorState() const { return m_errorState; }
    const string& ErrorDescription() const { return m_errorDescription; }
    // reset state including possible error state
    // if reset position is false then error state only will be reset
    void Reset(bool resetPosition);
    //
    BDeserializer& operator>> (bool& value);
    BDeserializer& operator>> (char& value);
    BDeserializer& operator>> (unsigned char& value);
    BDeserializer& operator>> (unsigned short& value);
    BDeserializer& operator>> (unsigned int& value);
    // four bytes of vector size added before vector data
    BDeserializer& operator>> (vector<unsigned char>& v);
    // four bytes of string size added before string data
    BDeserializer& operator>> (string& s);
    //
    BDeserializer& operator>> (uint256_t& ui);
    BDeserializer& operator>> (uint64_t& ui);
    // if big endian then reverse data?
    template <size_t N>
    BDeserializer& operator>> (Array<N>& a)
    {
        if (CheckError(N))
        {
            return *this;
        }

        string errorDescription;

        // ignore error code and error description because possible errors already checked at this time
        a.Set(m_data, m_currentPosition, errorDescription);

        m_currentPosition += N;

        return *this;
    }
    // four bytes of data size processed before data
    bool GetData(char* data, size_t& size);
    bool GetRawData(unsigned char* data, size_t size);
    bool GetData(unsigned char* data, size_t& size);
    bool GetRawData(vector<unsigned char>& data, size_t size);
    //
    bool Skip(size_t size);
};

#endif