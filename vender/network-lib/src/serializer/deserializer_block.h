#ifndef _DESERIALIZER_BLOCK_H
#define _DESERIALIZER_BLOCK_H
#include <memory>
#include <utility>

#include "crypto_types.h"
#include "deserializer.h"
#include "utils.h"
#include "transaction.h"

struct BlockHeader
{
    // block header data
    unsigned short         version;             // blockchain version
    Digest                 previousBlockDigest; // digest of previous block
    Digest                 merkleRoot;          // root hash of all block transactions
    uint64_t               timestamp;           // timestamp
    unsigned short         transactionCount;
    uint256_t              id;                  // block serial ID
    unsigned short         producerPublicKeySize;

    void Description(std::vector<std::pair<std::string, std::string>>& description) const
    {
        description.push_back(std::make_pair("version", std::to_string(version)));
        description.push_back(std::make_pair("previousBlockDigest" , previousBlockDigest.ToHexString()));
        description.push_back(std::make_pair("merkleRoot", merkleRoot.ToHexString()));
        description.push_back(std::make_pair("timeStamp", UInt64ToDecString(timestamp)));
        description.push_back(std::make_pair("transactionCount", std::to_string(transactionCount)));
        description.push_back(std::make_pair("id", UInt256ToDecString(id)));
        description.push_back(std::make_pair("producerPublicKeySize", std::to_string(producerPublicKeySize)));
    }
};

BDeserializer& operator>>(BDeserializer& serializer, BlockHeader& header)
{
    return serializer >> header.version >> header.previousBlockDigest >> header.merkleRoot
                      >> header.timestamp >> header.transactionCount >> header.id >> header.producerPublicKeySize;
}

struct BlockSignature
{
    PublicKey producerPublicKey;
    Signature producerSignature;

    void Description(std::vector<std::pair<std::string, std::string>>& description) const
    {
        description.push_back(std::make_pair("producerPublicKey", producerPublicKey.ToHexString()));
        description.push_back(std::make_pair("producerSignature" , producerSignature.ToHexString()));
    }
};

BDeserializer& operator>>(BDeserializer& serializer, BlockSignature& signature)
{
    return serializer >> signature.producerPublicKey >> signature.producerSignature;
}

struct Block
{
    using BlockSignatures = std::vector<BlockSignature>;
    using BlockTransactions = std::vector<std::unique_ptr<Transaction>>;

    BlockHeader       header;
    BlockSignatures   signatures;
    BlockTransactions transactions;
};

bool deserializeBlock(const vector<unsigned char>& data, Block& block, std::string& errorDescription)
{
    BDeserializer deserialize(data);
    deserialize >> block.header;
    if (deserialize.ErrorState())
    {
        errorDescription = StringFormat("couldn't deserialize block header: %s", deserialize.ErrorDescription().c_str());
        return false;
    }

    block.signatures.reserve(block.header.producerPublicKeySize);
    for (unsigned short i = 0; i < block.header.producerPublicKeySize; ++i)
    {
        BlockSignature signature;
        deserialize >> signature;
        if (deserialize.ErrorState())
        {
            errorDescription = StringFormat("couldn't deserialize signature '%d': %s", i, deserialize.ErrorDescription().c_str());
            return false;
        }
        block.signatures.push_back(std::move(signature));
    }

    std::unique_ptr<Transaction> transaction;
    for (unsigned short i = 0; i < block.header.transactionCount; ++i)
    {
        transaction = deserializeTransaction(deserialize, errorDescription);
        if (!transaction)
        {
            return false;
        }
        block.transactions.emplace_back(transaction.release());
    }

    return true;
}

#endif