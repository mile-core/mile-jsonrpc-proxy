#ifndef _BINARY_SERIALIZER_
#define _BINARY_SERIALIZER_

#include "crypto_types.h"
#include "base58.h"
#include "blockchain_amount.h"

#include <vector>


class BSerializer;


template <typename T>
struct UnaryModifier
{
    UnaryModifier(void (&func)(BSerializer&, T), T data): 
                                     m_function(func),
                                     m_data(data) {}

    BSerializer& action(BSerializer& serializer) const
    {
        m_function(serializer, m_data);
        return serializer;
    }


    friend BSerializer& operator<<(BSerializer& serializer, const UnaryModifier& mod)
    { 
        return mod.action(serializer);
    }


private:
    T m_data;
    void (&m_function)(BSerializer&, T);
};

template <typename A, typename B>
struct BinaryModifier
{
    BinaryModifier(void (&func)(BSerializer&, A, B), A first, B second): 
                                     m_function(func),
                                     m_first(first),
                                     m_second(second) {}

    BSerializer& action(BSerializer& serializer) const
    {
        m_function(serializer, m_first, m_second);
        return serializer;
    }


    friend BSerializer& operator<<(BSerializer& serializer, const BinaryModifier& mod)
    { 
        return mod.action(serializer);
    }


private:
    A m_first;
    B m_second;
    void (&m_function)(BSerializer&, A, B);
};

class BSerializer
{
private:
    std::vector<unsigned char> m_data;
    unsigned int m_width;
    char         m_fill;
    Signature    m_signature;


    unsigned int getWidth();
    char getFill();

    void setWidth(unsigned int width);
    void setFill(char fill);

    void setSignature(const Signature& sign);

public:
    Signature& getSignature(); 
    static void do_setw(BSerializer& serializer, unsigned int width);
    static void do_setFill(BSerializer& serializer, char fill);
    static void do_signDigest(BSerializer& serializer, PrivateKey privateKey, PublicKey publicKey);

    BSerializer(): m_width(0), m_fill(0) {};
    BSerializer(unsigned int reserveSize): m_width(0), m_fill(0) 
    {
        m_data.reserve(reserveSize);
    };

    BSerializer& operator<< (bool value);
    BSerializer& operator<< (char value);
    BSerializer& operator<<(const std::vector<unsigned char>& data);
    BSerializer& operator<<(const uint256_t& data);
    BSerializer& operator<<(const uint64_t& data);
    BSerializer& operator<<(const char* str);
    BSerializer& operator<<(unsigned char);
    BSerializer& operator<<(unsigned int);
    BSerializer& operator<<(unsigned short);
    BSerializer& operator<<(BSerializer& (&call)(BSerializer&));

    std::string toHex();

    template <size_t T>
    BSerializer& operator<<(const Array<T>& data)
    {
        data.AppendTo(m_data);
        return *this;
    }

};

BSerializer& signer(BSerializer& signer);

UnaryModifier<unsigned int> setw(unsigned int width);
UnaryModifier<char> setfill(char fill);

BinaryModifier<PrivateKey, PublicKey> signDigest(const PrivateKey& privateKey, const PublicKey& publicKey);
#endif