#include <cstring>
#include "deserializer.h"
#include "utils.h"

BDeserializer::BDeserializer(const vector<unsigned char>& data, size_t offset, bool littleEndian)
    : m_data(data)
    , m_offset(offset)
    , m_currentPosition(offset)
    , m_littleEndian(littleEndian)
    , m_errorState(false)
{
}

bool BDeserializer::CheckError(size_t sizeToRead)
{
    if (m_errorState)
    {
        return true; // Bdeserializer is in error state already
    }
    if ((m_currentPosition + sizeToRead) > m_data.size())
    {
        m_errorState = true;
        m_errorDescription = StringFormat("size to read is greater than remaining data: %zu > %zu", sizeToRead, m_data.size() - m_currentPosition);
        return true;
    }
    return false;
}

void BDeserializer::Reset(bool resetPosition)
{
    if (resetPosition)
    {
        m_currentPosition = m_offset;
    }
    m_errorState = false;
    m_errorDescription.clear();
}

BDeserializer& BDeserializer::operator>> (bool& value)
{
    if (CheckError(sizeof(bool)))
    {
        return *this;
    }

    value = m_data[m_currentPosition++] != 0;

    return *this;
}

BDeserializer& BDeserializer::operator>> (char& value)
{
    if (CheckError(sizeof(char)))
    {
        return *this;
    }

    value = static_cast<char>(m_data[m_currentPosition++]);

    return *this;
}

BDeserializer& BDeserializer::operator>> (unsigned char& value)
{
    if (CheckError(sizeof(char)))
    {
        return *this;
    }

    value = m_data[m_currentPosition++];

    return *this;
}

BDeserializer& BDeserializer::operator>> (unsigned short& value)
{
    if (CheckError(sizeof(unsigned short)))
    {
        return *this;
    }

    if (m_littleEndian)
    {
        value = (static_cast<unsigned short>(m_data[m_currentPosition])) | ((static_cast<unsigned short>(m_data[m_currentPosition+1])) << 8);
    }
    else // big endian
    {
        value = ((static_cast<unsigned short>(m_data[m_currentPosition])) << 8) | (static_cast<unsigned short>(m_data[m_currentPosition+1]));
    }

    m_currentPosition += sizeof(unsigned short);

    return *this;
}

BDeserializer& BDeserializer::operator>> (unsigned int& value)
{
    if (CheckError(sizeof(unsigned int)))
    {
        return *this;
    }

    if (m_littleEndian)
    {
        value = (static_cast<unsigned int>(m_data[m_currentPosition])) | 
                ((static_cast<unsigned int>(m_data[m_currentPosition+1])) << 8) |
                ((static_cast<unsigned int>(m_data[m_currentPosition+2])) << 16) | 
                ((static_cast<unsigned int>(m_data[m_currentPosition+3])) << 24);
    }
    else // big endian
    {
        value = ((static_cast<unsigned int>(m_data[m_currentPosition])) << 24) | 
                ((static_cast<unsigned int>(m_data[m_currentPosition+1])) << 16) |
                ((static_cast<unsigned int>(m_data[m_currentPosition+2])) << 8) | 
                (static_cast<unsigned int>(m_data[m_currentPosition+3]));
    }

    m_currentPosition += sizeof(unsigned int);

    return *this;
}

BDeserializer& BDeserializer::operator>> (vector<unsigned char>& v)
{
    // check for vector size (four bytes before data)
    if (CheckError(sizeof(unsigned int)))
    {
        return *this;
    }

    // size is in little endian format always
    unsigned int size = (static_cast<unsigned int>(m_data[m_currentPosition])) | 
                        ((static_cast<unsigned int>(m_data[m_currentPosition+1])) << 8) |
                        ((static_cast<unsigned int>(m_data[m_currentPosition+2])) << 16) | 
                        ((static_cast<unsigned int>(m_data[m_currentPosition+3])) << 24);

    // check for vector size and data (current position pointer is not moved yet)
    if (CheckError(sizeof(unsigned int) + size))
    {
        return *this;
    }

    // skip vector size
    m_currentPosition += sizeof(unsigned int);

    // read vector data
    v.assign(m_data.begin() + m_currentPosition, m_data.begin() + m_currentPosition + size);

    // skip vector data
    m_currentPosition += size;

    return *this;
}

BDeserializer& BDeserializer::operator>> (string& s)
{
    // check for string size (four bytes before string)
    if (CheckError(sizeof(unsigned int)))
    {
        return *this;
    }

    // size is in little endian format always
    unsigned int size = (static_cast<unsigned int>(m_data[m_currentPosition])) | 
                        ((static_cast<unsigned int>(m_data[m_currentPosition+1])) << 8) |
                        ((static_cast<unsigned int>(m_data[m_currentPosition+2])) << 16) | 
                        ((static_cast<unsigned int>(m_data[m_currentPosition+3])) << 24);

    // check for string size and string data (current position pointer is not moved yet)
    if (CheckError(sizeof(unsigned int) + size))
    {
        return *this;
    }

    // skip vector size
    m_currentPosition += sizeof(unsigned int);

    // read string data
    // note: control symbols and zero symbol are not processed in special way (remove these symbols?)
    s.assign(m_data.begin() + m_currentPosition, m_data.begin() + m_currentPosition + size);

    // skip string data
    m_currentPosition += size;

    return *this;
}

BDeserializer& BDeserializer::operator>> (uint256_t& ui)
{
    if (CheckError(32))
    {
        return *this;
    }

    // if big endian then reverse vector data?

    vector<unsigned char> v(m_data.begin() + m_currentPosition, m_data.begin() + m_currentPosition + 32);

    // convert vector to unsigned int 256 bits
    // use little endian 
    if (!VectorToUInt256(v, ui, m_littleEndian))
    {
        m_errorState = true;
        m_errorDescription = "couldn't deserialize uint256";
        return *this;
    }

    m_currentPosition += 32;

    return *this;
}

BDeserializer& BDeserializer::operator>> (uint64_t& ui)
{
    if (CheckError(sizeof(uint64_t)))
    {
        return *this;
    }

    // if big endian then reverse vector data?

    vector<unsigned char> v(m_data.begin() + m_currentPosition, m_data.begin() + m_currentPosition + sizeof(uint64_t));

    // convert vector to unsigned int 64 bits
    // use little endian 
    if (!VectorToUInt64(v, ui, m_littleEndian))
    {
        m_errorState = true;
        m_errorDescription = "couldn't deserialize uint64";
        return *this;
    }

    m_currentPosition += sizeof(uint64_t);

    return *this;
}

bool BDeserializer::GetData(char* data, size_t& size)
{
    if (!data)
    {
        m_errorState = true;
        m_errorDescription = "internal error: null data pointer for deserialization";
        return false;
    }

    // check for data size (four bytes before string)
    if (CheckError(sizeof(unsigned int)))
    {
        return false;
    }

    // size is in little endian format always
    size = static_cast<size_t>((static_cast<unsigned int>(m_data[m_currentPosition])) | 
                               ((static_cast<unsigned int>(m_data[m_currentPosition+1])) << 8) |
                               ((static_cast<unsigned int>(m_data[m_currentPosition+2])) << 16) | 
                               ((static_cast<unsigned int>(m_data[m_currentPosition+3])) << 24));

    // check for string size and string data (current position pointer is not moved yet)
    if (CheckError(sizeof(unsigned int) + size))
    {
        return false;
    }

    // skip vector size
    m_currentPosition += sizeof(unsigned int);

    // read data
    memcpy(data, &m_data[m_currentPosition], size);

    m_currentPosition += size;

    return true;
}

bool BDeserializer::GetRawData(unsigned char* data, size_t size)
{
    if (!data)
    {
        m_errorState = true;
        m_errorDescription = "internal error: null data pointer for deserialization";
        return false;
    }

    // check for string data (current position pointer is not moved yet)
    if (CheckError(size))
    {
        return false;
    }

    // read data
    memcpy(data, &m_data[m_currentPosition], size);

    m_currentPosition += size;

    return true;
}

bool BDeserializer::GetData(unsigned char* data, size_t& size)
{
    if (!data)
    {
        m_errorState = true;
        m_errorDescription = "internal error: null data pointer for deserialization";
        return false;
    }

    // check for data size (four bytes before string)
    if (CheckError(sizeof(unsigned int)))
    {
        return false;
    }

    // size is in little endian format always
    size = static_cast<size_t>((static_cast<unsigned int>(m_data[m_currentPosition])) | 
                               ((static_cast<unsigned int>(m_data[m_currentPosition+1])) << 8) |
                               ((static_cast<unsigned int>(m_data[m_currentPosition+2])) << 16) | 
                               ((static_cast<unsigned int>(m_data[m_currentPosition+3])) << 24));

    // check for string size and string data (current position pointer is not moved yet)
    if (CheckError(sizeof(unsigned int) + size))
    {
        return false;
    }

    // skip vector size
    m_currentPosition += sizeof(unsigned int);

    // read data
    memcpy(data, &m_data[m_currentPosition], size);

    m_currentPosition += size;

    return true;
}

bool BDeserializer::GetRawData(vector<unsigned char>& data, size_t size)
{
    if (CheckError(size))
    {
        return false;
    }

    if (size == 0)
    {
        return true;
    }

    // allocate memory for raw data
    data.resize(size);

    // read raw data
    memcpy(&data[0], &m_data[m_currentPosition], size);

    m_currentPosition += size;

    return true;
}

bool BDeserializer::Skip(size_t size)
{
    if (CheckError(size))
    {
        return false;
    }

    m_currentPosition += size;

    return true;
}