#include <string>
#include <set>

#include <boost/lexical_cast.hpp>

#include "connection.h"
#include "connection_msg.h"
#include "csa_impl.h"
#include "csa_type.h"
#include "csa_cxx.h"
#include "crypto.h"
#include "utils.h"
#include "csa_pdbg.h"
#include "csa_cxx_light.h"
#include "blockchain_amount.h"

using std::string;
using namespace Engine;

const static std::set<string> falseMap = {"0", "no", "false", ""};

static inline void copy_error(const std::string & text, pcsa_error *error) {
    if (error) {
        unsigned int size = text.length() - 1;
        if (size > error_string)
            size = error_string;
        strncpy(error->text, text.c_str(), size);
    }
}

static pcsa_result basic_get_wallet_keys(const char* query, const char* walletName, 
                                         const char* password, pcsa_keys_pair* keyPair)
{
    if (keyPair == nullptr)
    {
        SetErrorText("Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    memset(keyPair, 0, sizeof(pcsa_keys_pair));
    PDBG_GET_CACHE_NODE_KEY(walletName, keyPair);

    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op(query)
                .add("wallet_name", walletName)
                .add("encrypted", "0");

        if (password != nullptr)
        {
            mo.add("password",  password)
              .add("encrypted", "1");
        }

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

        Params param = mi.params();
        const std::string& private_key = param.param("private_key");
        if (private_key.empty())
        {
            SetErrorText("Eror private key no exist");
            return PCSA_RES_FAIL;
        }

        if (private_key.size() > sizeof(pcsa_keys_pair::private_key) - 1)
        {
            SetErrorText(StringFormat("Eror private key size '%d' is > then max length '%d'", 
                                      private_key.size(), sizeof(pcsa_keys_pair::private_key) - 1));
            return PCSA_RES_FAIL;
        }

        const std::string& public_key = param.param("public_key");
        if (public_key.empty())
        {
            SetErrorText("Eror public key no exist");
            return PCSA_RES_FAIL;
        }

        if (public_key.size() > sizeof(pcsa_keys_pair::public_key) - 1)
        {
            SetErrorText(StringFormat("Eror public key size '%d' is > then max length '%d'", 
                                      public_key.size(), sizeof(pcsa_keys_pair::public_key) - 1));
            return PCSA_RES_FAIL;
        }

        std::string errorDescription;
        PublicKey publicKey;
        if (!publicKey.SetBase58CheckString(public_key, errorDescription))
        {
            SetErrorText(StringFormat("Public key has invalide base58 encode: '%s'", errorDescription.c_str()));
            return PCSA_RES_FAIL;
        }

        PrivateKey privateKey;
        if (!privateKey.SetBase58CheckString(private_key, errorDescription))
        {
            SetErrorText(StringFormat("Private key has invalide base58 encode: '%s'", errorDescription.c_str()));
            return PCSA_RES_FAIL;
        }

        PDBG_CACHE_NODE_KEY(walletName, publicKey, privateKey);

        strncpy(keyPair->public_key, public_key.c_str(), sizeof(pcsa_keys_pair::public_key) - 1);
        strncpy(keyPair->private_key, private_key.c_str(), sizeof(pcsa_keys_pair::private_key) - 1);

    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result create_node_wallet(const char* walletName, const char* password, pcsa_keys_pair* keyPair)
{
    return basic_get_wallet_keys("create-wallet", walletName, password, keyPair);
}

pcsa_result get_node_wallet_keys(const char* walletName, const char* password, pcsa_keys_pair* keyPair)
{
    return basic_get_wallet_keys("get-wallet-keys", walletName, password, keyPair);
}

static pcsa_result  basic_get_state(const char* id, pcsa_wallet_state* state, char byKey)
{
    if (state == nullptr)
    {
        SetErrorText("Internal error: pcsa_wallet_state is null");
        return PCSA_RES_FAIL;
    }

    memset(state, 0, sizeof(pcsa_wallet_state));

    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("get-wallet-state");
        if (byKey)
        {
           mo.add("public_key", id);
        } else
        {
            mo.add("wallet_name", id);
        }

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

        Params param = mi.params();

        Params balance = param.get_child("balance");
        unsigned size = balance.size();
        if (size != 0)
        {
            state->assets = (pcsa_supported_asset *) malloc(sizeof(pcsa_supported_asset) * size);

            if (state->assets == nullptr)
            {
                throw(std::bad_alloc());
            }
        }

        int i = 0;
        for (const auto& asset: balance)
        {
            Params assetParam(asset.second);

            state->assets[i].code = assetParam.param_uint("asset_code");
            state->assets[i].asset_name.name[0] = 0; 
            std::string amount = assetParam.param("amount");
            if (!amount.empty())
            {
                strncpy(state->assets[i].asset_name.name, amount.c_str(), sizeof(pcsa_supported_asset::asset_name) - 1);
                state->assets[i].asset_name.name[std::min(amount.size(), sizeof(pcsa_supported_asset::asset_name) - 1)] = 0;
            }
            ++i;
        }

        if (i != size)
        {
            free(state->assets);
            return PCSA_RES_FAIL;
        }
        state->assets_length = size;

        Params tags = param.get_child("tags");

        std::string tagString;
        for (const auto& tag: tags)
        {
            Params tagsParam(tag.second);

            std::string temp = tagsParam.param();
            tagString += tagString.empty() ? temp : std::string(", ") + temp; 
        }

        strncpy(state->tags, tagString.c_str(), sizeof(pcsa_wallet_state::tags) - 1);

        strncpy(state->node_address, param.param("node_address").c_str(), 
                sizeof(pcsa_wallet_state::node_address) - 1);
        strncpy(state->last_transaction_id, param.param("last_transaction_id").c_str(), 
                sizeof(pcsa_wallet_state::last_transaction_id) - 1);

        state->exist = falseMap.find(param.param("exist")) == falseMap.end();
    }
    catch(std::exception const &e)
    {
        if (state->assets != nullptr)
        {
            free(state->assets);
        }

        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  transfer_assets_transaction_data(const char *trxData, pcsa_error *error)
{
    try
    {
        XMsgOut mo; XMsgIn mi;

        std::string h(trxData);

        PDBG("Transfer_assets_transaction_data[%i] %s", h.length(), h.c_str());

        mo.set_op("add-transaction")
            .add("transaction_name", "TransferAssetsTransaction")
            .add("transaction_data", h);

        get_connection()->request(mo, mi);


        PDBG("Transfer_assets_transaction_data result %s", mi.text().c_str());

        if (mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            copy_error(mi.text(), error);
            return (pcsa_result)mi.result();
        }
    }
    catch (std::exception const &e)
    {
        PDBG_ERR("EXCEPTION: %s", e.what());
        copy_error(e.what(), error);
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  get_wallet_state_by_key(const char* pubKey, pcsa_wallet_state* state)
{
    return basic_get_state(pubKey, state, 1);
}

pcsa_result  get_wallet_state(const char* walletName, pcsa_wallet_state* state)
{
    return basic_get_state(walletName, state, 0);
}

pcsa_result  get_node_wallet_list(pcsa_string_literal** walletList, unsigned* walletCount)
{
    *walletList = nullptr;
    *walletCount = 0;
    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("get-local-wallet-list");

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

        Params param = mi.params().get_child("wallet_name");
        unsigned size = param.size();
        if (size == 0)
        {
            return PCSA_RES_OK;
        }

        *walletList = (pcsa_string_literal *) malloc(sizeof(pcsa_string_literal) * size);

        if (*walletList == nullptr)
        {
            throw(std::bad_alloc());
        }

        unsigned i = 0;
        pcsa_string_literal* point = *walletList;

        for (const auto& tree: param)
        {
            Params innerParam(tree.second);
            string wallet = innerParam.param();
            strncpy(point[i].name, wallet.c_str(), sizeof(pcsa_string_literal::name) - 1); 
            point[i].name[std::min(wallet.size(), sizeof(pcsa_string_literal::name) - 1)] = 0;  
            ++i;
        }

        if (i != size)
        {
            free(*walletList);
            return PCSA_RES_FAIL;
        }
        *walletCount = i;
    }
    catch(std::exception const &e)
    {
        if (*walletList != nullptr)
        {
            free(*walletList);
        }

        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  register_node(const char* walletName, const char* address)
{
    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("register-node")
          .add("wallet_name", walletName)
          .add("address", address);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  unregister_node(const char* walletName)
{
    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("unregister-node")
          .add("wallet_name", walletName);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  transfer_asset(const char* asset, const char* srcWallet, 
                            const char* dstWallet, const char* amount)
{
    try
    {
        XMsgOut mo; XMsgIn mi;

        blockchain_amount_t amountType;
        if (!StringToBlockchainAmount(amount, amountType))
        {
            SetErrorText(StringFormat("Could't parse amount string %s", amount));
            return PCSA_RES_FAIL;
        }

        mo.set_op("transfer-assets")
          .add("asset", asset)
          .add("source_wallet_name", srcWallet)
          .add("destination_wallet", dstWallet)
          .add("amount", amount);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

static pcsa_result basic_get_wallet_transactions(const char* id, unsigned count,
                                                 pcsa_wallet_transaction** transaction,
                                                 unsigned* transactionCount, char byKey)
{
    *transaction = nullptr;
    *transactionCount = 0;
    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("get-wallet-transactions")
                .add("count", boost::lexical_cast<std::string>(count));

        if (byKey)
        {
            mo.add("public_key", id);
        } else
        {
            mo.add("wallet_name", id);
        }

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

        Params param = mi.params().get_child("transaction_list");
        unsigned size = param.size();
        if (size == 0)
        {
            return PCSA_RES_OK;
        }

        *transaction = (pcsa_wallet_transaction*) malloc(sizeof(pcsa_wallet_transaction) * size);
        if (*transaction == nullptr)
        {
            throw(std::bad_alloc());
        }

        unsigned i = 0;
        pcsa_wallet_transaction* temp;

        for (const auto& tree: param)
        {
            Params innerParam(tree.second);
            temp = *transaction + i;

            string description = innerParam.param("description");
            strncpy(temp->description, description.c_str(), sizeof(pcsa_wallet_transaction::description) - 1);
            temp->description[sizeof(pcsa_wallet_transaction::description) - 1] = 0;

            string digest = innerParam.param("digest");
            strncpy(temp->digest, digest.c_str(), sizeof(pcsa_wallet_transaction::digest) - 1);
            temp->digest[sizeof(pcsa_wallet_transaction::digest) - 1] = 0;

            string type = innerParam.param("type");
            strncpy(temp->type, type.c_str(), sizeof(pcsa_wallet_transaction::type) - 1);
            temp->type[sizeof(pcsa_wallet_transaction::type) - 1] = 0;

            ++i;
        }

        if (i != size)
        {
            free(*transaction);
            return PCSA_RES_FAIL;
        }
        *transactionCount = i;

    }
    catch(...)
    {
        if (*transaction != nullptr)
        {
            free(*transaction);
            *transactionCount = 0;
        }

        SetErrorText("Internal Error: fail read transaction data");
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result  get_wallet_transactions(const char* walletName, unsigned count,
                                     pcsa_wallet_transaction** transaction, unsigned* transactionCount)
{
    return basic_get_wallet_transactions(walletName, count,
                                         transaction, transactionCount, 0);
}

pcsa_result  get_wallet_transactions_by_key(const char* pubKey, unsigned count,
                                     pcsa_wallet_transaction** transaction, unsigned* transactionCount)
{
    return basic_get_wallet_transactions(pubKey, count,
                                         transaction, transactionCount, 1);
}

pcsa_result get_blockchain_info(pcsa_blockchain_info* info)
{
    if (info == nullptr)
    {
        SetErrorText("Internal error: pcsa_blockchain_info is null");
        return PCSA_RES_FAIL;
    }

    memset(info, 0, sizeof(pcsa_blockchain_info));
    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("get-blockchain-info");

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

        Params param = mi.params();
        strncpy(info->project_name, param.param("project").c_str(), 
                sizeof(pcsa_blockchain_info::project_name) - 1);

        strncpy(info->version, param.param("version").c_str(), 
                sizeof(pcsa_blockchain_info::version) - 1);

        Params supportedTransactions = param.get_child("supported_transactions");
        unsigned size = supportedTransactions.size();
        unsigned i = 0; 
        if (size != 0)
        {
            info->supported_transactions = (pcsa_string_literal *) malloc(sizeof(pcsa_string_literal) * size);

            if (info->supported_transactions == nullptr)
            {
                throw(std::bad_alloc());
            }

            for (const auto& transaction: supportedTransactions)
            {
                Params transactionParam(transaction.second);

                std::string transactionString = transactionParam.param();
                strncpy(info->supported_transactions[i].name, transactionString.c_str(),
                        sizeof(pcsa_string_literal::name) - 1); 
                info->supported_transactions[i].name[std::min(transactionString.size(), sizeof(pcsa_string_literal::name) - 1)] = 0;  
                ++i;
            }

            if (i != size)
            {
                throw std::bad_alloc();
            }
            info->transactions_count = i;

        }

        Params supportedAssets = param.get_child("supported_assets");
        size = supportedAssets.size();
        if (size == 0)
        {
            return PCSA_RES_OK;
        }

        info->supported_assets = (pcsa_supported_asset *) malloc(sizeof(pcsa_supported_asset) * size);
        if (info->supported_assets == nullptr)
        {
            throw(std::bad_alloc());
        }

        i = 0;

        for (const auto& asset: supportedAssets)
        {
            Params assetParam(asset.second);

            info->supported_assets[i].code = assetParam.param_uint("code");
            info->supported_assets[i].asset_name.name[0] = 0; 
            std::string amount = assetParam.param("name");
            if (!amount.empty())
            {
                strncpy(info->supported_assets[i].asset_name.name, amount.c_str(), sizeof(pcsa_supported_asset::asset_name) - 1);
                info->supported_assets[i].asset_name.name[std::min(amount.size(), sizeof(pcsa_supported_asset::asset_name) - 1)] = 0;
            }
            ++i;
        }

        if (i != size)
        {
            throw std::bad_alloc();
        }
        info->assets_count = i;


        Params supportedWaletTags = param.get_child("supported_wallet_tags");
        size = supportedWaletTags.size();
        i = 0;
        if (size != 0)
        {
            info->supported_wallet_tags = (pcsa_supported_wallet_tags *) malloc(sizeof(pcsa_supported_wallet_tags) * size);

            if (info->supported_wallet_tags == nullptr)
            {
                throw(std::bad_alloc());
            }
            
            for (const auto& tags: supportedWaletTags)
            {
                Params tagsParam(tags.second);

                info->supported_wallet_tags[i].code = tagsParam.param_uint("code");
                info->supported_wallet_tags[i].asset_name.name[0] = 0; 
                std::string name = tagsParam.param("name");
                if (!name.empty())
                {
                    strncpy(info->supported_wallet_tags[i].asset_name.name, name.c_str(), sizeof(pcsa_supported_wallet_tags::asset_name) - 1);
                    info->supported_wallet_tags[i].asset_name.name[std::min(name.size(), sizeof(pcsa_supported_wallet_tags::asset_name) - 1)] = 0;
                }
                ++i;
            }

            if (i != size)
            {
                throw std::bad_alloc();
            }
            info->wallet_tags_count = i;
        }

        std::string base_assets = param.param("base_assets_for_node_registration");
        strncpy(info->base_assets, base_assets.c_str(), sizeof(info->base_assets) - 1);
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        if (info->supported_transactions != nullptr)
        {
            free(info->supported_transactions);
            info->transactions_count = 0;
        }

        if (info->supported_assets != nullptr)
        {
            free(info->supported_assets);
            info->assets_count = 0;
        }

        if (info->supported_wallet_tags != nullptr)
        {
            free(info->supported_wallet_tags);
            info->wallet_tags_count = 0;
        }

        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}


pcsa_result get_block(const char* id, char** data, char byId)
{
    if (data == nullptr)
    {
        SetErrorText("Internal error: get block data in null");
        return PCSA_RES_FAIL;
    }

    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("get-block")
           .add(byId ? "block_id" : "block_digest", id);

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

       std::string blockHex = mi.params().param("block_data");
       if (blockHex.empty())
       {
            SetErrorText("Get empty block");
            return PCSA_RES_FAIL;
       }

       char* errorMessage = nullptr;
       if (deserialize_block(blockHex.c_str(), data, &errorMessage))
       {
            if (errorMessage != nullptr)
            {
                SetErrorText(StringFormat("Couldn't serialize block: '%s'", errorMessage));
                free(errorMessage);
                return PCSA_RES_FAIL;
            }
            SetErrorText("Couldn't serialize block");
            return PCSA_RES_FAIL;
       }
    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result get_block_by_digest(const char* digest, char** data)
{
    return get_block(digest, data, 0);
}

pcsa_result get_block_by_id(const char* id, char** data)
{
    return get_block(id, data, 1);
}

pcsa_result get_blockchain_state(pcsa_blockchain_state* state)
{
    if (state == nullptr)
    {
        SetErrorText("Internal error: pcsa_blockchain_state is null");
        return PCSA_RES_FAIL;
    }
    
    memset(state, 0, sizeof(pcsa_blockchain_state));
    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("get-blockchain-state");

        get_connection()->request(mo, mi);

        if(mi.result())
        {
            PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), mi.text().c_str());
            return (pcsa_result)mi.result();
        }

        Params param = mi.params();

        strncpy(state->block_count, param.param("block_count").c_str(), 
                sizeof(pcsa_blockchain_state::block_count) - 1);

        strncpy(state->last_block_digest, param.param("last_block_digest").c_str(), 
                sizeof(pcsa_blockchain_state::last_block_digest) - 1);


        strncpy(state->last_block_merkle_root, param.param("last_block_merkle_root").c_str(), 
                sizeof(pcsa_blockchain_state::last_block_merkle_root) - 1);

        strncpy(state->transaction_count, param.param("transaction_count").c_str(), 
                sizeof(pcsa_blockchain_state::transaction_count) - 1);

        strncpy(state->node_count, param.param("node_count").c_str(), 
                sizeof(pcsa_blockchain_state::node_count) - 1);

        state->local_node = falseMap.find(param.param("local_node")) == falseMap.end();

        strncpy(state->non_empty_wallet_count, param.param("non_empty_wallet_count").c_str(), 
                sizeof(pcsa_blockchain_state::non_empty_wallet_count) - 1);

        strncpy(state->voting_transaction_count, param.param("voing_transaction_count").c_str(), 
                sizeof(pcsa_blockchain_state::voting_transaction_count) - 1);

        strncpy(state->pending_transaction_count, param.param("pending_transaction_count").c_str(), 
                sizeof(pcsa_blockchain_state::pending_transaction_count) - 1);

        strncpy(state->blockchain_state, param.param("blockchain_state").c_str(), 
                sizeof(pcsa_blockchain_state::blockchain_state) - 1);

        const std::string& synchronization_state = param.param("synchronization_state");
        if (!synchronization_state.empty())
        {
            state->synchronization_state = strdup(synchronization_state.c_str());
        }

        strncpy(state->consensus_round, param.param("consensus_round").c_str(), 
                sizeof(pcsa_blockchain_state::consensus_round) - 1);

        const std::string& voting_nodes = param.param("voting_nodes");
        if (!voting_nodes.empty())
        {
            state->voting_nodes = strdup(voting_nodes.c_str());
        }

    }
    catch(std::exception const &e)
    {
        SetErrorText(StringFormat("EXCEPTION: %s", e.what()));
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

