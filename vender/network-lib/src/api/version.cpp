/* 
 * version.cpp
 */

#include "version.h"
#include <stdio.h>

#if !defined(MAJOR_VERSION)
    #error MAJOR_VERSION not defined in CMakeLists.txt
#endif
#if !defined(MINOR_VERSION)
    #error MINOR_VERSION not defined in CMakeLists.txt
#endif

namespace csa
{
    // Module version
    static const unsigned short cMajorVersion = MAJOR_VERSION;
    static const unsigned short cMinorVersion = MINOR_VERSION;
#if defined(DEBUG_BUILD)
    static const char* cVersionSuffix = "d";
#else
    static const char* cVersionSuffix = "";
#endif

    // Module build date
    // note: use const string in case of release version
    static const char* cBuildDateString = BUILD_DATE; // or "2016.06.08"

    // Get SHELL version string
    const char* GetVersionString(void)
    {
        const int nStrLen = 32;
        static char versionString[nStrLen] = {0};
        snprintf(versionString, nStrLen, "%u.%02u%s", cMajorVersion, cMinorVersion, cVersionSuffix);
        return versionString;
    }

    // Get SHELL build-date string
    const char* GetBuildDateString(void)
    {
        return cBuildDateString;
    }

}  //namespace csa
