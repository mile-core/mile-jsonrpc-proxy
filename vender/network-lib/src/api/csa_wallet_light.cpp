#include <cstring>
#include <string>

#include "crypto_types.h"
#include "crypto.h"
#include "csa_type_light.h"
#include "csa_cxx_light.h"
#include "utils.h"
#include "deserializer_block.h"

#include "connection_msg.h"

pcsa_result generate_key_pair(pcsa_keys_pair* keyPair, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }
    memset(keyPair, 0, sizeof(pcsa_keys_pair));

    PrivateKey privateKey;
    PublicKey publicKey;
    CreateKeyPair(privateKey, publicKey);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(keyPair->private_key, privateKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::private_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result generate_key_pair_from_private_key(pcsa_keys_pair* keyPair, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }
    memset(keyPair->public_key, 0, sizeof(pcsa_keys_pair::public_key));

    std::string errorDescription;
    PrivateKey privateKey;
    if (!privateKey.SetBase58CheckString(keyPair->private_key, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Error read private key: %s", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    PublicKey publicKey;
    RestoreKeyPairFromPrivate(privateKey, publicKey);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result generate_key_pair_with_secret_phrase(pcsa_keys_pair* keyPair, const char* phrase, 
                                                 size_t length, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    if (phrase == nullptr || length == 0)
    {
        LogAndSafeError(errorMessage, "Internal error: secret phrase is empty");
        return PCSA_RES_FAIL;
    }

    memset(keyPair, 0, sizeof(pcsa_keys_pair));

    Seed seed;
    seed.Clear();
    sha3_256((const unsigned char*) phrase, length, seed.Data.data());

    PrivateKey privateKey;
    PublicKey publicKey;
    CreateKeyPair(privateKey, publicKey, seed);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(keyPair->private_key, privateKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::private_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result deserialize_block(const char* blockHex, char** deserialaizedBlock, char** errorMessage)
{
    if (blockHex == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: block is null");
        return PCSA_RES_FAIL;
    }
    vector<unsigned char> vectorData;
    Block block;
    std::string errorDescription;
    if (!HexToBin(blockHex, vectorData, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Error hex to bin transform: '%s'", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    if (!deserializeBlock(vectorData, block, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Error deserialize block: '%s'", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    std::vector<std::pair<std::string, std::string>> result;
    Engine::XMsgOut out;
    block.header.Description(result);
    for (const auto& value: result)
    {
        out.add(value.first, value.second, false);
    }
    result.clear();

    Engine::XMsgOut signatures;
    for (const auto& signature: block.signatures)
    {
        signature.Description(result);
        Engine::XMsgOut outSignature;
        for (const auto& value: result)
        {
            outSignature.add(value.first, value.second, false);
        }
        signatures.push_back(outSignature);
        result.clear();
    }

    out.add_child("signature", signatures, false);

    Engine::XMsgOut transactions;
    for (const auto& transaction: block.transactions)
    {
        transaction->Description(result);
        Engine::XMsgOut outTransaction;
        for (const auto& value: result)
        {

            outTransaction.add(value.first, value.second, false);
        }
        transactions.push_back(outTransaction);
        result.clear();

    }

    out.add_child("transactions",transactions,false);

    std::string jsonFormat = out.asString();
    *deserialaizedBlock = (char *) malloc(jsonFormat.length() + 1);
    if (*deserialaizedBlock == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: memory alloc");
        return PCSA_RES_FAIL;
    }

    strncpy(*deserialaizedBlock, jsonFormat.c_str(), jsonFormat.length());
    (*deserialaizedBlock)[jsonFormat.length()] = '\0';

    return PCSA_RES_OK;
}