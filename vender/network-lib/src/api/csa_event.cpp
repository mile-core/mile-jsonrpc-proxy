#include <string>

#include "csa_event.h"
#include "csa_event_parse.h"
#include "connection_msg.h"


#define REGISTER_EVENT(EVENT, ...) \
                        static void (*event_ ## EVENT)(__VA_ARGS__) = nullptr; \
                        void pcsa_register_event_ ## EVENT(void (*func)(__VA_ARGS__)) \
                        {\
                            event_ ## EVENT = func;\
                        }

#define CALL_EVENT(EVENT, ...) event_ ## EVENT(__VA_ARGS__)\

#define CHECK_EVENT(EVENT)\
                        do { \
                            if (*event_ ## EVENT == nullptr)\
                                return;\
                        } while(0)


REGISTER_EVENT(announce_transaction, const char* block_id, 
                                     const char *last_block_digest,
                                     const char* transaction_name,
                                     const char* transaction_data)

REGISTER_EVENT(start_consensus, const char* block_id, const char *last_block_digest)

REGISTER_EVENT(announce_block, const char* block_id, const char *last_block_digest,
                               const char* block_digest)

REGISTER_EVENT(consensus_digest_transaction_block, const char* digest_data, 
                                                   const char *digest_data_signature)

REGISTER_EVENT(consensus_mask_transaction_block, const char* mask_data, 
                                                 const char *mask_data_signature)

REGISTER_EVENT(consensus_block_signature, const char* last_block_digest, 
                                          const char *next_block_signature)

REGISTER_EVENT(text_message, const char* text)

REGISTER_EVENT(new_block, const char* block_id, const char* block_digest)




void announce_transaction(const Engine::Params& params)
{
    CHECK_EVENT(announce_transaction);
    std::string block_id = params.param("block_id");
    std::string last_block_digest = params.param("last_block_digest");
    std::string transaction_name = params.param("transaction_name");
    std::string transaction_data = params.param("transaction_data");
    CALL_EVENT(announce_transaction, block_id.c_str(), last_block_digest.c_str(), 
               transaction_name.c_str(), transaction_data.c_str());

}



void start_consensus(const Engine::Params& params)
{
    CHECK_EVENT(start_consensus);
    std::string block_id = params.param("block_id");
    std::string last_block_id = params.param("last_block_id");
    CALL_EVENT(start_consensus, block_id.c_str(), last_block_id.c_str());
}



void announce_block(const Engine::Params& params)
{
    CHECK_EVENT(announce_block);
    std::string block_id = params.param("block_id");
    std::string last_block_id = params.param("last_block_digest");
    std::string block_digest = params.param("block_digest");
    CALL_EVENT(announce_block, block_id.c_str(), last_block_id.c_str(), block_digest.c_str());

}



void consensus_digest_transaction_block(const Engine::Params& params)
{
    CHECK_EVENT(consensus_digest_transaction_block);
    std::string digest_data = params.param("digest_data");
    std::string digest_data_signature = params.param("digest_data_signature");
    CALL_EVENT(consensus_digest_transaction_block, digest_data.c_str(), digest_data_signature.c_str());

}



void consensus_mask_transaction_block(const Engine::Params& params)
{
    CHECK_EVENT(consensus_mask_transaction_block);
    std::string mask_data = params.param("mask_data");
    std::string mask_data_signature = params.param("mask_data_signature");
    CALL_EVENT(consensus_mask_transaction_block, mask_data.c_str(), mask_data_signature.c_str());

}



void consensus_block_signature(const Engine::Params& params)
{
    CHECK_EVENT(consensus_block_signature);
    std::string last_block_digest = params.param("last_block_digest");
    std::string next_block_signature = params.param("next_block_signature");
    CALL_EVENT(consensus_block_signature, last_block_digest.c_str(), next_block_signature.c_str());

}

void text_message(const Engine::Params& params)
{
    CHECK_EVENT(text_message);
    std::string text = params.param("text");
    CALL_EVENT(text_message, text.c_str());

}

void new_block(const Engine::Params& params)
{
    CHECK_EVENT(new_block);
    std::string block_id = params.param("block_id");
    std::string block_digest = params.param("block_digest");
    CALL_EVENT(new_block, block_id.c_str(), block_digest.c_str());

}