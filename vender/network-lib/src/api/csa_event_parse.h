#pragma once

namespace Engine
{
	class Params;
}

void announce_transaction(const Engine::Params& params);
void start_consensus(const Engine::Params& params);
void announce_block(const Engine::Params& params);
void consensus_digest_transaction_block(const Engine::Params& params);
void consensus_mask_transaction_block(const Engine::Params& params);
void consensus_block_signature(const Engine::Params& params);
void text_message(const Engine::Params& params);
void new_block(const Engine::Params& params);
