/*
 * csa.cpp
 *
 * Potok Control System Access (pcsa) Library
 */
#include <unordered_map>

#include "csa_impl.h"
#include "csa_pdbg.h"
#include "csa_cxx.h"
#include "connection_msg.h"
#include "connection_error.h"
#include "connection.h"
#include "version.h"
#include "csa.h"

using namespace std;
using namespace Engine;

//**************************************************************************************************
class pcsa_result_str_table
{
public:
    pcsa_result_str_table();
    const char * get_str(pcsa_result r) const;
private:
    unordered_map<int, const char *> table_;
};

pcsa_result_str_table::pcsa_result_str_table()
{
    table_.insert({PCSA_RES_OK, "OK"});
    table_.insert({PCSA_RES_FAIL, "Control System error"});
    table_.insert({PCSA_RES_INVALID_PARAM, "Invalid request parameter"});
    table_.insert({PCSA_RES_TIMEOUT, "Timeout"});
    table_.insert({PCSA_RES_ACCESS_DENIED, "Access denied"});
    table_.insert({PCSA_RES_LOGOUT, "Logout"});
    table_.insert({PCSA_RES_RESOURCE_UNAVAILABLE, "Resource unavailable"});
    table_.insert({PCSA_RES_NOT_SUPPORTED, "Not implemented or not supported"});
    table_.insert({PCSA_RES_NOT_FOUND, "Requested object not found"});
    table_.insert({PCSA_RES_ALREADY_EXIST, "Already exist"});
    table_.insert({PCSA_RES_EXCEPTION, "Exception raised"});
}

const char * pcsa_result_str_table::get_str(pcsa_result r) const
{
    const char *str = "";

    try
    {
        str = table_.at(r);
    }
    catch(std::out_of_range)
    {
        str = "Unknown error";
    }

    return str;
}

const char * pcsa_result_to_str(pcsa_result r)
{
    static pcsa_result_str_table table;
    return table.get_str(r);
}

//**************************************************************************************************

pcsa_result pcsa_get_last_cs_result()
{
    return get_connection()->last_cs_result();
}


void pcsa_get_last_cs_error_text(char *buf, unsigned buf_size)
{
    strncpy(buf, get_connection()->last_cs_error_text().c_str(), buf_size-1);
    buf[buf_size-1] = 0;
}

void pcsa_clear_last_cs_error_text()
{
    get_connection()->set_last_cs_result(0);
    get_connection()->set_last_cs_error_text("");
}


const char* pcsa_get_version_str()
{
    //return "PCSA 1.0.0";
    return csa::GetVersionString();
}


void pcsa_on_connect(void (*cb)(int code, const char *msg))
{
    get_connection()->set_network_failure_callback(cb);
}

void pcsa_on_auth(void (*cb)(int code, const char *msg))
{
    get_connection()->set_network_auth_callback(cb);
}

void pcsa_destroy(void)
{
    try
    {
        destroy_connection();
    }
    catch (std::exception exp)
    {
        PDBG_ERR("EXCEPTION: %s", exp.what());
    }
    catch (...)
    {
        PDBG_ERR("EXCEPTION: <unknown>");
    }
}

pcsa_result debug_request(const char *request, char** response)
{
    *response = nullptr;

    try
    {
        XMsgOut mo; XMsgIn mi;

        mo.set_op("debug").add("text", request);
        get_connection()->request(mo, mi);
        const std::string& textResult = mi.text();
        if (!textResult.empty())
        {
            *response = strdup(textResult.c_str());
        }

        if(mi.result() != 0)
        {
            if (*response != nullptr)
            {
                PDBG_ERR("mi.result()=%d mi.text()=%s", mi.result(), textResult.c_str());
            }

            return (pcsa_result)mi.result();
        }
    }
    catch(std::exception const &e)
    {
        PDBG_ERR("EXCEPTION: %s", e.what());
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result pcsa_connect(const char* ip_address,
                         unsigned short port,
                         const char* user_name,
                         const char* client)
{
    try
    {
        get_connection()->connect(ip_address, port, user_name, client);
    }
    catch (Engine::error const &e)
    {
        PDBG_ERR("EXCEPTION: %s", e.what());
        return (e.code());
    }
    catch (std::exception const &e)
    {
        PDBG_ERR("EXCEPTION: %s", e.what());
        return (PCSA_RES_FAIL);
    }

    return PCSA_RES_OK;
}

pcsa_result pcsa_disconnect(void)
{
    get_connection()->disconnect();

    return PCSA_RES_OK;
}