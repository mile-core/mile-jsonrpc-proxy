// Potok-KM-2420 project config
#ifndef VERSION_H
#define VERSION_H

namespace csa
{
    // Get LIBCSA version string
    const char* GetVersionString(void);
    // Get LIBCSA build-date string
    const char* GetBuildDateString(void);
}

#endif
