#include <cstring>
#include <string>

#include "crypto_container.h"
#include "csa_cxx_light.h"
#include "crypto.h"
#include "utils.h"
#include "csa_pdbg.h"

#include "csa_cxx_container.h"

pcsa_result create_local_wallet(const char* walletName, 
                                const char* password, pcsa_keys_pair* keyPair, 
                                char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    memset(keyPair, 0, sizeof(pcsa_keys_pair));
    PrivateKey privateKey;
    PublicKey publicKey;
    CreateKeyPair(privateKey, publicKey);

    std::string errorDescription;
    if (wallet_container_create(walletName, password ? password : "", 
                                privateKey, publicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL;
    }

    PDBG_CACHE_LOCAL_KEY(walletName, publicKey, privateKey);

    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(keyPair->private_key, privateKey.ToBase58CheckString().c_str(), 
            sizeof(pcsa_keys_pair::private_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result create_local_wallet_by_keys(const char* walletName, 
                                        const char* password, pcsa_keys_pair* keyPair,
                                        char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    std::string errorDescription;
    PublicKey publicKey;
    if (!publicKey.SetBase58CheckString(keyPair->public_key, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Couldn't read public key: %s", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    PrivateKey privateKey;
    if (!privateKey.SetBase58CheckString(keyPair->private_key, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Couldn't read private key: %s", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    if (generate_key_pair_from_private_key(keyPair, errorMessage))
    {
        LogAndSafeError(errorMessage, StringFormat("Couldn't check crypto pairs: %s", (*errorMessage ? *errorMessage : "")).c_str());
        if (*errorMessage != nullptr)
        {
            free(errorMessage);
        }

        return PCSA_RES_FAIL;
    }

    PublicKey publicKeyCheck;
    if (!publicKeyCheck.SetBase58CheckString(keyPair->public_key, errorDescription))
    {
        LogAndSafeError(errorMessage, StringFormat("Couldn't read public key: %s", errorDescription.c_str()).c_str());
        return PCSA_RES_FAIL;
    }

    if (publicKeyCheck != publicKey)
    {
        LogAndSafeError(errorMessage, "Wrong crypto pairs");
        return PCSA_RES_FAIL;
    }

    if (wallet_container_create(walletName, password ? password : "", 
                                privateKey, publicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL;
    }

    PDBG_CACHE_LOCAL_KEY(walletName, publicKey, privateKey);

    return PCSA_RES_OK;
}

pcsa_result get_local_wallet_keys(const char* walletName, const char* password, 
                                  pcsa_keys_pair* keyPair, char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    memset(keyPair, 0, sizeof(pcsa_keys_pair));
    PDBG_GET_CACHE_LOCAL_KEY(walletName, keyPair);

    PrivateKey privateKey;
    PublicKey  publicKey;    
    

    std::string errorDescription;
    if (wallet_container_read(walletName, password ? password : "", privateKey, publicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL; 
    }

    PDBG_CACHE_LOCAL_KEY(walletName, publicKey, privateKey);
    
    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), sizeof(pcsa_keys_pair::public_key) - 1);
    strncpy(keyPair->private_key, privateKey.ToBase58CheckString().c_str(), sizeof(pcsa_keys_pair::private_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result get_local_wallet_public_key(const char* walletName, pcsa_keys_pair* keyPair,
                                        char** errorMessage)
{
    if (keyPair == nullptr)
    {
        LogAndSafeError(errorMessage, "Internal error: pcsa_keys_pair is null");
        return PCSA_RES_FAIL;
    }

    memset(keyPair, 0, sizeof(pcsa_keys_pair));

    PublicKey  publicKey;
    std::string errorDescription;
    if (wallet_container_read_public(walletName, publicKey, errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL; 
    }
    
    strncpy(keyPair->public_key, publicKey.ToBase58CheckString().c_str(), sizeof(pcsa_keys_pair::public_key) - 1);

    return PCSA_RES_OK;
}

pcsa_result delete_local_wallet(const char* walletName, const char* password, 
                                char** errorMessage)
{
    std::string errorDescription;
    if (wallet_container_delete(walletName, password ? password : "", errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL; 
    }

    PDBG_DEL_CACHE_NODE_KEY(walletName);

    return PCSA_RES_OK;
}

pcsa_result update_password_local_wallet(const char* walletName, const char* oldPassword, 
                                         const char* newPassword, char** errorMessage)
{
    std::string errorDescription;

    if (wallet_container_update_password(walletName, oldPassword ? oldPassword : "" , 
                                         newPassword ? newPassword : "", errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL; 
    }

    return PCSA_RES_OK;
}


pcsa_result  get_local_wallet_list(pcsa_string_literal** walletList, 
                                   unsigned* walletCount,
                                   char** errorMessage)
{
    *walletList = nullptr;
    *walletCount = 0;

    std::vector<std::pair<std::string, PublicKey>> wallets;

    std::string errorDescription;
    if (wallet_container_list_wallets(wallets, errorDescription))
    {
        LogAndSafeError(errorMessage, errorDescription.c_str());
        return PCSA_RES_FAIL;
    }

    if (wallets.empty())
    {
        return PCSA_RES_OK;
    }

    unsigned size = wallets.size();
    *walletList = (pcsa_string_literal*)malloc(sizeof(pcsa_string_literal) * size);

    if (*walletList == nullptr)
    {
        return PCSA_RES_FAIL;  
    }

    unsigned i = 0;
    pcsa_string_literal* point = *walletList;

    for (const auto& wallet: wallets)
    {
        strncpy(point[i].name, wallet.first.c_str(), sizeof(pcsa_string_literal::name) - 1); 
        point[i].name[std::min(wallet.first.size(), sizeof(pcsa_string_literal::name) - 1)] = 0;  
        ++i;
    }

    if (i != size)
    {
        free(*walletList);
        return PCSA_RES_FAIL;
    }
    *walletCount = i;

    return PCSA_RES_OK;
}