#include <boost/filesystem.hpp>

#include <crypto_encrypt.h>

#include "csa_pdbg.h"
#include "crypto_container.h"
#include "utils.h"

static const std::string walletPrefix = "wallets/";
static const std::string walletPostfix = ".wallet";

pcsa_result wallet_container_create(const std::string& walletName, const std::string& password, 
                                    const PrivateKey& privateKey, const PublicKey& publicKey,
                                    std::string& errorDescription)
{
    using namespace boost::filesystem;
    path walletDir(walletPrefix);
    if (!exists(walletDir))
    {
        boost::system::error_code ec;
        if (!create_directory(walletDir, ec))
        {
            errorDescription = StringFormat("Internal error. Couldn't create dir '%s': '%s'", 
                                            walletPrefix.c_str(), ec.message().c_str());
            return PCSA_RES_FAIL;
        }
    } else if (!is_directory(walletDir))
    {
        errorDescription = StringFormat("Internal error. Couldn't create dir '%s'"
                                        " because file with same name already exist", 
                                        walletPrefix.c_str());
        return PCSA_RES_FAIL;
    }

    path wallet(walletPrefix + walletName + walletPostfix);
    if (exists(wallet))
    {
        errorDescription = StringFormat("Internal error. Wallet '%s': 'already exist'", walletName.c_str());
        return PCSA_RES_FAIL;
    }

    ContainerCipher container;

    std::string errorString;
    std::vector<unsigned char> publicVector(publicKey.Data.begin(), publicKey.Data.end());
    std::vector<unsigned char> privateVector(privateKey.Data.begin(), privateKey.Data.end());

    if (!container.saveKey(walletPrefix + walletName + walletPostfix, publicVector, privateVector,
                           password, errorString))
    {
        errorDescription = StringFormat("Internal error. Error create container '%s': '%s'", 
                                        walletName.c_str(), errorString.c_str());
        return PCSA_RES_FAIL;
    }
    return PCSA_RES_OK;
}

pcsa_result wallet_container_update_password(const std::string& walletName, const std::string& oldPassword,
                                             const std::string& newPassword, std::string& errorDescription)
{
    using namespace boost::filesystem;
    path wallet(walletPrefix + walletName + walletPostfix);
    if (!exists(wallet) || !is_regular(wallet))
    {
        errorDescription = StringFormat("Internal error. Wallet '%s': 'not exist'", walletName.c_str());
        return PCSA_RES_FAIL;
    }
    ContainerCipher container;

    std::string errorString;
    if (!container.setNewPassword(walletPrefix + walletName + walletPostfix, oldPassword, newPassword, errorString))
    {
        errorDescription = StringFormat("Internal error. Error update container password '%s': '%s'", walletName.c_str(), errorString.c_str());
        return PCSA_RES_FAIL;
    }
    return PCSA_RES_OK;
}

pcsa_result wallet_container_read(const std::string& walletName, const std::string& password,
                                  PrivateKey& privateKey, PublicKey& publicKey, std::string& errorDescription)
{
    using namespace boost::filesystem;
    path wallet(walletPrefix + walletName + walletPostfix);
    if (!exists(wallet) || !is_regular(wallet))
    {
        errorDescription = StringFormat("Internal error. Wallet '%s': 'not exist'", walletName.c_str());
        return PCSA_RES_FAIL;
    }
    ContainerCipher container;
    
    std::vector<unsigned char> privKey;
    privKey.reserve(ECrypto::ePrivateKeySize);

    std::vector<unsigned char> pubKey;
    pubKey.reserve(ECrypto::ePublicKeySize);
    
    std::string errorString;
    if (!container.getKey(walletPrefix + walletName + walletPostfix, pubKey, privKey, password, errorString))
    {
        errorDescription = StringFormat("Internal error. Error read container '%s': '%s'", walletName.c_str(), errorString.c_str());
        return PCSA_RES_FAIL;
    }

    if (!privateKey.Set(privKey, errorString))
    {
        errorDescription = StringFormat("Internal error. Error in set private key: '%s'", errorString.c_str());
        return PCSA_RES_FAIL;
    }

    if (!publicKey.Set(pubKey, errorString))
    {
        errorDescription = StringFormat("Internal error. Error in set public key: '%s'", errorString.c_str());
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;
}

pcsa_result wallet_container_read_public(const std::string& walletName, PublicKey& publicKey, std::string& errorDescription)
{
    using namespace boost::filesystem;
    path wallet(walletPrefix + walletName + walletPostfix);
    if (!exists(wallet) || !is_regular(wallet))
    {
        errorDescription = StringFormat("Internal error. Wallet '%s': 'not exist'", walletName.c_str());
        return PCSA_RES_FAIL;
    }
    ContainerCipher container;
    
    std::vector<unsigned char> pubKey;
    pubKey.reserve(ECrypto::ePublicKeySize);
    
    std::string errorString;
    if (!container.getPublicKey(wallet.string(), pubKey, errorString))
    {
        errorDescription = StringFormat("Internal error. Error read container '%s': '%s'", 
                                        walletName.c_str(), errorString.c_str());
        return PCSA_RES_FAIL;
    }

    if (!publicKey.Set(pubKey, errorString))
    {
        errorDescription = StringFormat("Internal error. Error in set public key: '%s'", errorString.c_str());
        return PCSA_RES_FAIL;
    }

    return PCSA_RES_OK;    
}

pcsa_result wallet_container_delete(const std::string& walletName, const std::string& password, std::string& errorDescription)
{
    using namespace boost::filesystem;
    path wallet(walletPrefix + walletName + walletPostfix);
    if (!exists(wallet) || !is_regular(wallet))
    {
        errorDescription = StringFormat("Internal error. Wallet '%s': 'not exist'", walletName.c_str());
        return PCSA_RES_FAIL;
    }
    ContainerCipher container;
    
    std::vector<unsigned char> privKey;
    privKey.reserve(ECrypto::ePrivateKeySize);

    std::vector<unsigned char> pubKey;
    pubKey.reserve(ECrypto::ePublicKeySize);
    
    std::string errorString;
    if (!container.getKey(walletPrefix + walletName + walletPostfix, pubKey, privKey, password, errorString))
    {
        errorDescription = StringFormat("Internal error. Error read container '%s': '%s'", walletName.c_str(), errorString.c_str());
        return PCSA_RES_FAIL;
    }

    boost::system::error_code ec;
    if (!remove(wallet, ec))
    {
        errorDescription = StringFormat("Internal error. Error delete container '%s': '%s'", walletName.c_str(), ec.message().c_str());
        return PCSA_RES_FAIL;  
    }

    return PCSA_RES_OK;
}

pcsa_result wallet_container_list_wallets(std::vector<std::pair<std::string, PublicKey>>& wallets, std::string& errorDescription)
{
    ContainerCipher container;

    using namespace boost::filesystem;
    
    boost::system::error_code ec;
    boost::filesystem::directory_iterator walletsDir(walletPrefix, ec);

    if (ec)
    {
        errorDescription = StringFormat("Internal error. Not a directiry '%s': '%s'", walletPrefix.c_str(), ec.message().c_str());
        return PCSA_RES_OK;
    }

    std::string errorString;
    std::vector<unsigned char> pubKey;
    PublicKey key;
    pubKey.reserve(ECrypto::ePublicKeySize);

    for (directory_iterator it(walletsDir); it != directory_iterator(); ++it)
    {
        const auto& file = *it;
        pubKey.clear();
        key.Clear();
        errorString.clear();
        if (file.status().type() != file_type::regular_file || file.path().extension().string() != walletPostfix)
        {
            continue;
        }
        
        if (!container.getPublicKey(file.path().string(), pubKey, errorString))
        {
            errorDescription += StringFormat("Internal error. Error read container '%s': '%s'", 
                      file.path().string().c_str(), errorString.c_str());
            continue;
        }

        if (!key.Set(pubKey, errorString))
        {
            errorDescription += StringFormat("Internal error. Error get public key from container '%s': '%s'", 
                     file.path().string().c_str(), errorString.c_str());
            continue;
        }
        wallets.push_back({file.path().stem().string(), key});
    }


    return PCSA_RES_OK;
}