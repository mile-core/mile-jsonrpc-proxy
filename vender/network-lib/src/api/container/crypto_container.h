#pragma once

#include "csa_type.h"
#include "csa_type.h"
#include "crypto_types.h"

pcsa_result wallet_container_create(const std::string& walletName, const std::string& password, 
                                    const PrivateKey& privateKey, const PublicKey& publicKey, 
                                    std::string& errorDescription);
pcsa_result wallet_container_update_password(const std::string& walletName, const std::string& oldPassword,
                                             const std::string& newPassword, std::string& errorDescription);
pcsa_result wallet_container_read(const std::string& walletName, const std::string& password,
                                  PrivateKey& privateKey, PublicKey& publicKey,
                                  std::string& errorDescription);
pcsa_result wallet_container_list_wallets(std::vector<std::pair<std::string, PublicKey>>& wallets,
	                                      std::string& errorDescription);
pcsa_result wallet_container_read_public(const std::string& walletName, PublicKey& publicKey,
	                                     std::string& errorDescription);
pcsa_result wallet_container_delete(const std::string& walletName, const std::string& password,
	                                std::string& errorDescription);