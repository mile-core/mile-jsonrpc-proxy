/*
 * engine_connection.h
 */
#pragma once

#include <atomic>
#include <string>
#include <unordered_map>
#include <thread>
#include <memory>
#include <functional>
#include <chrono>
#include <mutex>

#include "csa_type.h"
#include "crypto_types.h"
#include "connection_event_handler.h"
#include "csa_asio.h"

namespace Engine
{

class XMsgIn;
class XMsgOut;
class Params;
class ControlSystemClient;

// Connection implemented with Boost
class ConnectionBst : public IEventHandler
{
private:

    std::atomic<bool>                  m_bActive;

    pcsa_result                        last_cs_result_;         // the last response result code
    std::string                        last_cs_error_text_;     // the last response error text message

    std::unique_ptr<asio::io_service>           
                                       m_ioService;
    std::unique_ptr<ControlSystemClient>
                                       m_csClient;

    std::thread                     m_thread;

    std::unordered_map<std::string, 
                       std::function<
                       void(const Params&)>
                       >               m_eventCallback;

    const std::chrono::seconds m_cacheTimeout;                       
    std::unordered_map<PublicKey, 
                        std::pair
                        <std::chrono::seconds, uint64_t>
                      >               m_transactionCache;
    std::mutex                        m_cacheMutex;
protected:

    void IoServiceThread(void);

    virtual bool HandleEvent(const XMsgIn& message);


public:
    void set_last_cs_error_text(const std::string& err_txt);
    void set_last_cs_result(const int r);

    bool check_transaction_cache(const PublicKey& key, uint64_t& value);
    void set_transaction_cache(const PublicKey& key, uint64_t value);
    void clear_transaction_cache();

    // CTOR
    ConnectionBst();
    // DTOR
    ~ConnectionBst();

    void set_network_failure_callback( void (*cb)(int code, const char * message) );

    void set_network_auth_callback( void (*cb)(int code, const char * message) );

    pcsa_result last_cs_result(void) const
    { return last_cs_result_; }

    const std::string& last_cs_error_text(void) const
    { return last_cs_error_text_; }

    void connect(const char* address,
                 unsigned short port,
                 const char* uname,
                 const char* client);

    // send request and get answer
    void request(XMsgOut const & mo, XMsgIn& mi);

    void disconnect(void);
};

}   // namespace Engine
