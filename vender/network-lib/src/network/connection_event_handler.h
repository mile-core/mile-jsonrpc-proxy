#pragma once

namespace Engine
{

class XMsgIn;

// Interface (base-class) for events handling
class IEventHandler
{
public:
    // DTOR -virt-
    virtual ~IEventHandler(void) {}

    virtual bool HandleEvent(const XMsgIn& message) = 0;
};
	
}