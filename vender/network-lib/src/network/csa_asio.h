#pragma once

#ifdef ASIO_STANDALONE

#define ASIO_HAS_BOOST_DATE_TIME
#define ASIO_HAS_BOOST_BIND
#include <system_error>
#include <asio.hpp>

namespace Engine
{
    namespace asio
    {
        using namespace ::asio;
        using std::error_code;
    }
}

#else
#include <boost/asio.hpp>
#include <boost/system/error_code.hpp>

namespace Engine
{
    namespace asio
    {
        using namespace boost::asio;
        using boost::system::error_code;
    }
}
#endif