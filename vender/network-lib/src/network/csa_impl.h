#pragma once
namespace Engine
{
    // forward declaration:
    class ConnectionBst;

    ConnectionBst* get_connection(void);
    void destroy_connection(void);

    void SetErrorText(const std::string& errorDescription);
}