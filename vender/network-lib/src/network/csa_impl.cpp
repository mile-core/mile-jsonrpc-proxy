/*
 * csa_impl.cpp
 */

#include <string>

#include "csa_pdbg.h"
#include "connection.h"

namespace Engine
{

    static ConnectionBst *st_pConn;

    ConnectionBst* get_connection(void)
    {
        if(!st_pConn)
        {
            st_pConn = new ConnectionBst();
        }

        return st_pConn;
    }

    void destroy_connection(void)
    {
        if (st_pConn)
        {
            delete st_pConn;
            st_pConn = nullptr;
        }
    }

    void SetErrorText(const std::string& errorDescription)
    {
        PDBG_ERR(errorDescription.c_str());
        get_connection()->set_last_cs_error_text(errorDescription);
    }

}
