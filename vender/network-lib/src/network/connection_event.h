#pragma once
#include <string>
#include <mutex>
#include <condition_variable>

namespace Engine
{

class Event
{
    // event parameters
    std::string                    m_name;
    bool                      m_isAutoResetEvent;
    // synchronization data
    std::mutex              m_mutex;
    std::condition_variable m_signal;
    // state
    volatile bool             m_eventIsSet;
    //
    Event(const Event& event);
public:
    Event(const char* name = "Event", bool isAutoResetEvent = true, bool eventIsSet = false);
    // note: do set event and wait for waiters before event destruction
    ~Event() { }
    //
    const std::string& Name() const { return m_name; }
    bool IsSet() const { return m_eventIsSet; }
    bool IsAutoReset() const { return m_isAutoResetEvent; }
    //
    void Set();
    //
    void Reset();
    //
    void Wait();
    // note: default time is 100 milliseconds
    bool TimedWait(unsigned short timeout_ms = 100);
};

}