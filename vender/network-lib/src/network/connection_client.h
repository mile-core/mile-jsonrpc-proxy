#pragma once
#include "connection_event.h"
#include "csa_asio.h"

namespace Engine
{

using std::string;
typedef asio::ip::tcp::socket cs_socket;
typedef asio::ip::tcp::endpoint cs_endpoint;

typedef std::shared_ptr<Event> SP_Event;

class ControlSystemClient
{
    // stopped/connected state flag
    volatile bool         m_stopped = true;
    // network/user request is sent flag
    volatile bool         m_requestIsSent = false;

    volatile bool         m_isLogin = false;

    cs_socket               m_socket;
    asio::streambuf         m_networkInputBuffer;
    asio::deadline_timer    m_deadlineConnection;
    asio::deadline_timer    m_deadlineLogin;
    asio::deadline_timer    m_deadlineServiceLive;
    // network output message
    string                  m_outputMessage;
    SP_Event                m_completionEvent;
    string                  m_responseMessage;
    // network event handler
    IEventHandler&          m_handler;
    // network request/response delimiter
    char                    m_delimiter;
    // synchronization data
    std::mutex            m_mutex;
    // user info
    std::string             m_userName;
    std::string             m_clientName;

    std::string             m_lastDisconnectErrorMsg;
    unsigned                m_lastDisconnectErrorCode;

    void (*m_network_connect_cb)(int code, const char * message) = nullptr;
    void (*m_network_auth_cb)(int code, const char * message) = nullptr;

    // --- async network functions ---

    //
    void StartConnect(cs_endpoint endpoint);
    void HandleConnect(const asio::error_code& ec, cs_endpoint ep);
    //
    void StartNetworkRead();
    void HandleNetworkRead(const asio::error_code& ec, std::size_t size);
    //
    void StartNetworkWrite(const string& message);
    void HandleNetworkWrite(const asio::error_code& ec);
    //
    void HandleNetworkLoginTimeout(const asio::error_code& ec);

    void CheckDeadline(const asio::error_code& ec);

public:
    // CTOR
    ControlSystemClient(asio::io_service& ioService, IEventHandler& handler, char delimiter = 0);

    void set_network_connect_callback(void (*cb)(int code, const char * message))
    {
        m_network_connect_cb = cb;
    }

    void set_network_auth_callback(void (*cb)(int code, const char * message))
    {
        m_network_auth_cb = cb;
    }

    void DummyTask();
    // Called by the user of the client class to initiate the connection process.
    // The endpoint iterator will have been obtained using a tcp::resolver.
    void Connect(const char* address, unsigned short port);

    void onConnect();

    void onAuth(int code, const char* msg);

    // This function terminates all the actors to shut down the connection. It
    // may be called by the user of the client class, or by the class itself in
    // response to graceful termination or an unrecoverable error.
    void Disconnect();

    void setUserName(const std::string& userName);
    void setClientName(const std::string& clientName);

    std::string getUserName();
    std::string getClientName();

    const string& request(const string& request) noexcept(false);
    // timeout is 60 seconds!
    const unsigned short  m_nRequestTimeoutMS = 5000;
};

}