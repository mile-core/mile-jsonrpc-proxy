/*
 * engine_connection_bst.cpp
 */
#include "csa_pdbg.h"
#include "connection.h"
#include "csa_event_parse.h"
#include "connection_msg.h"
#include "connection_error.h"
#include "connection_client.h"

#ifdef ANDROID
#include <jni.h>

JavaVM *javaVm = nullptr;
JNIEnv *jniEnv = nullptr;

class ThreadJNIEnv {
public:
    ThreadJNIEnv() {
        PDBG_("cs", "Jni Attaching thread");
        javaVm->AttachCurrentThread(&jniEnv, NULL);
    }

    ~ThreadJNIEnv() {
        PDBG_("cs", "Jni Detaching thread");
        javaVm->DetachCurrentThread();
    }
};

#endif

namespace Engine
{

void ConnectionBst::IoServiceThread(void)
{
    try
    {
#ifdef ANDROID
        static thread_local ThreadJNIEnv envs;
#endif
        while (m_bActive) {
            m_ioService->run();
            m_ioService->reset();
            clear_transaction_cache();
            m_csClient->DummyTask();
        }
    }
    catch (...)
    {
        //std::cerr << "I/O service exception: UNKNOWN!\n";
    }
    PDBG(">> ConnectionBst::IoServiceThread()");
}

ConnectionBst::ConnectionBst() :
        last_cs_result_{PCSA_RES_OK},
        last_cs_error_text_{},
        m_bActive(false),
        m_cacheTimeout(3)
{
    m_ioService.reset(new asio::io_service());
    m_csClient.reset(new ControlSystemClient(*m_ioService, *this));
    m_eventCallback = {
                        {"announce-transaction", announce_transaction},
                        {"start-consensus", start_consensus},
                        {"announce-block", announce_block},
                        {"digest-transaction-block", consensus_digest_transaction_block},
                        {"mask-transaction-block", consensus_mask_transaction_block},
                        {"block-signature", consensus_block_signature},
                        {"text-message", text_message},
                        {"new-block", new_block}
                    };
}

ConnectionBst::~ConnectionBst()
{
    disconnect();
}

bool ConnectionBst::HandleEvent(const XMsgIn& message)  
{
    const auto it = m_eventCallback.find(message.id());
    if (it == m_eventCallback.cend())
    {
        return false;
    }

    if (it->first == "new-block")
    {
        clear_transaction_cache();
    }

    it->second(message.params());
    return true;
}

void ConnectionBst::connect(const char* address,
                            unsigned short port,
                            const char* uname,
                            const char* client)
{
    PDBG("ENTER connect()");

    try
    {
        // connect to control system
        m_csClient->setUserName(uname);
        m_csClient->setClientName(client);
        m_csClient->Connect(address, port);

        m_bActive = true;  // start working

        // m_printThread = boost::thread(&ConnectionBst::ByTimerEventsPrint, this);
        // start control system client thread
        if (!m_thread.joinable())
        {
            m_thread = std::thread(&ConnectionBst::IoServiceThread, this);
        }
    }
    catch (Engine::error const &e)
    {
        throw;
    }
    catch(std::exception const &e)
    {
        PDBG("EXCEPTION: %s", e.what());
        //throw;
        throw error{PCSA_RES_RESOURCE_UNAVAILABLE, e.what()};
    }

    PDBG("RETURN");
}


void ConnectionBst::set_last_cs_result(const int r)
{
    if(r < 0 || r > PCSA_RES_LAST)
    {
        last_cs_result_ = PCSA_RES_FAIL;
    }
    else
    {
        last_cs_result_ = (pcsa_result)r;
    }
}

void ConnectionBst::set_last_cs_error_text(const string& err_txt)
{
    last_cs_error_text_ = err_txt;
}

bool ConnectionBst::check_transaction_cache(const PublicKey& key, uint64_t& value)
{
    std::lock_guard<std::mutex> lk(m_cacheMutex);
    auto it = m_transactionCache.find(key);
    if (it == m_transactionCache.end())
    {
        return false;
    }
    auto& data = it->second;
    const auto time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch());
    if (data.first + m_cacheTimeout < time)
    {
        return false;
    }
    
    value = data.second;
    ++data.second;
    data.first = time;
    return true;
}

void ConnectionBst::set_transaction_cache(const PublicKey& key, uint64_t value)
{
    std::lock_guard<std::mutex> lk(m_cacheMutex);
    m_transactionCache[key] = std::make_pair(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()), 
                                             value);
}

void ConnectionBst::clear_transaction_cache()
{
    std::lock_guard<std::mutex> lk(m_cacheMutex);
    m_transactionCache.clear();
}

void ConnectionBst::set_network_failure_callback(void (*cb)(int code, const char * message))
{
    m_csClient->set_network_connect_callback(cb);
}

void ConnectionBst::set_network_auth_callback(void (*cb)(int code, const char * message))
{
    m_csClient->set_network_auth_callback(cb);
}

void ConnectionBst::request(XMsgOut const & mo, XMsgIn& mi)
{
    string response;

    PDBG_("cs", "request:\n%s", mo.asString().c_str());

    try
    {
        response = m_csClient->request(mo.asString());
    }
    catch(std::exception const &e)
    {
        PDBG_ERR("EXCEPTION: %s", e.what());

        set_last_cs_error_text(e.what());
        set_last_cs_result(PCSA_RES_EXCEPTION);

        throw;  // exception goes up!
    }

    PDBG_("cs", "response:\n%s", response.c_str());
    mi.init(response);

    set_last_cs_result(mi.result());
    set_last_cs_error_text(mi.text());
}

void ConnectionBst::disconnect(void)
{
    m_bActive = false;  // stop working
    // stop I/O service
    m_ioService->stop();
    //m_printThread.join();
    m_thread.join();
}

}

