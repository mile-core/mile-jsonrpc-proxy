#pragma once

#include <stdexcept>
#include <string>

#include "csa_type.h"

namespace Engine
{
	// connection exception class
	class error final : public std::runtime_error
	{
	public:

	    error(void) = delete;
	    // CTOR
	    error(const pcsa_result ec, const std::string text)
	    :   runtime_error(text),
	        m_code{ec}
	    {}

	    error(const error&) = default;
	    error& operator = (const error&) = default;

	    pcsa_result code(void) const
	    {
	        return (m_code);
	    }

	private:

	    const pcsa_result  m_code = PCSA_RES_UNKNOWN;
	};
	
}