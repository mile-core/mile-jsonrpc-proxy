#include <vector>
#include <iostream>
#include <sstream>
#include <memory>
#include <mutex>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include "connection.h"
#include "connection_error.h"
#include "connection_client.h"
#include "connection_msg.h"
#include "csa_pdbg.h"

#include "csa_impl.h"

using namespace std;

namespace Engine
{

ControlSystemClient::ControlSystemClient(asio::io_service& ioService, IEventHandler& handler, char delimiter) :
    m_socket(ioService),
    m_deadlineConnection(ioService),
    m_deadlineLogin(ioService),
    m_deadlineServiceLive(ioService),
    m_handler(handler),
    m_delimiter(delimiter),
    m_lastDisconnectErrorMsg(""),
    m_lastDisconnectErrorCode(0)
{
    m_completionEvent.reset( new Event("m_completionEvent") );
}

void ControlSystemClient::DummyTask()
{
    m_deadlineServiceLive.expires_at(boost::posix_time::pos_infin);
    m_deadlineServiceLive.async_wait([](const asio::error_code&) {
        PDBG(">> m_deadlineServiceLive");
    });
}

void ControlSystemClient::Connect(const char* address, unsigned short port)
{
    PDBG(">> Connect() via network TCP socket");

    asio::error_code ec;

    asio::ip::address ip_address = asio::ip::address::from_string(address, ec);
    if (ec)
    {
        throw Engine::error(PCSA_RES_INVALID_PARAM, ec.message());
    }

    m_lastDisconnectErrorMsg = "";
    m_lastDisconnectErrorCode = 0;

    m_stopped       = true;
    m_requestIsSent = false;
    m_isLogin       = false;

    cs_endpoint endpoint(ip_address, port);
    
    StartConnect(endpoint);

    // Start the deadline actor. You will note that we're not setting any
    // particular deadline here. Instead, the connect and input actors will
    // update the deadline prior to each asynchronous operation.
    PDBG(">> Connect() via network TCP socket");
    m_deadlineConnection.async_wait(boost::bind(&ControlSystemClient::CheckDeadline, this, _1));
}

void ControlSystemClient::onConnect()
{
    if (m_network_connect_cb)
    {
        // clean and close <shell>!
        m_network_connect_cb(m_lastDisconnectErrorCode, m_lastDisconnectErrorMsg.c_str());
    }
}

void ControlSystemClient::onAuth(int code, const char* msg)
{
    if (code)
    {
        if (m_network_auth_cb)
        {
            m_network_auth_cb(code, msg);
        }
        m_lastDisconnectErrorCode = 1;
        m_lastDisconnectErrorMsg = "Auth error";
        Disconnect();
    } else
    {
        m_isLogin = true;
        if (m_network_auth_cb)
        {
            m_network_auth_cb(0, msg);
        }
    }
}

void ControlSystemClient::Disconnect()
{
    PDBG(">> Disconnect()!");
    m_stopped = true;
    m_isLogin = false;
    m_requestIsSent = false;
    asio::error_code ignoredErrorCode;
    m_socket.shutdown(cs_socket::shutdown_both, ignoredErrorCode);
    m_socket.close(ignoredErrorCode);

    m_deadlineConnection.cancel();
    m_deadlineLogin.cancel();
    m_deadlineServiceLive.cancel();
    onConnect();
}

void ControlSystemClient::setUserName(const std::string& userName)
{
    m_userName = userName;
}

void ControlSystemClient::setClientName(const std::string& clientName)
{
    m_clientName = clientName;
}

std::string ControlSystemClient::getUserName()
{
    return m_userName;
}

std::string ControlSystemClient::getClientName()
{
    return m_clientName;
}

void ControlSystemClient::StartConnect(cs_endpoint endpoint)
{
    PDBG(">> ControlSystemClient::StartConnect()");

    // std::cout << "Trying to connect to " << endpoint << "...\n";

    // set a deadline for the connect operation
    m_deadlineConnection.expires_from_now(boost::posix_time::seconds(5));

    // start the asynchronous connect operation
    m_socket.async_connect(endpoint, boost::bind(&ControlSystemClient::HandleConnect, this, _1, endpoint));
}


void ControlSystemClient::HandleConnect(const asio::error_code& ec, cs_endpoint endpoint)
{
    PDBG(">> ControlSystemClient::HandleConnect()");
/*
    if (m_stopped)
    {
        return;
    }*/

    // the async_connect() function automatically opens the socket at the start
    // of the asynchronous operation. If the socket is closed at this time then
    // the timeout handler must have run first.
    if (!m_socket.is_open())
    {
        PDBG("!m_socket.is_open()");
        m_lastDisconnectErrorCode = 1;
        m_lastDisconnectErrorMsg = "Connect timed out";
        Disconnect();
        return;
    }
    else if (ec) // check if the connect operation failed before the deadline expired.
    {
        PDBG("ec");
        m_lastDisconnectErrorCode = 1;
        m_lastDisconnectErrorMsg = "Connect error: " + ec.message();
        m_socket.close();
        return;
    }
    // otherwise we have successfully established a connection.
#ifdef DEBUG_BUILD
    std::cout << "Connected to " << endpoint << "\n";
#endif

    m_socket.set_option(asio::socket_base::keep_alive(true) );
    m_stopped = false;
    m_deadlineConnection.expires_at(boost::posix_time::pos_infin);
    //m_deadlineConnection.cancel();
    // m_deadline.cancel();
    // start network event/response reading
    StartNetworkRead();
    m_lastDisconnectErrorCode = 0;
    m_lastDisconnectErrorMsg  = "";
    onConnect();

    PDBG("login");
    XMsgOut mo;
    mo.set_op("login")
            .add("user", m_userName)
            .add("client", m_clientName);

    StartNetworkWrite(mo.asString());
    m_deadlineLogin.expires_from_now(boost::posix_time::milliseconds(m_nRequestTimeoutMS));
    m_deadlineLogin.async_wait(boost::bind(&ControlSystemClient::HandleNetworkLoginTimeout, this, _1));
}


const string& ControlSystemClient::request(const string& req_str) noexcept(false)
{
    PDBG(">> request(to CS): \'%s\'", req_str.c_str());

    if (m_stopped)
    {
    	PDBG_ERR("throw std::domain_error");
    	throw std::domain_error{"control system client stopped"};
    }

    m_completionEvent->Reset();

    // process current message
    StartNetworkWrite(req_str);

    // note: inf waiting -> add error processing e.g. disconnect etc
    if (!m_completionEvent->TimedWait(m_nRequestTimeoutMS))
    {
        PDBG_ERR("throw std::runtime_error");
        throw std::runtime_error{"response timed out"};
    }

    return m_responseMessage;
}


void ControlSystemClient::StartNetworkWrite(const string& message)
{
    PDBG(">> ControlSystemClient::StartNetworkWrite()");

    m_requestIsSent = true;
    m_outputMessage = message;

    // start an asynchronous operation to send packed to XML output message
    // note: null-terminated string
    asio::async_write(m_socket, asio::buffer(m_outputMessage.c_str(), m_outputMessage.size() + 1),
                      boost::bind(&ControlSystemClient::HandleNetworkWrite, this, _1));
}


void ControlSystemClient::HandleNetworkWrite(const asio::error_code& ec)
{
    PDBG(">> ControlSystemClient::HandleNetworkWrite()");

    if(m_stopped)
    {
    	PDBG("m_stopped");
        return;
    }

    if(ec)
    {
        PDBG_ERR("network write: %s", ec.message().c_str());

        m_completionEvent->Set();
        m_lastDisconnectErrorMsg = ec.message();
        m_lastDisconnectErrorCode = ec.value();
        Disconnect();
    }
}


void ControlSystemClient::StartNetworkRead()
{
    PDBG(">> ControlSystemClient::StartNetworkRead()");

    if (m_stopped)
    {
        return;
    }

    // start an asynchronous operation to read a zero-delimited message.
    asio::async_read_until(m_socket, m_networkInputBuffer, m_delimiter,
                           boost::bind(&ControlSystemClient::HandleNetworkRead, this, asio::placeholders::error, asio::placeholders::bytes_transferred));
}


void ControlSystemClient::HandleNetworkRead(const asio::error_code& ec, std::size_t size)
{
    PDBG(">> ControlSystemClient::HandleNetworkRead()");

    if (m_stopped)
    {
        return;
    }

    if (ec)
    {
        PDBG_ERR("error_code: %d (%s)", ec.value(), ec.message().c_str());
        m_lastDisconnectErrorMsg = ec.message();
        m_lastDisconnectErrorCode = ec.value();
        Disconnect();
        return;
    }

    if (size == 0)
    {
        // start network event/response reading
        StartNetworkRead();
        return;
    }

    // extract the zero-delimited message from the buffer.
    string line;
    istream inputStream(&m_networkInputBuffer);

    std::getline(inputStream, line, m_delimiter);

    // decode input XML message and detect type of message (response or event)
    boost::property_tree::ptree propertyTree;
    try
    {
        std::stringstream ss; 
        ss << line;
        boost::property_tree::read_json(ss, propertyTree);
    }
    catch (const boost::property_tree::json_parser::json_parser_error &e)
    {
        PDBG_ERR("failed to parse received json message: %s (%s)", line.c_str(), e.what());
    }
    catch (...)
    {
        PDBG_ERR("failed to parse received json message: %s (unknown error)", line.c_str());
    }
    // check message type
    if (propertyTree.find("id") != propertyTree.not_found())
    {

        // check if request sent
        XMsgIn in;
        if (!m_handler.HandleEvent(in.init_event(propertyTree)))
        {
            if (m_requestIsSent)       
            {
                // lock response data during processing
                std::lock_guard<std::mutex> lock(m_mutex);
                m_requestIsSent = false;
                // response message detected
                m_responseMessage = line;

                // set completion event
                if (!m_isLogin)
                {
                    m_deadlineLogin.cancel();
                } 
                else
                {
                    m_completionEvent->Set();
                }
            } 
            else
            {
                // TODO: currently get false error messeges due to HandleNetworkWrite can be called AFTER this function
                PDBG_ERR("response is received but request hasn't been sent");
            }
        } 
    }
    else // error
    {
        // unknown message detected
        PDBG_ERR("unknown message received");
    }

    // start network event/response reading
    StartNetworkRead();
}

void ControlSystemClient::HandleNetworkLoginTimeout(const asio::error_code& ec)
{
    PDBG("...HandleNetworkLoginTimeout()...");
    if (m_stopped)
    {
        return;
    }

    if (!ec)
    {
        onAuth(1, "Login timeout");
        return;
    }

    XMsgIn mi;
    mi.init(m_responseMessage);
    std::string result = mi.result() ? std::string("Failed to login, error: " +
                                                   mi.text())
                                     : "";
    onAuth(mi.result(), result.c_str());
}

void ControlSystemClient::CheckDeadline(const asio::error_code& ec)
{
    PDBG("...CheckDeadline()...");

/*    if (m_stopped)
    {
        return;
    }*/
    // Check whether the deadline has passed. We compare the deadline against
    // the current time since a new asynchronous operation may have moved the
    // deadline before this actor had a chance to run.
    if (!ec)
    {
        // The deadline has passed. The socket is closed so that any outstanding
        // asynchronous operations are cancelled.
        if (!m_socket.is_open())
        {
          Disconnect();  
      } else
      {
        m_socket.close();
      }
    }
}

//-------------------------------------------------------------------------------------------------

Event::Event(const char* name, bool isAutoResetEvent, bool eventIsSet)
    : m_name(name)
    , m_isAutoResetEvent(isAutoResetEvent)
    , m_eventIsSet(eventIsSet)
{
}

void Event::Set()
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_eventIsSet = true;
    }
    // not locked?
    m_signal.notify_one();
}

void Event::Reset()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_eventIsSet = false;
}

void Event::Wait()
{
    std::unique_lock<std::mutex> lock(m_mutex);

    m_signal.wait(lock, [this] { return m_eventIsSet; });

    if (m_isAutoResetEvent)
    {
        m_eventIsSet = false; // waiting resets the event state
    }
}

bool Event::TimedWait(unsigned short timeout_ms)
{
    // absolute time timeout
    // note: what if time is changed during waiting?
    std::unique_lock<std::mutex> lock(m_mutex);

    if (!m_signal.wait_for(lock, std::chrono::milliseconds(timeout_ms), [this] {return m_eventIsSet; }))
    {
       return false; // timeout error 
    }

    if(m_isAutoResetEvent)
    {
        m_eventIsSet = false; // waiting resets the event state
    }

    return true;
}

}
