#pragma once

#include "csa_type.h"

#ifdef __cplusplus
extern "C" {
#endif

void pcsa_register_event_announce_transaction(void (*)(const char* block_id, 
                                                       const char *last_block_digest,
                                                       const char* transaction_name,
                                                       const char* transaction_data));

void pcsa_register_event_start_consensus(void (*)(const char* block_id, 
                                                  const char *last_block_digest));

void pcsa_register_event_announce_block(void (*)(const char* block_id, 
                                                 const char *last_block_digest,
                                                 const char* block_digest));

void pcsa_register_event_consensus_digest_transaction_block(void (*)(const char* digest_data, 
                                                                     const char *digest_data_signature));

void pcsa_register_event_consensus_mask_transaction_block(void (*)(const char* mask_data, 
                                                                   const char *mask_data_signature));

void pcsa_register_event_consensus_block_signature(void (*)(const char* last_block_digest, 
                                                            const char *next_block_signature));

void pcsa_register_event_text_message(void (*)(const char* text));

void pcsa_register_event_new_block(void (*)(const char* block_id, 
                                            const char *block_digest));

#ifdef __cplusplus
}
#endif