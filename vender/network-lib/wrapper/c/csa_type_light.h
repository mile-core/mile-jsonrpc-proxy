#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define BASE58MULTY 138 / 100

static const unsigned int private_key_len = (64 + 3) * BASE58MULTY + 4; // 4 byte for checksum
static const unsigned int publick_key_len = (32 + 3) * BASE58MULTY + 4;
// id == 8, node public key == 32, node address == 64, signed flag == 1, sing == 64
static const unsigned int transaction_reg_node_len = 8 + 32 + 64 + 1 + 64;
// id == 8, node public key == 32, signed flag == 1, sing == 64
static const unsigned int transaction_unreg_node_len = 8 + 64 + 1 + 32;
// id == 8, asset code ==2, src wallet public key == 32, dst wallet public key == 32,
// amount == 16, signed flag == 1, signature == 64
static const unsigned int transaction_transfer_asset_len = 8 + 2 + 32 + 32 + 16 + 1 + 64;

typedef enum
{
    PCSA_RES_UNKNOWN = -1,

    PCSA_RES_OK = 0,
    PCSA_RES_FAIL = 1,
    PCSA_RES_NOT_SUPPORTED = 2,
    PCSA_RES_INVALID_PARAM = 3,
    PCSA_RES_ACCESS_DENIED = 4,
    PCSA_RES_TIMEOUT = 5,
    PCSA_RES_LOGOUT = 6,
    // ex.: disk full when appending to log
    PCSA_RES_RESOURCE_UNAVAILABLE = 7,
    PCSA_RES_NOT_FOUND = 8,
    PCSA_RES_ALREADY_EXIST = 9,
    PCSA_RES_EXCEPTION = 10,
    PCSA_RES_LAST = PCSA_RES_EXCEPTION
} pcsa_result;

typedef struct _pcsa_keys_pair
{
    char public_key[publick_key_len + 1];
    char private_key[private_key_len + 1];
} pcsa_keys_pair;

#ifdef __cplusplus
}
#endif