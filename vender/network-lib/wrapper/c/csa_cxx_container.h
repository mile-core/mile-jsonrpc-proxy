#pragma once

#include "csa_type.h"

#ifdef __cplusplus
extern "C" {
#endif

// local
pcsa_result delete_local_wallet(const char* walletName, const char* password, char** errorMessage);
pcsa_result update_password_local_wallet(const char* walletName, const char* oldPassword, const char* newPassword, char** errorMessage);
pcsa_result get_local_wallet_list(pcsa_string_literal** walletList, unsigned* walletCount, char** errorMessage);
pcsa_result create_local_wallet(const char* walletName, const char* password, pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result create_local_wallet_by_keys(const char* walletName, const char* password, pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result get_local_wallet_keys(const char* walletName, const char* password, pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result get_local_wallet_public_key(const char* walletName, pcsa_keys_pair* keyPair, char** errorMessage);

#ifdef __cplusplus
}
#endif