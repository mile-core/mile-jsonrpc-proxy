/*
 * csa_cxx.h
 *
 * C++ API to Control System services.
 */
#pragma once

#include "csa_type.h"

#ifdef __cplusplus
extern "C" {
#endif


/*
* Выполнить отладочный запрос к системе управления.
* Формат строки запроса определяется реализацией системы управления. Результат возвращается в виде
* строки ответа произвольного формата и кода ошибки.
* request      ASCIIz строка запроса
* response     Указатель на буфер для строки ответа.
*              D буфер будет помещено не более response_buf_size символов
*              ответа системы управления, включая '\0'.
*/
pcsa_result debug_request(const char *request, char **response);
// node wallet
pcsa_result create_node_wallet(const char* walletName, const char* password, pcsa_keys_pair* keyPair);
pcsa_result get_node_wallet_keys(const char* walletName, const char* password, pcsa_keys_pair* keyPair);
pcsa_result get_node_wallet_list(pcsa_string_literal** walletList, unsigned* walletCount);

// blockchain 
pcsa_result  transfer_assets_transaction_data(const char *trxData, pcsa_error *error);
pcsa_result get_wallet_state(const char* walletName, pcsa_wallet_state* state);
pcsa_result get_wallet_state_by_key(const char* pubKey, pcsa_wallet_state* state);
pcsa_result register_node(const char* walletName, const char* address);
pcsa_result unregister_node(const char* walletName);
pcsa_result transfer_asset(const char* asset, const char* srcWallet, 
                            const char* dstWallet, const char* amount);
pcsa_result get_wallet_transactions(const char* walletName, unsigned count,
                                     pcsa_wallet_transaction** transaction, unsigned* transactionCount);

pcsa_result get_wallet_transactions_by_key(const char* pubKey, unsigned count,
                                     pcsa_wallet_transaction** transaction, unsigned* transactionCount);

pcsa_result get_blockchain_info(pcsa_blockchain_info* info);
pcsa_result get_block_by_id(const char* id, char** data);
pcsa_result get_block_by_digest(const char* hash, char** data);
pcsa_result get_blockchain_state(pcsa_blockchain_state* state);

// binary transaction
pcsa_result register_node_transaction(const pcsa_keys_pair* keyPair,
                                       const char* nodeAddress);
pcsa_result unregister_node_transaction(const pcsa_keys_pair* keyPair);
pcsa_result transfer_assets_transaction(const pcsa_keys_pair* srcKeyPair, 
                                         const char* dstWalletPublicKey,
                                         unsigned short assets,
                                         const char* amount);
pcsa_result user_data_transaction(const pcsa_keys_pair* keyPair,
                                   const char* userData,
                                   unsigned int length);
pcsa_result register_system_wallet_transaction(const pcsa_keys_pair* srcKeyPair, 
                                               const char* dstWalletPublicKey,
                                               unsigned char walletTag);
pcsa_result unregister_system_wallet_transaction(const pcsa_keys_pair* srcKeyPair, 
                                               const char* dstWalletPublicKey,
                                               unsigned char walletTag);

#ifdef __cplusplus
}
#endif
