#pragma once

#include <stdint.h>
#include "csa_type_light.h"

#ifdef __cplusplus
extern "C" {
#endif

pcsa_result generate_key_pair(pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result generate_key_pair_from_private_key(pcsa_keys_pair* keyPair, char** errorMessage);
pcsa_result deserialize_block(const char* blockHex, char** deserialaizedBlock, char** errorMessage);
pcsa_result generate_key_pair_with_secret_phrase(pcsa_keys_pair* keyPair, const char* phrase, 
                                                 size_t length, char** errorMessage);
pcsa_result create_transaction_register_node(const pcsa_keys_pair* keyPair, 
                                             const char* nodeAddress,
                                             uint64_t id,
                                             char** transaction, 
                                             char** errorDescription);
pcsa_result create_transaction_unregister_node(const pcsa_keys_pair* keyPair,
                                               uint64_t id,
                                               char** transaction, 
                                               char** errorDescription);

pcsa_result create_transaction_transfer_assets(const pcsa_keys_pair* srckeyPair,
                                               const char* dstWalletPublicKey,
                                               uint64_t id,
                                               unsigned short assets,
                                               const char* amount,
                                               char** transaction,
                                               char** errorDescription);

pcsa_result  create_transaction_user_data(const pcsa_keys_pair* keyPair,
                                          uint64_t id,
                                          const char* dataString,
                                          char** transaction,
                                          char** errorDescription);
pcsa_result create_transaction_register_system_wallet(const pcsa_keys_pair* srcKeyPair,
                                                       const char* dstWalletPublicKey,
                                                       uint64_t id,
                                                       unsigned char walletTag,
                                                       char** transaction,
                                                       char** errorMessage);
pcsa_result create_transaction_unregister_system_wallet(const pcsa_keys_pair* srcKeyPair,
                                                       const char* dstWalletPublicKey,
                                                       uint64_t id,
                                                       unsigned char walletTag,
                                                       char** transaction,
                                                       char** errorMessage);
#ifdef __cplusplus
}
#endif