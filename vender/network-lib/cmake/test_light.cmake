include(CMakeParseArguments)
enable_testing()

add_custom_target(unit_test)

function(add_unit_test dependency)
    cmake_parse_arguments(UT "" "NAME" "COMMAND" ${ARGN} )
    add_test(${ARGN})
    add_dependencies(unit_test ${dependency})
    message(STATUS "Dependency ${dependency}")
    set_target_properties(
        ${dependency}
        PROPERTIES
        RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/test
    )
endfunction(add_unit_test)

add_executable(lightAPITest src/test/test.cpp)
target_link_libraries(lightAPITest ${LIBNAME})
add_unit_test(lightAPITest NAME lightAPITest COMMAND $<TARGET_FILE:lightAPITest>)

add_executable(uin256Test src/test/test_uint256.cpp)
target_link_libraries(uin256Test ${LIBNAME})
add_unit_test(uin256Test NAME uin256Test COMMAND $<TARGET_FILE:uin256Test>)

if (NOT LIGHT_BUILD)
    add_executable(networkTest src/test/test_network.cpp)
    target_link_libraries(networkTest ${LIBNAME})
    add_unit_test(networkTest NAME networkTest COMMAND $<TARGET_FILE:networkTest>)
endif()

add_custom_command(
     TARGET unit_test
     COMMENT "Run tests"
     POST_BUILD 
     COMMAND ${CMAKE_CTEST_COMMAND} -C $<CONFIGURATION> ${CMAKE_BINARY_DIR}/test
)