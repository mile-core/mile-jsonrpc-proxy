if (WITH_CONTAINER)
    find_package(OpenSSL)
    include_directories(${OPENSSL_INCLUDE_DIR})
endif()

if (ASIO_STANDALONE)
    add_definitions("-DASIO_STANDALONE")
    include_directories(${CMAKE_CURRENT_SOURCE_DIR}/src/external/asio/asio/include)         
elseif(NOT LIGHT_BUILD)
    set(Boost_USE_STATIC_LIBS ON)
    set(Boost_USE_MULTITHREADED ON) 
    
    find_package(Boost REQUIRED COMPONENTS system thread filesystem)

    find_package(Threads)
endif()


include_directories (
    ${Boost_INCLUDE_DIRS}
)

if (DEBUG_BUILD)
    if(SANITIZER)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address -fsanitize=leak -fsanitize=undefined")
    endif()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -ggdb")
else()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -s -O2")
endif()


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -DX86_LINUX -DBOOST_THREAD_USE_LIB")

# write libsumus version
file(WRITE "${VERSION_FILE}" "${LIBNAME} ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")
message(STATUS "${LIBNAME} version ${MAJOR_VERSION}.${MINOR_VERSION}${VERSION_SUFFIX}")

target_link_libraries(${LIBNAME} ${CMAKE_THREAD_LIBS_INIT} ${Boost_LIBRARIES} ${OPENSSL_SSL_LIBRARY} ${OPENSSL_CRYPTO_LIBRARY} rt dl)
