set(ps_lib_js "lib${PROJECT}.js") 

include_directories (
    ${Boost_INCLUDE_DIRS}
)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --bind")
add_custom_target(${ps_lib_js} ALL
                  COMMAND ${CMAKE_C_COMPILER} --bind ${CMAKE_SHARED_LIBRARY_PREFIX}${LIBNAME}${CMAKE_SHARED_LIBRARY_SUFFIX} -o ${ps_lib_js}
                  DEPENDS ${LIBNAME}
)