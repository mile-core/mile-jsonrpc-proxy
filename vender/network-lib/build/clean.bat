rmdir /S /Q CMakeFiles 
rmdir /S /Q test
rmdir /S /Q Testing
del Makefile
del cmake_install.cmake
del CMakeCache.txt
@rem CL specific
del *.pdb
del *.res
del *.ilk
del *.dll
del *.lib
del *.exp
