rm -r CMakeFiles
rm -r test
rm -r Testing
rm Makefile
rm cmake_install.cmake
rm CMakeCache.txt
ls | grep -P "^lib.*\.a$" | xargs -d"\n" rm
rm CTestTestfile.cmake
