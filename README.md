* BUILD
git clone git@bitbucket.org:mile-core/mile-explorer-backend.git ~/mile-explorer-backend
cd ~/mile-explorer-backend


cmake -DBoost_NO_BOOST_CMAKE=TRUE -DBoost_NO_SYSTEM_PATHS=TRUE -DBOOST_ROOT='path'' -DBoost_LIBRARY_DIRS='path'' CMakeLists.txt

make wallet-jsonrpcd



# jsonrpc-proxy

Prepare
```bash
git clone git@bitbucket.org:mile-core/mile-jsonrpc-proxy.git ~/mile-jsonrpc-proxy
cd ~/mile-jsonrpc-proxy
git checkout update_lib
```

Build base image
```bash
docker build -f Dockerfile_base -t mile:base .
```

Build app image
```bash
docker build -f Dockerfile_app -t jsonrpc:$(git log -1 --format=%h) .
```

